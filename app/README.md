# IUDo App

Le projet utilise [Nx](https://nx.dev).

## Setup

Pour développer, il est conseillé d'installer Nx globalement :

```
npm install -g @nrwl/cli
```

1. Copier le fichier `.env` en `.local.env` et renseigner les variables manquantes.

2. Installer le projet :

```
npm install
```

3. Démarrer le projet :

```
npm run serve
```

L'application est disponible sur http://localhost:8080

## Ajout de fonctionnalités

Nx prend en charge de nombreux plugins qui ajoutent des capacités pour développer différents types d'applications et différents outils.

Ces capacités incluent la génération d'applications, de bibliothèques, etc. ainsi que les devtools pour tester et créer des projets également.

## Code partagé

Exécutez `nx g @nrwl/workspace:lib data` pour générer une bibliothèque.

Vous pouvez consulter la documentation suivante pour en savoir plus :

* https://nx.dev/angular/tutorial/07-share-code
* https://nx.dev/angular/tutorial/08-create-libs

## Development

Exécutez `npm run serve` pour démarrer le front + l'API.

* http://localhost:8080 -> front
* http://localhost:8080/api -> API servie en proxy à partir de http://localhost:3333/api

## Storybook

Exécutez `npm run storybook:serve` pour démarrer Storybook

* http://localhost:6006

## Build

Exécutez `npm run build` pour compiler le projet. Les artefacts de construction seront stockés dans le répertoire `dist/`. Utilisez l'indicateur `--prod` pour une version de production.

## Dépendances

Exécutez `nx dep-graph` pour voir un diagramme des dépendances de vos projets.

## Aide supplémentaire

Visitez la [Documentation Nx] (https://nx.dev) pour en savoir plus.
