import { LngLatLike } from 'mapbox-gl';

class HTTPError extends Error {
  constructor(name, message) {
    console.log(message, 'MESSAGE');
    super(message);
    // if (arguments.length >= 3 && extras) {
    //   Object.assign(this, extras);
    // }
    this.name = name;
    this.message = message;
  }
}
export default HTTPError;

export const fetchApi = async (
  url: string,
  params?: object,
  options?: RequestInit
) => {
  // @ts-ignore
  const queryString = new URLSearchParams(params).toString();

  try {
    const response = await fetch(`${url}?${queryString}`, {
      ...options,
    });

    const result = await response.json();
    if (response.ok === false) {
      const CustomError = new Error(result.error);
      CustomError.name = response.status.toString();
      CustomError.message = result.error;

      throw new HTTPError(
        response.status.toString(),
        result.error || response.statusText
      );
    } else {
      return result;
    }
  } catch (error) {
    throw Error(error);
  }
};

export const convertCoordinatesToLngLat = (
  coordinates: Coordinates
): LngLatLike => {
  return {
    lng: coordinates[0],
    lat: coordinates[1],
  };
};

export const roundValue = (value: number, decimals = 0) => {
  const formatter = new Intl.NumberFormat('fr-FR', {
    minimumFractionDigits: 0,
    maximumFractionDigits: decimals,
  });

  return formatter.format(value).replace(',', '.');
  // if (!decimals) {
  //   return Math.round(value * 100) / 100;
  // } else {
  //   return Math.round(value);
  // }
};

export const formatAsSquareMeters = (value: number | null, isSuffix = true) => {
  if (value !== null) {
    const suffix = isSuffix ? ' m²' : '';
    return `${roundValue(value)}${suffix}`;
  } else {
    return '';
  }
};
export const formatAsPercent = (value: number | null, isSuffix = true) => {
  if (value !== null) {
    const suffix = isSuffix ? ' %' : '';
    return `${roundValue(value)}${suffix}`;
  } else {
    return '';
  }
};

export const formatAsMeters = (value: number | null, isSuffix = true) => {
  if (value !== null) {
    const suffix = isSuffix ? ' m' : '';
    return `${roundValue(value, 1)}${suffix}`;
  } else {
    return '';
  }
};

export const formatAsPrice = (value: number, isSuffix = true) => {
  const formatter = new Intl.NumberFormat('fr-FR', {
    style: 'currency',
    currency: 'EUR',
    minimumFractionDigits: 0,
    maximumFractionDigits: 2,
  });

  return isSuffix ? formatter.format(value).replace(',', '.') : value;
};

export const formatAsPricePerSquareMeter = (value: number, isSuffix = true) => {
  if (value !== null) {
    const suffix = isSuffix ? '/m2' : '';
    return `${formatAsPrice(value)}${suffix}`;
  } else {
    return value;
  }
};

export const formatAsYesNo = (value: boolean | null) => {
  if (value === true) {
    return 'oui';
  } else if (value === false) {
    return 'non';
  } else {
    return '';
  }
};

export const scrollToElement = (
  selector: string,
  options?: ScrollIntoViewOptions
) => {
  const element = document.querySelector(selector);
  const { top } = element.getBoundingClientRect();

  const defaultOptions: ScrollIntoViewOptions = {
    // top: -top,
    // left: 0,
    behavior: 'smooth',
    block: 'end',
  };

  element.scrollIntoView({ ...defaultOptions, ...options });
};
