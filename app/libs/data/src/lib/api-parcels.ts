import {
  LandUnit,
  Parcel,
  ParcelsApiParams,
  ParcelsApiResponse,
} from '@iudo/data';
import { fetchApi } from '@iudo/shared/utils';

export const searchParcel = async (
  params: ParcelsApiParams
): Promise<Parcel> => {
  return await fetchApi(`${process.env.VUE_APP_API_URL}/parcels`, {
    ...params,
  } as ParcelsApiParams);
};

export const fetchParcels = async (
  parcels: Array<Parcel['id']>
): Promise<ParcelsApiResponse> => {
  const parcelList = parcels.join(',').toString();
  return await fetchApi(`${process.env.VUE_APP_API_URL}/parcels/${parcelList}`);
};

// export const fetchParcelsNeighbors = async (
//   parcels: Array<Parcel["id"]>
// ): Promise<Parcel[]> => {
//   const parcelList = parcels.join(",").toString();
//   return await fetchApi(`${process.env.VUE_APP_API_URL}/parcels/${parcelList}/neighbors`);
// };

export const fetchLandUnits = async (
  parcels: Array<Parcel['id']>
): Promise<LandUnit[]> => {
  const parcelList = parcels.join(',').toString();
  return await fetchApi(
    `${process.env.VUE_APP_API_URL}/parcels/${parcelList}/units`
  );
};
