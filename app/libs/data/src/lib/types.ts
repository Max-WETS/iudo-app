import { AddressApi } from '@iudo/data';
export type AreaGeometry = (GeoJSON.Polygon | GeoJSON.MultiPolygon) & {
  id?: number;
};
export type AreaCoordinates = Pick<Coordinates, 'latitude' | 'longitude'>;
import Stripe from 'stripe';
import { SegmentAnalytics } from '@segment/analytics.js-core';

declare global {
  interface Window {
    analytics: SegmentAnalytics.AnalyticsJS;
  }
}
export type Analytics = SegmentAnalytics.AnalyticsJS;

/**
 * Parcelle
 */
export interface Parcel {
  /**
   * ID unique (Identifiant séquentiel)
   */
  id: number;

  /**
   * Le code CSP ne concerne que les communes de la banlieue parisienne.
   * C’est l'identifiant cadastral de la parcelle. Il est composé de 3 chiffres
   * correspondant au code INSEE de la commune 1 ou 2 lettres correspondant à
   * la section cadastrale et enfin, 4 chiffres correspondant au numéro de la
   * parcelle (Ex: 0052 pour parcelle n°52)
   */
  csp: string;

  /**
   * Le code ASP ne concerne que Paris. Il (Arrondissement, Section, Parcelle)
   * constitue la référence cadastrale d’une parcelle dans Paris. Il est composé
   * de 2 chiffres correspondant à l’arrondissement, de trois caractères correspondant
   * à la section cadastrale et enfin, 4 chiffres correspondant au numéro de la parcelle.
   */
  asp?: string;

  /**
   * La section est l'unité de découpage cadastral. C'est une partie du territoire communal
   * déterminée dans le but de faciliter l'établissement, la consultation et la tenue des
   * documents cadastraux. La section peut être représentée sur une ou plusieurs feuilles
   * (ou subdivisions de section).
   */
  section: string;

  /**
   * Code INSEE de la commune ou de l'arrondissement
   */
  insee: number;

  /**
   * Numero de parcelle dans la section
   */
  number: number;

  /**
   * Type de parcelle cadastrale
   */
  type: string;

  /**
   * Surface de la parcelle
   */
  surface: number;

  /**
   * Surface fiscale de la parcelle
   */
  surface_fisc: number;

  /**
   * Géométrie en GeoJSON
   */
  geometry: AreaGeometry;

  /**
   * Coordonnées du centroïde de la parcelle
   */
  coordinates?: AreaCoordinates;

  /**
   * Adresse BAN rattachée à la parcelle
   */
  address?: AddressApi.SearchFeatureProperties;

  /**
   * Lors de la sélection de plusieurs parcelles, cette propriété permet
   * d'indiquer que la parcelle ne peut être supprimer sans invalider
   * l'unité foncière
   */
  protected?: boolean;
}

/**
 * Unité foncière
 */
export interface LandUnit {
  /**
   * Parcelles
   */
  parcels: Parcel[];

  /**
   * Surface de l'unitée
   */
  surface: number;

  /**
   * Surface fiscale de l'unité
   */
  surface_fisc: number;

  /**
   * Géométrie en GeoJSON
   */
  geometry: AreaGeometry;

  /**
   * Coordonnées du centroïde de l'unité foncière
   */
  coordinates: AreaCoordinates;

  /**
   * Emprise bâtie de l'unité foncière
   */
  building_extent: BuildingExtent;

  /**
   * Liste des buildings
   */
  buildings: Building[];

  /**
   * Zones PLU
   */
  urban_zones: UrbanZone[];

  /**
   * Liste des éléments notables à proximité
   */
  nearby: Nearby[];

  /**
   * Url pour récupérer la liste des risques
   */
  risks: string;

  /**
   * Adresse BAN rattachée à l'unité foncière'
   */
  address?: AddressApi.SearchFeatureProperties;

  /**
   * Règles d’emprise au sol maximale des constructions inscrite dans les PLU des communes
   */
  footprint_rules?: Dictionary[];

  /**
   * Règles de hauteur maximale des constructions inscrite dans les PLU des communes
   */
  height_rules?: Dictionary[];

  /**
   * Prix moyens
   */
  prices: RealEstatePrices;
}

export enum Nearby {
  /**
   * Station de transport en commun à moins de 500m
   */
  transport = 'TRANSPORT',

  /**
   * Monument historique à moins de 500m
   */
  'historic_monument' = 'HISTORIC_MONUMENT',

  /**
   * Station Grand Paris Express à moins de 800m
   */
  'gpe_station' = 'GPE_STATION',
}

export enum Risk {
  /**
   * Cavités souteraine
   */
  cavity = 'CAVITY',

  /**
   * Inondations
   */
  flood = 'FLOOD',

  /**
   * Mouvement de terrain
   */
  tremor = 'TREMOR',

  /**
   * Retrait-gonflement des argiles
   */
  clay = 'CLAY',
}

export interface BuildingExtent {
  /**
   * Hauteur moyenne de l'emprise bâtie (m)
   */
  height_avg: number;

  /**
   * Hauteur médianne de l'emprise bâtie (m)
   */
  height_med?: number;

  /**
   * Hauteur minimum de l'emprise bâtie (m)
   */
  height_min: number;

  /**
   * Hauteur maximum de l'emprise bâtie (m)
   */
  height_max: number;

  /**
   * Surface de l'emprise bâtie
   */
  surface: number;
}

/**
 * Emprise bâtie
 */
export interface Building extends BuildingExtent {
  /**
   * Index utilisé lorsqu'il y'a une sélection de parcelles
   */
  index?: number;

  /**
   * ID unique
   */
  id: number;

  /**
   * Indicateur d'immeuble IGH
   */
  igh?: boolean;

  /**
   * Indicateur d'établissement répertorié
   */
  er?: boolean;

  /**
   * Indicateur d'emprise d'équipement
   */
  ee?: boolean;

  /**
   * Année de construction
   */
  year_const?: number;

  /**
   * Année de rehabilitation
   */
  year_rehab?: number;

  /**
   * Période de construction
   * @see http://capgeo.sig.paris.fr/arcgis/rest/services/CapGEO_FDP/FDP_CAPGEO_Plan_Noir_Blanc_sans_voies/MapServer/44?f=pjson
   */
  period_const?: number;

  /**
   * GeoJson geometry
   */
  geometry: AreaGeometry;
}

/**
 * Addresse
 */
export interface Address {
  /**
   * numero de rue
   */
  number: number;

  /**
   * numero de la rue
   */
  street: string;

  /**
   * code postal
   */
  postcode: string;

  /**
   * nom de la commune
   */
  city: string;

  /**
   * code insee de la commune
   */
  citycode: string;

  /**
   * coordonnées
   */
  coordinates: AreaCoordinates;
}

/**
 * Georiques AZI
 */
export interface FloodZone {
  code_insee: string;
  code_national_azi: string;
  date_debut_etude: string;
  date_debut_information: string;
  date_debut_programmation: string;
  date_diffusion: string;
  date_fin_etude: string;
  date_fin_information: string;
  date_fin_programmation: string;
  date_publication_web: string;
  date_realisation: string;
  libelle_azi: string;
  libelle_bassin_risques: string;
  libelle_commentaire: string;
  libelle_commune: string;
  liste_libelle_risque: Risk[];
}

/**
 * CheckoutParams
 */
export interface CheckoutParams {
  email: string;
  name: string;
  address: Stripe.AddressParam;
  parcels: string[];
  message?: string;
  phone?: string;
  href?: string;
}

/**
 * Product
 */
export interface Product extends Stripe.Product {
  price: Stripe.Price;
}

export type ParcelsApiResponse = {
  parcels: Parcel[];
  neighbors: number[];
};

export interface ParcelsApiParams {
  lon: string;
  lat: string;
}

export interface UrbanZone {
  label: string;
  description: string;
  link?: string;
}

export interface Dictionary {
  [id: string]: {
    label: string;
    description: string;
  };
}
export interface RealEstatePrices {
  rental: number;
  selling: number;
}

export interface User {
  id: string;
  email: string;
  name: string;
}
