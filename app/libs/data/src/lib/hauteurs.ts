import { Dictionary } from './types';

export const hauteurs: Dictionary = {
  1: {
    label: 'Inférieur ou égal à 9m',
    description: 'La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette jusqu’à 9 mètres'
  },
  2: {
    label: 'de 10 à 15m',
    description: 'La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette entre 10 et 15 mètres'
  },
  3: {
    label: 'de 16 à 24m',
    description: 'La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette entre 16 et 24 mètres'
  },
  4: {
    label: 'de 25 à 30 m',
    description: 'La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette entre 25 et 30 mètres'
  },
  5: {
    label: 'de 31 à 36m',
    description: 'La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette entre 31 et 36 mètres'
  },
  6: {
    label: '37 m et plus',
    description: 'La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette de 37 mètres et plus'
  },
  7: {
    label: 'Zone naturelle',
    description: 'Zones classées en zone N dans les PLU des communes. Ces zones n’ayant pas vocation à être bâties, elles n’ont pas fait l’objet d’un récolement de la règle de la hauteur des constructions'
  },
  8: {
    label: 'Zone sans limitation de hauteur',
    description: 'Zones ou secteurs pour lesquels la hauteur des constructions n’est pas règlementée, la règle relative à la hauteur des constructions étant facultative dans les règlements des PLU.'
  },
  9: {
    label: 'Hauteur définie selon le plan masse',
    description: 'Secteurs pour lesquels la hauteur règlementaire est reportée sur un plan de masse qui comporte généralement des prescriptions architecturales (hauteur, emprise, implantation…) particulières et plus précises que la règle générale.'
  },
  10: {
    label: 'Hauteur relative aux façades voisines',
    description: 'La hauteur est définie par référence à celle des bâtiments existants voisins. Secteurs généralement constitués de bâtis anciens dans lesquels les hauteurs existantes sont variables ce qui constitue une caractéristique architecturale et urbaine que les communes souhaitent préserver.'
  },
  11: {
    label: 'Hauteur relative à largeur de voie',
    description: 'La hauteur est définie par référence à la largeur de la voie, c’est-à-dire en fonction de la distance entre la construction et l’alignement opposé.'
  },
  12: {
    label: 'La Défense',
    description: 'Secteur de la Défense, pour lequel les hauteurs règlementaires sont spécifiques et correspondent généralement à des immeubles de grande hauteur (IGH)'
  },
  13: {
    label: 'Secteur hors PLU',
    description: 'Correspond aux secteurs non règlementés par les PLU, comme les secteurs du Marais et du 7ème arrondissement à Paris qui sont régis par des plans de sauvegarde et de mise en valeur (PSMV) et le jardin du Luxembourg que la loi « urbanisme et habitat » du 2 juillet 2003 a placé en dehors du champ d’application du PLU parisien.'
  }
};
