import { Dictionary } from './types';

export const emprises: Dictionary = {
  1: {
    label: 'inférieur ou égal à 40%',
    description: 'Le CES maximal défini dans le règlement se situe dans une fourchette de 0 à 40%'
  },
  2: {
    label: '50%',
    description: 'Le CES maximal défini dans le règlement se situe dans une fourchette de 41 à 55%'
  },
  3: {
    label: '60%',
    description: 'Le CES maximal défini dans le règlement se situe dans une fourchette de 56 à 65%'
  },
  4: {
    label: 'entre 70% et 80%',
    description: 'Le CES maximal défini dans le règlement se situe dans une fourchette de 66 à 85%'
  },
  5: {
    label: 'entre 90% et 100%',
    description: 'Le CES maximal défini dans le règlement se situe dans une fourchette de 86 à 100%'
  },
  6: {
    label: 'Zone naturelle',
    description: 'Zones classées en zone N dans les PLU des communes. Ces zones n’ayant pas vocation à être bâties, elles n’ont pas fait l’objet d’un récolement de la règle d’emprise au sol des constructions'
  },
  7: {
    label: 'Emprise non règlementée',
    description: 'Zones ou secteurs pour lesquels l’emprise au sol des constructions n’est pas règlementée, la règle relative à l’emprise au sol des constructions étant facultative dans les règlements des PLU.'
  },
  8: {
    label: 'Emprise définie selon plan masse',
    description: 'Secteurs pour lesquels l’emprise au sol règlementaire est reportée sur un plan de masse qui comporte généralement des prescriptions architecturales (hauteur, emprise, implantation…) particulières et plus précises que la règle générale.'
  },
  9: {
    label: 'Secteur hors PLU',
    description: 'Correspond aux secteurs non règlementés par les PLU, comme les secteurs du Marais et du 7ème arrondissement à Paris qui sont régis par des plans de sauvegarde et de mise en valeur (PSMV) et le jardin du Luxembourg que la loi « urbanisme et habitat » du 2 juillet 2003 a placé en dehors du champ d’application du PLU parisien.'
  }
};
