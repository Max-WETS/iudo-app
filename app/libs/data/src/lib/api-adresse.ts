// Type definitions for API Adresse
// Project: https://geo.api.gouv.fr/adresse
// Definitions by: Maxime Le Breton <https://github.com/maximelebreton>

export interface SearchFeatureProperties {
  /**
   * identifiant de l’adresse (clef d’interopérabilité)
   */
  id: string;

  /**
   * type de résultat trouvé :
   * - housenumber : numéro « à la plaque »
   * - street : position « à la voie », placé approximativement au centre de celle-ci
   * - locality : lieu-dit
   * - municipality : numéro « à la commune »
   */
  type: 'housenumber' | 'street' | 'locality' | 'municipality';

  /**
   * valeur de 0 à 1 indiquant la pertinence du résultat
   */
  score: number;

  /**
   * numéro avec indice de répétition éventuel (bis, ter, A, B)
   */
  housenumber: string;

  /**
   * numéro éventuel et nom de voie ou lieu dit
   */
  name: string;

  /**
   * rue : Non documenté
   */
  street: string;

  /**
   * code postal
   */
  postcode: string;

  /**
   * code INSEE de la commune
   */
  citycode: string;

  /**
   * nom de la commune
   */
  city: string;

  /**
   * nom de l’arrondissement (Paris/Lyon/Marseille)
   */
  district?: string;

  /**
   * n° de département, nom de département et de région
   */
  context: string;

  /**
   * libellé complet de l’adresse
   */
  label: string;

  /**
   * coordonnées géographique en projection légale
   */
  x: number;

  /**
   * coordonnées géographique en projection légale
   */
  y: number;

  /**
   * indicateur d’importance (champ technique)
   */
  importance: number;

  /**
   * @property oldcitycode : code INSEE de la commune ancienne (le cas échéant)
   * @property oldcity : nom de la commune ancienne (le cas échéant)
   */
}

export interface SearchFeature
  extends GeoJSON.Feature<{
    type: 'Point';
    coordinates: [number, number];
  }> {
  properties: SearchFeatureProperties;
}

export interface SearchResult extends GeoJSON.FeatureCollection {
  features: Array<SearchFeature>;
}

export interface SearchParams {
  q: string;
  limit?: number;
  autocomplete?: number;
  lat?: number;
  long?: number;
  type?: SearchFeatureProperties['type'];
  postcode?: string;
  citycode?: string;
}

export interface ReverseParams {
  lat?: number;
  long?: number;
  type?: SearchFeatureProperties['type'];
}
