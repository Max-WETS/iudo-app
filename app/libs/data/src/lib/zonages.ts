import { Dictionary } from './types';

export const zonages: Dictionary = {
  1: {
    label: 'Zone naturelle',
    description: 'Zone classée en zone N (selon les articles L151-11 à L151-13 du CU) dans les PLU des communes. Secteurs équipés ou non, à protéger en raison soit de la qualité des sites, des milieux naturels, des paysages et de leur intérêt, soit de l’existence d’une exploitation forestière, soit de leur caractère d’espaces naturels, selon le code de l’urbanisme.'
  },
  2: {
    label: 'Zone urbaine verte',
    description: 'Zone urbaine utilisée dans certains PLU pour protéger les espaces verts et de loisirs urbains (squares, équipements sportifs, cimetière…). Cette zone couvre généralement des espaces faiblement bâtis dont la fonction sportive ou culturelle, de loisirs ou paysagère doit être préservée et mise en valeur afin d’assurer aux habitants un cadre de vie de qualité. La constructibilité est généralement limitée.'
  },
  3: {
    label: 'Zone pavillonnaire',
    description: 'Zone urbaine d’habitat à dominante pavillonnaire : zone à vocation résidentielle, composée d’habitat individuel. C’est généralement une zone monofonctionnelle où l’activité n’est pas autorisée ou est peu développée.'
  },
  4: {
    label: 'Grand Ensemble',
    description: 'Zone urbaine d’habitat collectif discontinu qui désigne les zones dans les PLU dont la vocation est principalement résidentielle et qui correspondent aux immeubles de logements collectifs construits en ordre discontinu et aux grands ensembles datant des années 1960-1970.'
  },
  5: {
    label: 'Zone mixte dense',
    description: 'Zone urbaine destinées à la fois à l’habitat, aux services et aux activités. Elle correspond en général à un tissu dense. Elle se développe le plus souvent le long des axes et au niveau des centres-villes. Elle se caractérise par des règles de hauteur et d’emprise au sol plus élevées. Et les constructions doivent en principe s’implanter en ordre continu, à l’alignement des voies.'
  },
  6: {
    label: 'Zone mixte semi-dense',
    description: 'Zone urbaine correspondant à une zone intermédiaire composée à la fois de petits collectifs et d’habitat pavillonnaire. On y trouve également quelques commerces et des équipements. C’est un tissu généralement semi-dense. Les règles relatives aux hauteurs et à l’emprise au sol des constructions sont moins élevées qu’en zone mixte dense. Et les bâtiments peuvent être implantés en retrait ou à l’alignement par rapport aux voies.'
  },
  7: {
    label: 'Equipement',
    description: 'Zone urbaine d’équipement public ou collectif d’intérêt général. Tous les PLU ne disposent pas d’une telle zone. Elle correspond aux différents équipements publics ou collectifs d’intérêt général que les communes souhaitent préserver. Ces zones à vocation unique n’autorisent généralement pas les autres destinations (habitat, commerces, bureaux…).'
  },
  8: {
    label: 'Zone d\'activité',
    description: 'Zone urbaine dans les PLU qui correspond aux zones destinées aux activités à vocation économique, industrielle, commerciale ou dédiées aux infrastructures ferroviaires, portuaires et aéroportuaires. Elle se développe généralement le long des infrastructures routières et ferroviaires, mais également le long des canaux. L’habitat n’y est généralement pas autorisé sauf en cas de nécessité de fonctionnement ou de gardiennage.'
  },
  9: {
    label: 'Zone à urbaniser',
    description: 'Identifié comme « zone AU » dans les PLU communaux, elle correspond aux secteurs destinés à être ouverts à l’urbanisation selon le code de l’urbanisme (article R151-20)'
  },
  10: {
    label: 'Zone Agricole',
    description: 'Identifié comme « zone A » dans les PLU communaux, elle correspond aux secteurs de la commune, équipés ou non, à protéger en raison du potentiel agronomique, biologique ou économique des terres agricoles selon le code de l’urbanisme (articles L151-11 à L151-13).'
  },
  11: {
    label: 'Secteur hors PLU',
    description: 'Correspond aux secteurs non règlementés par les PLU, comme les secteurs du Marais et du 7ème arrondissement à Paris qui sont régis par des plans de sauvegarde et de mise en valeur (PSMV) et le jardin du Luxembourg que la loi « urbanisme et habitat » du 2 juillet 2003 a placé en dehors du champ d’application du PLU parisien.'
  }
};
