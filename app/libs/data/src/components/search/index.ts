import { AddressApi } from "@iudo/data";

import state from "./state";
import * as getters from "./getters";
import * as methods from "./methods";
import { toRefs } from "@vue/composition-api";

export type SearchEvent = {
  value: string;
  feature: AddressApi.SearchFeature;
  isDisabled: boolean;
};

export const useSearch = () => ({
  ...toRefs(state),
  ...getters,
  ...methods,
});
