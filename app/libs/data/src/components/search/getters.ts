import { computed } from '@vue/composition-api';
import state from './state';
import { enabledInseeCodes } from '@iudo/data';

export const getSearchInput = computed(() => state.searchInput);

export const getEnabledInseeCodes = computed(() => {
  return Object.keys(enabledInseeCodes);
});

export const getSearchResults = computed(() => state.searchResults);
export const getSearchSelectedIndex = computed(() => state.selectedIndex);
