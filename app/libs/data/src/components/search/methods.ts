import state from './state';
import { searchAddress } from '@iudo/shared/components/address';

import { SearchEvent } from '.';
import { getEnabledInseeCodes } from './getters';
import { sortBy } from 'lodash';
import { searchParcel } from '@iudo/shared/lib/api-parcels';
import EventBus from './eventBus';
import { Analytics } from '@iudo/data';

export const setSearchInput = (value: string) => {
  state.searchInput = value;
  if (state.enableLocalStorage) {
    localStorage.searchInput = state.searchInput;
  }
};
export const clearSearchInput = () => {
  setSearchInput('');
  localStorage.removeItem('searchInput');
};

export const onSearchInputUpdate = (e) => {
  setSearchInput(e.target.value);
};

export const setSearchResults = (results) => {
  state.searchResults = results;
};
export const setSearchSelectedIndex = (index) => {
  state.selectedIndex = index;
};

export const handleSearchInput = async (query: string) => {
  if (query === '') {
    return [];
  }

  const result = await searchAddress({
    q: query,
  });

  const filteredResults = result.features.map(
    (feature) =>
      ({
        value: feature.properties.label,
        isDisabled: !getEnabledInseeCodes.value.includes(
          feature.properties.citycode
        ),
        feature: feature,
      } as SearchEvent)
  );
  // @comment: Dont remember why this line, but it creates a bug with 12 Avenue Pierre Ronsard Arcueil
  // .filter((item) =>
  //   normalizeString(item.value).includes(normalizeString(query))
  // );

  // sort by enabled insee codes first, and score next
  const sortedResults = sortBy(filteredResults, [
    (item) =>
      !getEnabledInseeCodes.value.includes(item.feature.properties.citycode),
    // (item) => item.feature.properties.score,
  ]);

  return sortedResults;
};

export const onSearchInputSubmit = async (
  event: SearchEvent,
  isStandalone: boolean
) => {
  if (event && event.isDisabled) {
    return;
  } else {
    setSearchInput(event.value);

    if (window.analytics) {
      window.analytics.track('Search submited', {
        value: event.value,
      });
    }

    const address = event.feature.properties;
    const { coordinates } = event.feature.geometry;
    const [lon, lat] = coordinates;

    const parcel = await searchParcel({
      lat: lat.toString(),
      lon: lon.toString(),
    });

    if (parcel && address.type === 'housenumber') {
      if (isStandalone) {
        window.location.href = `${process.env.VUE_APP_URL}/?parcels=${parcel.id}&search=${address.label}`;
      } else {
        EventBus.$emit('on-selected-result', { address, parcel });
      }
    } else {
      if (isStandalone) {
        window.location.href = `${process.env.VUE_APP_URL}/@${lat},${lon},16z?search=${address.label}`;
      } else {
        EventBus.$emit('on-selected-result', { address, coordinates });
      }
    }
  }
};

export const normalizeString = function (text) {
  return text
    .normalize('NFD')
    .toLowerCase()
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/[\s-]+/g, ' ');
};

export const getHighlightedMarkup = (words: string, query: string) => {
  const regex = new RegExp(query, 'ig');
  // const normalizedWords = words
  //   .normalize("NFD")
  //   .replace(/[\u0300-\u036f]/gi, "");
  return words.replace(regex, (matchedText) => {
    return `<b class="autocomplete-highlight">${matchedText}</b>`;
  });
};
