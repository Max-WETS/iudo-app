import { reactive } from '@vue/composition-api';

const ENABLE_LOCAL_STORAGE = true;

export interface SearchState {
  enableLocalStorage: boolean;
  searchInput: string;
  searchPlaceholder: string;
  searchResults: [];
  selectedIndex: number;
}

export const initSearchInput = () => {
  if (ENABLE_LOCAL_STORAGE && localStorage.searchInput) {
    return localStorage.searchInput;
  } else {
    return '';
  }
};

export default reactive<SearchState>({
  enableLocalStorage: ENABLE_LOCAL_STORAGE,
  searchInput: initSearchInput(),
  searchPlaceholder: 'Rechercher une adresse',
  searchResults: null,
  selectedIndex: 0,
});
