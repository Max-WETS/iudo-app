import { AddressApi } from '@iudo/data';
import { fetchApi } from '@iudo/shared/utils';

const API_URL = 'https://api-adresse.data.gouv.fr';

/**
 *
 * Utiliser le paramètre q pour faire une recherche plein texte:
 * curl "https://api-adresse.data.gouv.fr/search/?q=8+bd+du+port"
 *
 * Avec limit on peut contrôler le nombre d’éléments retournés:
 * curl "https://api-adresse.data.gouv.fr/search/?q=8+bd+du+port&limit=15"
 *
 * Avec autocomplete on peut désactiver les traitements d’auto-complétion:
 * curl "https://api-adresse.data.gouv.fr/search/?q=8+bd+du+port&autocomplete=0"
 *
 * Avec lat et lon on peut donner une priorité géographique:
 * curl "https://api-adresse.data.gouv.fr/search/?q=8+bd+du+port&lat=48.789&lon=2.789"
 *
 * Les filtres type, postcode (code Postal) et citycode (code INSEE) permettent de restreindre la recherche:
 * curl "https://api-adresse.data.gouv.fr/search/?q=8+bd+du+port&postcode=44380"
 * curl "https://api-adresse.data.gouv.fr/search/?q=paris&type=street"
 *
 *
 * Le retour est un geojson FeatureCollection respectant la spec GeoCodeJSON:
 *
 * {
 *   "type":"FeatureCollection",
 *   "version":"draft",
 *   "features":[
 *       {
 *         "type":"Feature",
 *         "geometry":{
 *             "type":"Point",
 *             "coordinates":[
 *               2.290084,
 *               49.897443
 *             ]
 *         },
 *         "properties":{
 *             "label":"8 Boulevard du Port 80000 Amiens",
 *             "score":0.49159121588068583,
 *             "housenumber":"8",
 *             "id":"80021_6590_00008",
 *             "type":"housenumber",
 *             "name":"8 Boulevard du Port",
 *             "postcode":"80000",
 *             "citycode":"80021",
 *             "x":648952.58,
 *             "y":6977867.25,
 *             "city":"Amiens",
 *             "context":"80, Somme, Hauts-de-France",
 *             "importance":0.6706612694243868,
 *             "street":"Boulevard du Port"
 *         }
 *       }
 *   ],
 *   "attribution":"BAN",
 *   "licence":"ODbL 1.0",
 *   "query":"8 bd du port",
 *   "limit":1
 * }
 *
 * Les coordonnées GeoJSON sont exprimées en WGS-84 (EPSG 4326)
 *
 */
export const searchAddress = async (
  params: AddressApi.SearchParams
): Promise<AddressApi.SearchResult> => {
  return await fetchApi(`${API_URL}/search`, {
    limit: 15,
    ...params,
  } as AddressApi.SearchParams);
};

export const reverseAddress = async (
  params: AddressApi.ReverseParams
): Promise<AddressApi.SearchResult> => {
  return await fetchApi(`${API_URL}/reverse`, {
    limit: 1,
    ...params,
  } as AddressApi.ReverseParams);
};
