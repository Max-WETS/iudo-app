import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as passport from 'passport';
import * as session from 'express-session';
import * as FileStore from 'session-file-store';
import { Strategy as LocalStrategy } from 'passport-local';
import { findUserByEmail } from '../repositories/users';
import { apiUserLogin } from '../services/airtable';
import { User, CheckoutParams } from '@iudo/data';
import { appendFile } from 'fs';

const sessionStore = new (FileStore(session))({
  path: 'var/sessions',
});

export const authRouter = express.Router();

authRouter.use(bodyParser.urlencoded({ extended: false }));
authRouter.use(bodyParser.json());
authRouter.use(
  session({
    secret: ';QgdpH8"8YhyfnAy!K*T8"wsEG2b:[`(G~d7mC8LrFPwWR&e5MwZbX_BTTu8TXd<',
    store: sessionStore,
    cookie: { maxAge: 604800000 /* 7 days */ },
    resave: true,
    saveUninitialized: true,
  })
);
authRouter.use(passport.initialize());
authRouter.use(passport.session());

/**
 * Sign in using Email and Password.
 */
passport.use(
  new LocalStrategy(
    { usernameField: 'email' },
    (email: string, password: string, done: Function) => {
      findUserByEmail(email)
        .then((user) => {
          done(undefined, user);
          apiUserLogin.create(
            [
              {
                fields: {
                  ID_Compte: [user.id],
                  'Enregistrement connexion': user.email,
                  // "Dernière connexion": new Date().toISOString()
                },
              },
            ],
            (err, records) => {
              if (err) {
                console.error(err);
              }
            }
          );
        })
        .catch((message) => {
          done(undefined, false, { message });
        });
    }
  )
);

// tell passport how to serialize the user
passport.serializeUser((user: User, done: Function) => {
  done(null, JSON.stringify(user));
});

// tell passport how to deserialize the user
passport.deserializeUser((user: string, done: Function) => {
  done(null, JSON.parse(user));
});
