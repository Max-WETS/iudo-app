import * as express from 'express';
import * as cors from 'cors';

import parcels from './parcels';
import risks from './risks';
import tiles from './tiles';
import stripe from './stripe';
import session from './user';

const router = express.Router();

router.use('/parcels', cors(), parcels);
router.use('/risks', risks);
router.use('/tiles', tiles);
router.use('/stripe', stripe);
router.use('/user', session);

export default router;
