import * as express from 'express'
import { findRisksByCoordinates, findFloodZonesByCoordinates } from '../helpers/risks'
import { Risk } from '@iudo/data'

const router = express.Router()

/**
 * find risks
 *
 * ids : comma separated list of parcels ids
 *
 * ex : /api/risks?lon=2.28746398397236&lat=48.7569430316427
 */
router.get('', async (req, res) => {
  const { lon, lat } = req.query

  if (lon === undefined) {
    return res.status(400).json({ error: 'Mandatory parameter is missing : lon' })
  }

  if (lat === undefined) {
    return res.status(400).json({ error: 'Mandatory parameter is missing : lat' })
  }

  const p1 = findRisksByCoordinates({ latitude: +lat, longitude: +lon } as Coordinates);
  const p2 = findFloodZonesByCoordinates({ latitude: +lat, longitude: +lon } as Coordinates);

  return Promise.all([p1, p2]).then(([r1, r2]) => {
    // the flood risk is extracted from AZI datasource
    r1.delete(Risk.flood);
    if (r2.length) { r1.add(Risk.flood); }
    return res.json([...r1]);
  });
})

export default router
