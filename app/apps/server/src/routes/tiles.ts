import * as tilestrata from 'tilestrata';
import * as postgismvt from 'tilestrata-postgismvt';
import * as disk from 'tilestrata-disk';
import * as headers from 'tilestrata-headers';
import config from '../config';

const layers = {
  // Cadastre
  pca: {
    fields: 'n_sq_pc',
    minZoom: 14,
    maxZoom: 20,
  },
  // PLU
  plu: {
    fields: 'c_zonage c_zonage_b',
    minZoom: 10,
    maxZoom: 20,
  },
  // Monuments historiques
  mh: {
    table: 'pip',
    type: 'circle',
    fields: 'l_imp',
    geometry: 'ST_PointOnSurface(wkb_geometry)',
    where: "pip.c_imp_niv2='MH'",
    minZoom: 0,
    maxZoom: 20,
  },
  // Patrimoine Périmètre Protection
  ppp: {},
  // Station Transport en Commun
  stc: {
    fields: 'l_station c_nature',
    type: 'circle',
    minZoom: 0,
    maxZoom: 20,
  },
  // Périmètre 800 m Gares MGP Stat Associées
  p8g: {},
};

const server = tilestrata();

for (const layer in layers) {
  server
    .layer(layer)
    .route('*.pbf')
    .use(
      postgismvt({
        lyr: {
          table: 'public.' + layer,
          geometry: 'wkb_geometry',
          type: 'multipolygon',
          srid: 2154,
          buffer: 256,
          resolution: 4096,
          ...layers[layer],
        },
        pgConfig: {
          ...config.postgis,
        },
      })
    )
    .use(disk.cache({ dir: config.tilestrata.cache + '/' + layer }))
    .use(
      headers({
        'Content-Encoding': 'gzip',
        'X-Powered-By': 'Conjecto <https://www.conjecto.com>',
      })
    );
}

export default tilestrata.middleware({ server });
