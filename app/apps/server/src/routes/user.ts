import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as passport from 'passport';
import * as session from 'express-session';
import * as FileStore from 'session-file-store';
import { Strategy as LocalStrategy } from 'passport-local';
import { User, CheckoutParams } from '@iudo/data';
import { formatAsPricePerSquareMeter } from '@iudo/shared/utils';

import { findUserByEmail } from '../repositories/users';
import { authRouter } from '../middlewares/auth';
import { apiUserSearches } from '../services/airtable';
import { findUnitsByParcelIds } from '../repositories/parcels';
import { Nearby } from '@iudo/data';

const router = express.Router();
router.use(authRouter);

/**
 * GET session
 */
router.get('', async (req, res) => {
  if (!req.isAuthenticated()) {
    res.status(401).json({ error: 'Unauthenticated' });
  }

  return res.status(200).json(req.user);
});

/**
 * POST session
 */
router.post('', (req, res, next) => {
  // add fake password
  req.body.password = 'foo';
  // authenticate
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(401).json({ error: info.message });
    }
    req.logIn(user, function (err) {
      if (err) {
        return next(err);
      }
      return res.status(200).json(user);
    });
  })(req, res, next);
});

/**
 * logout
 */
router.get('/logout', async (req, res) => {
  req.logout();
  return res.status(200).json({ success: true, message: 'Logged out.' });
});

/**
 * Save a user search
 */
router.post(
  '/save',
  bodyParser.json({ type: '*/*' }),
  async (
    req: express.Request<{}, {}, CheckoutParams>,
    res: express.Response,
    next
  ) => {
    if (!req.isAuthenticated()) {
      res.status(401).json({ error: 'Unauthenticated' });
    }

    const user: User = req.user as User;
    const params = { ...req.body, ...user };

    const url = new URL(params.href);
    const embed = `${url.origin}/embed${url.pathname}${url.search}&buildings=true`;

    // find land units
    const units = await findUnitsByParcelIds(
      params.parcels.map((n) => parseInt(n))
    );
    if (units.length) {
      const unit = units[0];

      const round = (num: number, dec: number = 0): number => {
        return (
          Math.round((num + Number.EPSILON) * Math.pow(10, dec)) /
          Math.pow(10, dec)
        );
      };

      const landUnitSurface = unit.surface_fisc;
      const landUnitBuildingSurface: number =
        unit.building_extent !== null
          ? round(unit.building_extent.surface)
          : null;
      const landUnitBuildingFootprint =
        landUnitSurface && landUnitBuildingSurface
          ? round((landUnitBuildingSurface * 100) / landUnitSurface)
          : null;
      const landUnitBuildingMedianHeight = unit.building_extent
        ? round(unit.building_extent.height_med, 1)
        : null;
      const urbanZones = unit.urban_zones.map((u) => u.label).join(', ');
      const parcelIdendentifiers = unit.parcels
        .map((p) => (p.csp ? p.csp : p.asp))
        .join(', ');

      const data = {
        Demandeur: [user.id],
        Adresse: params.address.line1,
        Commune: params.address.city,
        'ID-parcelle(s)': params.parcels.join(', '),
        'Lien iudo.map': params.href,
        'Lien embed iudo.map': embed,
        'ID Parcelle': parcelIdendentifiers,
        'Surface unité foncière (m2)': landUnitSurface,
        'Surface batie au sol (m2)': landUnitBuildingSurface,
        // "Caract_emprise au sol": landUnitBuildingFootprint,
        'Hauteur moyenne (m)': landUnitBuildingMedianHeight,
        'Loc_zone PLU': urbanZones,
        'Loc_station de transport': unit.nearby.some(
          (n) => n == Nearby.transport
        )
          ? 'oui'
          : 'non',
        'Loc_monument historique': unit.nearby.some(
          (n) => n == Nearby.historic_monument
        )
          ? 'oui'
          : 'non',
        'Loc_future gare': unit.nearby.some((n) => n == Nearby.gpe_station)
          ? 'oui'
          : 'non',
        Loc_location: unit.prices?.rental,
        Loc_vente: unit.prices?.selling,
      };

      for (let i = 1; i <= unit.buildings.length; i++) {
        if (i > 12) return;
        const building = unit.buildings[i - 1];
        data[`Bati ${i}_id`] = `Bâti ${i}`;
        data[`Bati ${i}_emprise`] = round(building.surface);
        data[`Bati ${i}_h med`] = round(building.height_med, 1);
      }

      apiUserSearches.create([{ fields: data }], (err, records) => {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: err });
        }

        return res.status(200).json({ success: true });
      });
    } else {
      return res.status(404).json({ error: true });
    }
  }
);

export default router;
