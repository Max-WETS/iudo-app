import { ParcelsApiResponse } from '@iudo/data';
import * as express from 'express';
import {
  findParcelByCoordinates,
  findParcelsByIds,
  findParcelIdsByNeighbors,
  findUnitsByParcelIds,
} from '../repositories/parcels';

const router = express.Router();

/**
 * find parcel (lat, lon)
 *
 * ?lat : latitude
 * ?lon : longitude
 *
 * ex : /api/parcels?lat=48.835552&lon=2.240165
 */
router.get('/', async (req, res) => {
  const { lon, lat } = req.query;

  if (lon === undefined) {
    return res
      .status(400)
      .json({ error: 'Mandatory parameter is missing : lon' });
  }

  if (lat === undefined) {
    return res
      .status(400)
      .json({ error: 'Mandatory parameter is missing : lat' });
  }

  const parcel = await findParcelByCoordinates({
    latitude: +lat,
    longitude: +lon,
  } as Coordinates);
  if (parcel != null) {
    return res.json(parcel);
  }

  res.status(404).json({ error: 'Not found' });
});

/**
 * find parcel (ids)
 *
 * ids : comma separated list of ids
 *
 * ex : /api/parcels/415574
 */
router.get('/:ids', async (req, res) => {
  const ids = req.params.ids.split(',').map((n) => parseInt(n));

  const p1 = findParcelsByIds(ids);
  const p2 = findParcelIdsByNeighbors(ids);

  return Promise.all([p1, p2]).then(([parcels, neighbors]) => {
    return res.json({ parcels, neighbors } as ParcelsApiResponse);
  });
});

/**
 * find land units
 *
 * ids : comma separated list of parcels ids
 *
 * ex : /api/parcels/126937,415574,149014/units
 */
router.get('/:ids/units', async (req, res) => {
  const ids = req.params.ids.split(',').map(n => parseInt(n));
  const rows = await findUnitsByParcelIds(ids);
  return res.json(rows);
});

export default router;
