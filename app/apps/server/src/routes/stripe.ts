import * as express from 'express'
import * as bodyParser from 'body-parser'
import { CheckoutParams } from '@iudo/data'
import Stripe from 'stripe'

import { stripe, retrieveCustomer, retrieveProduct } from '../services/stripe'
import { findUnitsByParcelIds } from '../repositories/parcels'
import config from '../config'

const router = express.Router()

/**
 * Get info about price & product
 */
router.get('/product', async (req: express.Request, res: express.Response) => {
  const product = await retrieveProduct();
  return res.status(200).json(product);
});

/**
 * Create a payment intent
 *
 * @see https://stripe.com/docs/billing/prices-guide#bill-customer
 */
router.post('/checkout', bodyParser.json({ type: '*/*' }), async (req: express.Request<{}, {}, CheckoutParams>, res: express.Response, next) => {
  const params = req.body;

  try {

    // prepare invoice metadata
    const metadata: Stripe.Metadata = {
      'url': config.url + params.href
    };

    // get product
    const product = await retrieveProduct();

    // create/update customer
    const customer = await retrieveCustomer({
      name: params.name,
      email: params.email,
      address: params.address,
      phone: params.phone
    });

    // find land units
    const units = await findUnitsByParcelIds(params.parcels.map(n => parseInt(n)));

    // for each, create invoice item
    let count = 0;
    const promises = units.map(unit => {
      const parcels = unit.parcels.map(p => p.csp);
      metadata[`unit_${++count}`] = parcels.join(', ');
      return stripe.invoiceItems.create({
        customer: customer.id,
        price: product.price.id,
        // description: `${product.name} : ${parcels.join(', ')}`
      });
    });

    const items = await Promise.all(promises);
    if (!items.length) {
      return res.status(404).json({ error: 'No land unit found.' });
    }

    if (params.message != null) {
      metadata['message'] = params.message;
    }

    // create invoice
    let invoice = await stripe.invoices.create({
      customer: customer.id,
      metadata
    });

    // finalize invoice to create payment_intent
    invoice = await stripe.invoices.finalizeInvoice(
      invoice.id, {
        expand: ['payment_intent'],
      }
    );

    // return payment_intent
    return res.status(200).json(invoice.payment_intent);

  } catch(err) {
      next(err);
  }
});

export default router
