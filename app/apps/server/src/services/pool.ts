import { Pool } from 'pg'
import config from "../config"

export const pool = new Pool(config.postgis)

/**
 * Execute SQL and return rows
 */
export const execute = async (sql: string, values: any[]) =>  {
 const result = await pool.query(sql, values)
 return result.rows
}

/**
 * Execute SQL and return first result
 */
export const executeOne = async (sql: string, values: any[]) =>  {
  return execute(sql, values)
    .then(rows => rows.length > 0 ? rows[0] : null)
}
