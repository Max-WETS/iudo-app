import Airtable from 'airtable'

const base = new Airtable().base('appIXQaVXkUcXVHZ2');

export const apiInseeInfos = base('API - Liens & Prix');
export const apiUserAccounts = base('V1.1 - Comptes');
export const apiUserSearches = base('V1.1 - Requêtes adresses');
export const apiUserLogin = base('V1.1 - Activités connexions');

