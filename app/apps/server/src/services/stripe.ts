import Stripe from 'stripe'
import config from '../config'
import { Product } from '@iudo/data';

export const stripe = new Stripe(config.stripe.secret_key, {
  apiVersion: '2020-03-02',
})

/**
 * Try to retrieve a customer by its email :
 * - update if if exists
 * - create it if not exists
 */
export const retrieveCustomer = async (params: Stripe.CustomerCreateParams): Promise<Stripe.Customer> =>  {
  if (!params.email) {
    throw 'You must provide an email.';
  }
  return stripe.customers.list({ email: params.email, limit: 1 }).then(list => {
    if (list.data.length > 0) {
      const customer = list.data[0];
      return stripe.customers.update(customer.id, params);
    };
    return stripe.customers.create(params);
  });
}


/**
 * Retrieve the main product
 */
export const retrieveProduct = async (): Promise<Product> =>  {

  // find max 10 prices for this product
  const prices = await stripe.prices.list({
      product: config.stripe.product_id,
      type: 'one_time',
      active: true,
      limit: 10,
      expand: ['data.product']
    }).then(res => res.data);

  if (prices.length > 0) {
    // get the active price_id from metadata
    const product = prices[0].product as Stripe.Product;
    const priceId = product.metadata?.price_id;

    let price: Stripe.Price = null;
    if (priceId) {
      // try to find active price over the prices
      for (let i = 0; i < prices.length; i++) {
        if (prices[i].id == priceId) {
          price = prices[i];
          delete price.product;
          continue;
        }
      }
    } else {
      price = prices[0]
    }

    // return product with the right price, if found
    return {
      ...product as Stripe.Product,
      price: price ? price : null
    } as Product;
  }

  return null;
}
