import { Risk, FloodZone } from '@iudo/data'
import * as fetch from 'node-fetch'
import * as https from 'https'

const unsecureHttpsAgent = new https.Agent({
  rejectUnauthorized: false,
});

/**
* Find risks by coordinates, using Georisque API

* API : georisque
* ex : https://www.georisques.gouv.fr/api/v1/gaspar/risques?rayon=150&latlon=2.30694186877035%2C48.8219642636189&page=1&page_size=10
*
* @param {Array} ids
*/
export const findRisksByCoordinates = async(coordinates: Coordinates, radius=500): Promise<Set<Risk>> => {
  const latlon = coordinates.longitude + "," + coordinates.latitude //oui, c'est inversé sur l'API georisque !
  const url = 'https://www.georisques.gouv.fr/api/v1/gaspar/risques'
  const params = new URLSearchParams({ rayon: String(radius), latlon, page: "1", page_size: "10" })

  return fetch(url + '?' + params, {agent: unsecureHttpsAgent})
    .then(res => res.json())
    .then(body => body.data.map(row => row['risques_detail'].map(d => mapToRisk(d['num_risque'])).filter(x => !!x)).flat())
    .then(risks => new Set(risks))
}

/**
* Find risks by coordinates, using Georisque API

* API : georisque
* ex : https://www.georisques.gouv.fr/api/v1/gaspar/risques?rayon=150&latlon=2.30694186877035%2C48.8219642636189&page=1&page_size=10
*
* @param {Array} ids
*/
export const findFloodZonesByCoordinates = async(coordinates: Coordinates, radius=500): Promise<FloodZone[]> => {
  const latlon = coordinates.longitude + "," + coordinates.latitude //oui, c'est inversé sur l'API georisque !
  const url = 'https://www.georisques.gouv.fr/api/v1/gaspar/azi'
  const params = new URLSearchParams({ rayon: String(radius), latlon, page: "1", page_size: "10" })

  return fetch(url + '?' + params, {agent: unsecureHttpsAgent})
    .then(res => res.json())
    .then(body => {
      // build flood zone
      const zones = body.data.map(row => {
        return {
          ...row,
          liste_libelle_risque: row['liste_libelle_risque'].map(r => mapToRisk(r['num_risque'])).filter(x => !!x)
        } as FloodZone;
      });
      // filter to get unique entry for a given code_national_azi
      return zones.filter((e, i) => zones.findIndex(a => a["code_national_azi"] === e["code_national_azi"]) === i);
    })
}

/**
 * Map a Georisques num to a Risk
 *
 * @param num
 */
const mapToRisk = (num: string): Risk => {
  switch (num)  {
    case "135": return Risk.cavity;
    case "140": return Risk.flood;
    case "134": return Risk.tremor;
    case "157": return Risk.clay;
  }
  return null;
}



