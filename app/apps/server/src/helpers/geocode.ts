import * as fetch from 'node-fetch';
import { AddressApi } from '@iudo/data';

/**
 * Fetch postal address for given coordinates
 *
 * API : Base Adresse Nationale
 * ex : https://api-adresse.data.gouv.fr/reverse/?lon=-1.68329999118104&lat=47.986021356717
 *
 * @param coordinates
 */
export const fetchAddressFromCoordinates = async (
  coordinates: Coordinates
): Promise<AddressApi.SearchFeatureProperties> => {
  const url = 'https://api-adresse.data.gouv.fr/reverse/';
  const params = new URLSearchParams({
    lat: String(coordinates.latitude),
    lon: String(coordinates.longitude),
  });
  return fetch(url + '?' + params)
    .then((res) => res.json())
    .then((obj: AddressApi.SearchResult) => {
      if (obj.features[0].properties !== undefined) {
        return obj.features[0].properties;
      }
      return null;
    });
};
