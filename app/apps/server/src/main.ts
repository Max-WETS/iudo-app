/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import * as express from 'express';
import * as history from 'connect-history-api-fallback';
import * as compression from 'compression';
import * as cors from 'cors';
import routes from './routes';
import config from './config';

const app = express();

// compress all HTTP responses
app.use(compression());

// use to serve front files
app.use(express.static('dist/apps/front'));
app.use('/component', express.static('dist/apps/front-component'));

// cors middleware
app.use(cors());

// api
app.use('/api', routes);

// history middleware
app.use(history({ disableDotRule: true }));

// use to handle rewrited paths from previous middleware
app.use('/index.html', express.static('dist/apps/front/index.html'));

const { port } = config;
const server = app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`);
});
server.on('error', console.error);
