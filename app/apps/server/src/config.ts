// @see https://nx.dev/angular/cli/overview#loading-environment-variables

const config = {
  production: false,
  url: process.env.VUE_APP_URL,
  postgis: {
    host: process.env.POSTGIS_HOST,
    port: process.env.POSTGIS_PORT,
    database: process.env.POSTGIS_DBNAME,
    user: process.env.POSTGIS_USER,
    password: process.env.POSTGIS_PASS,
  },
  tilestrata: {
    cache: process.env.TILESTRATA_CACHE,
  },
  stripe: {
    publishable_key: process.env.VUE_APP_STRIPE_PUBLISHABLE_KEY,
    secret_key: process.env.STRIPE_SECRET_KEY,
    webhook_secret: process.env.STRIPE_WEBHOOK_SECRET,
    payment_methods: [
      // 'ach_credit_transfer', // usd (ACH Credit Transfer payments must be in U.S. Dollars)
      // 'alipay', // aud, cad, eur, gbp, hkd, jpy, nzd, sgd, or usd.
      // 'bancontact', // eur (Bancontact must always use Euros)
      'card', // many (https://stripe.com/docs/currencies#presentment-currencies)
      // 'eps', // eur (EPS must always use Euros)
      // 'ideal', // eur (iDEAL must always use Euros)
      // 'giropay', // eur (Giropay must always use Euros)
      // 'multibanco', // eur (Multibanco must always use Euros)
      // 'sepa_debit', // Restricted. See docs for activation details: https://stripe.com/docs/sources/sepa-debit
      // 'sofort', // eur (SOFORT must always use Euros)
      // 'wechat', // aud, cad, eur, gbp, hkd, jpy, sgd, or usd.
    ],
    product_id: process.env.STRIPE_PRODUCT_ID,
  },
  // Server port.
  port: process.env.PORT || 3333,
};

export default config;
