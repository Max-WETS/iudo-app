/* eslint-disable @typescript-eslint/camelcase */
/* eslint-disable @typescript-eslint/no-use-before-define */

import { Building, BuildingExtent } from '@iudo/data';
import { execute } from '../services/pool';

/**
 * Find buildings by parcel ids
 *
 * @param {Array} ids
 */
export const findBuildingsByParcelIds = async (
  ids: (string | number)[]
): Promise<Building[]> => {
  const promises = ids.map(id => findBuildingsByParcelId(id));
  return Promise.all(promises).then(b => {
    // flatten
    return [].concat.apply([], b);
  });
};

/**
 * Find buildings by parcel id
 *
 * The build must overlaps more than 80% to be considered as in the parcel
 *
 * @param {Array} ids
 */
export const findBuildingsByParcelId = async (
  id: (string | number)
): Promise<Building> => {

  const table = id.toString().startsWith('75') ? 'ebp' : 'ebm';
  const sql = `WITH _union AS (SELECT ST_Union(pca.wkb_geometry) as geom FROM pca WHERE pca.n_sq_pc = $1) \
    SELECT ${table}.*,\
      ST_AsGeoJSON(ST_Transform(${table}.wkb_geometry, 4326)) as feature, \
      ST_Area(${table}.wkb_geometry) as sqm \
    FROM ${table} \
    JOIN _union ON \
      ST_Intersects(_union.geom, ${table}.wkb_geometry) \
      AND (ST_Area(ST_Intersection(_union.geom, ${table}.wkb_geometry))/ST_Area(${table}.wkb_geometry)) > .8
    ORDER BY sqm DESC`;

    return execute(sql, [id]).then((rows) => rows.map(mapToBuilding))
};

/**
 * Compute a global building extent from a list of extents
 */
export const computeBuildingExtent = (
  extents: BuildingExtent[]
): BuildingExtent => {
  const surface = extents.reduce(function (surface, extent) {
    return surface + extent.surface;
  }, 0);
  return {
    height_avg: extents.reduce(function (avg, extent, _, { length }) {
      return avg + extent.height_avg / length;
    }, 0),
    height_max: extents.reduce(function (max, extent) {
      return extent.height_max > max ? extent.height_max : max;
    }, 0),
    height_min: extents.reduce(function (min, extent) {
      return extent.height_min < min || min == 0 ? extent.height_min : min;
    }, 0),
    height_med:
      surface > 0
        ? extents.reduce(function (prev, extent) {
            return prev + extent.height_med * extent.surface;
          }, 0) / surface
        : 0,
    surface,
  };
};

/**
 * Map obj to Building
 */
const mapToBuilding = (row: object): Building => {
  return {
    id: +row['n_sq_eb'],
    year_const: row['an_const'] ? +row['an_const'] : null,
    year_rehab: row['an_rehab'] ? +row['an_rehab'] : null,
    period_const: row['c_perconst'] ? +row['c_perconst'] : null,
    geometry: JSON.parse(row['feature']),
    ...mapToBuildingExtent(row),
  };
};

/**
 * Map obj to Building
 */
const mapToBuildingExtent = (row: object): BuildingExtent => {
  return {
    height_avg: +row['h_moy'],
    height_med: +row['h_med'],
    height_min: +row['h_min'],
    height_max: +row['h_max'],
    surface: +(row['surface'] ? row['surface'] : row['sqm']),
  };
};
