import { execute, executeOne } from '../services/pool';
import { Nearby, Dictionary, RealEstatePrices, UrbanZone } from '@iudo/data';
import { emprises, hauteurs, zonages } from '@iudo/data';
import { apiInseeInfos } from '../services/airtable';

/**
 * Find urban zones by parcel ids
 *
 * @param {Array} ids
 */
export const findUrbanZonesByParcelIds = async (
  ids: (string | number)[]
): Promise<UrbanZone[]> => {
  const sql =
    'SELECT DISTINCT c_zonage, c_zonage_b \
  FROM plu \
  JOIN pca ON ST_Intersects(plu.wkb_geometry, ST_Buffer(pca.wkb_geometry,-2)) \
  WHERE pca.n_sq_pc = ANY($1::integer[])';
  return execute(sql, [ids]).then((rows) =>
    rows.map((row) => {
      return { 
        ...zonages[row['c_zonage']],
        label: row['c_zonage_b'],
      } as UrbanZone;
    })
  );
};

/**
 * Find nearby by parcel ids
 *
 * @param {Array} ids
 */
export const findNearbyByParcelIds = async (
  ids: (string | number)[]
): Promise<Nearby[]> => {
  const sql =
    "SELECT \
    MIN(ST_Distance(pca.wkb_geometry, stc.wkb_geometry)) AS transport, \
    MIN(ST_Distance(pca.wkb_geometry, pip.wkb_geometry)) AS historic_monument, \
    MIN(ST_Distance(pca.wkb_geometry, ST_Centroid(p8g.wkb_geometry))) AS gpe_station \
  FROM pca \
  LEFT JOIN stc ON ST_DWithin(pca.wkb_geometry, stc.wkb_geometry, 500) \
  LEFT JOIN pip ON ST_DWithin(pca.wkb_geometry, pip.wkb_geometry, 500) AND pip.c_imp_niv2='MH' \
  LEFT JOIN p8g ON ST_Intersects(pca.wkb_geometry, p8g.wkb_geometry) \
  WHERE pca.n_sq_pc = ANY($1::integer[])";

  const row = await executeOne(sql, [ids]);
  const list = [] as Nearby[];
  if (row != null) {
    if (row['transport']) {
      list.push(Nearby.transport);
    }
    if (row['historic_monument']) {
      list.push(Nearby.historic_monument);
    }
    if (row['gpe_station']) {
      list.push(Nearby.gpe_station);
    }
  }

  return list;
};

/**
 * Find footprint rules by parcel ids
 *
 * @param {Array} ids
 */
export const findFootprintRulesByParcelIds = async (
  ids: (string | number)[]
): Promise<Dictionary[]> => {
  const sql =
    'SELECT DISTINCT c_emprise \
  FROM emp \
  JOIN pca ON ST_Intersects(emp.wkb_geometry, ST_Buffer(pca.wkb_geometry,-2)) \
  WHERE pca.n_sq_pc = ANY($1::integer[]) AND c_emprise IS NOT NULL';
  return execute(sql, [ids]).then((rows) =>
    rows.map(row => {
      return emprises[row['c_emprise']];
    })
  );
};

/**
 * Find height rules by parcel ids
 *
 * @param {Array} ids
 */
export const findHeightRulesByParcelIds = async (
  ids: (string | number)[]
): Promise<Dictionary[]> => {
  const sql =
    'SELECT DISTINCT c_hauteur \
  FROM hau \
  JOIN pca ON ST_Intersects(hau.wkb_geometry, ST_Buffer(pca.wkb_geometry,-2)) \
  WHERE pca.n_sq_pc = ANY($1::integer[]) AND c_hauteur IS NOT NULL';
  return execute(sql, [ids]).then((rows) =>
    rows.map(row => {
      return hauteurs[row['c_hauteur']];
    })
  );
};

/**
 * Find additional infos by insee code, from Airtable
 *
 * @param {string} insee
 */
export const findAdditionalInfosByInsee = async (
  insee: (string | number)
): Promise<{ prices: RealEstatePrices, pluLink: string }> => {
  if (String(insee).indexOf("75") == 0) {
    insee = "75056";
  }
  return new Promise((resolve, reject) => {
    apiInseeInfos.select({
      maxRecords: 1,
      fields: ["location_prix-moyen", "vente_prix-moyen", "lien_plu"],
      filterByFormula: `code_insee = '${insee}'`
    }).eachPage(function page(records, fetchNextPage) {
      if (records.length > 0) {
        resolve({
          prices: {
            rental: +records[0].get("location_prix-moyen"),
            selling: +records[0].get("vente_prix-moyen")
          } as RealEstatePrices,
          pluLink: records[0].get("lien_plu")
        });
      }
      resolve(null);
    }, function done(err) {
      console.log(err)
      if (err) { reject(err.message); return; }
      resolve(null);
    });
  });
};
