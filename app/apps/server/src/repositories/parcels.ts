/* eslint-disable @typescript-eslint/camelcase */
/* eslint-disable @typescript-eslint/no-use-before-define */

import { Parcel, LandUnit, AreaCoordinates } from '@iudo/data';
import { execute, executeOne } from '../services/pool';
import { findBuildingsByParcelIds, computeBuildingExtent } from './buildings';
import { findNearbyByParcelIds, findFootprintRulesByParcelIds, findHeightRulesByParcelIds, findUrbanZonesByParcelIds, findAdditionalInfosByInsee } from './miscs';

/**
 * Find the nearest parcel from the given coordinates
 *
 * @param lat
 * @param lon
 * @param precision : precision in meters
 */
export const findParcelByCoordinates = async (
  coordinates: Coordinates,
  precision = 20
): Promise<Parcel> => {
  const sql =
    'SELECT pca.*, \
    ST_AsGeoJSON(ST_Transform(wkb_geometry, 4326)) as feature, \
    ST_Distance(wkb_geometry, ST_Transform(ST_SetSRID(ST_MakePoint($1, $2), 4326), 2154)) AS distance \
  FROM pca \
  ORDER BY wkb_geometry <-> ST_Transform(ST_SetSRID(ST_MakePoint($1, $2), 4326), 2154) \
  LIMIT 1';

  let rows = execute(sql, [coordinates.longitude, coordinates.latitude]);
  if (precision !== null) {
    rows = rows.then((rows) => rows.filter((row) => row.distance < precision));
  }

  return rows
    .then((rows) => rows.map(mapToParcel))
    .then((parcels) => (parcels.length > 0 ? parcels[0] : null));
};

/**
 * Find parcels by ids
 */
export const findParcelsByIds = async (
  ids: number[]
): Promise<Parcel[]> => {
  const sql =
    'SELECT pca.*, ST_AsGeoJSON(ST_Transform(wkb_geometry, 4326)) as feature \
    FROM pca \
    WHERE n_sq_pc = ANY($1::integer[])';

  const parcels = execute(sql, [ids]).then((rows) =>
    rows
      .map(mapToParcel)
      .sort((a, b) => ids.indexOf(a.id) > ids.indexOf(b.id) ? 1 : -1)
  );

  // add protected properties
  return parcels.then((parcels) => {
    if (parcels.length > 1) {
      parcels = parcels.map(async (parcel: Parcel) =>  {
        parcel.protected = !(await isSingleUnit(ids.filter(id => id != parcel.id)));
        return parcel;
      })
    }
    return Promise.all(parcels);
  });
};

/**
 * Find neighbor parcels
 *
 * @param {Array} ids
 */
export const findParcelIdsByNeighbors = async (
  ids: number[]
): Promise<number[]> => {
  const sql =
    'WITH _selected as (select wkb_geometry as s_geom from pca where n_sq_pc = ANY($1::integer[])) \
    SELECT DISTINCT pca.n_sq_pc \
    FROM pca, _selected \
    WHERE NOT(n_sq_pc = ANY($1::integer[])) and ST_Intersects(wkb_geometry, s_geom)';
  return execute(sql, [ids]).then((rows) => rows.map(row => parseInt(row["n_sq_pc"])));
};

/**
 * Find units by parcels ids
 *
 * @param {Array} ids
 */
export const findUnitsByParcelIds = async (
  ids: number[]
): Promise<LandUnit[]> => {
  const sql =
    'SELECT \
      array_agg(s2.n_sq_pc) parcels, \
      SUM(shape_area) shape_area, \
      SUM(m2_dgfip) m2_dgfip, \
      ST_AsGeoJSON(ST_Transform(s1.geom, 4326)) feature \
  FROM \
    (\
      SELECT (dump).path[1] id_grp, (dump).geom geom \
      FROM \
      (\
        SELECT ST_Dump(ST_Union(wkb_geometry)) dump \
        FROM "pca" \
        WHERE n_sq_pc = ANY($1::integer[])\
      ) ts0\
    ) s1, \
    "pca" s2 \
  WHERE \
    s1.geom && s2.wkb_geometry \
    AND s2.n_sq_pc = ANY($1::integer[]) \
    AND st_intersects(s1.geom, s2.wkb_geometry) \
  GROUP BY \
    s1.id_grp, s1.geom';

  return execute(sql, [ids]).then((rows) => Promise.all(
    rows.map(row => {
      const parcels = (row['parcels'] as number[])
        .sort((a, b) => ids.indexOf(a) > ids.indexOf(b) ? 1 : -1);
      return mapToLandUnit(parcels, row);
    })
  ));
};

/**
 * Find coordinates of list of parcel id, based on the
 * centroid of the contained numvoie
 *
 * @param ids
 */
export const findCoordinatesByParcelIds = async (
  ids: number[]
): Promise<AreaCoordinates> => {

  // 1. try with numvoie centroid
  const sql = 'WITH _centroid as ( \
    SELECT DISTINCT ST_Centroid(ST_Union(nvo.wkb_geometry)) as geom \
      FROM nvo, pca \
      WHERE pca.n_sq_pc = ANY($1::integer[]) \
      AND ST_Contains(ST_Buffer(pca.wkb_geometry, 1), nvo.wkb_geometry) \
    ) \
    SELECT \
      ST_X(ST_Transform(_centroid.geom, 4326)) as lon, \
      ST_Y(ST_Transform(_centroid.geom, 4326)) as lat \
    FROM _centroid \
    WHERE geom IS NOT NULL';

  let row = await executeOne(sql, [ids]);

  if (row == null) {
    // 2. take the parcel centroid
    const sql = 'WITH _centroid as ( \
      SELECT DISTINCT ST_PointOnSurface(ST_Union(pca.wkb_geometry)) as geom \
        FROM pca \
        WHERE pca.n_sq_pc = ANY($1::integer[]) \
      ) \
      SELECT \
        ST_X(ST_Transform(_centroid.geom, 4326)) as lon, \
        ST_Y(ST_Transform(_centroid.geom, 4326)) as lat \
      FROM _centroid';
      row = await executeOne(sql, [ids]);
  }

  if (row != null) {
    return <AreaCoordinates> {
      latitude: row['lat'],
      longitude: row['lon']
    };
  }

  return null;
}

/**
 * True if the given list of unit constitute a single unit
 *
 * @param {Array} ids
 */
export const isSingleUnit = async (
  ids: number[]
): Promise<Boolean> => {
  const sql =
    'SELECT COUNT(dump) FROM  ( \
      SELECT ST_Dump(ST_Union(wkb_geometry)) dump \
      FROM "pca" \
      WHERE n_sq_pc = ANY($1::integer[]) \
    ) ts0';
  return executeOne(sql, [ids]).then((row) => {
    return parseInt(row['count']) == 1;
  });
};

/**
 * Map SQL row to Parcel
 */
const mapToParcel = (row: object): Parcel => {

  const parcel:Parcel = {
    id: +row['n_sq_pc'],
    insee: +row['c_cainsee'],
    section: row['c_sec'],
    number: +row['n_pc'],
    surface: +row['shape_area'],
    surface_fisc: +row['m2_dgfip'],
    asp: row['c_asp'],
    csp: row['c_csp'],
    type: row['c_typepc'],
    geometry: JSON.parse(row['feature'])
  };

  if (row['lon'], row['lat']) {
    parcel.coordinates = <AreaCoordinates> {
      latitude: row['lat'],
      longitude: row['lon']
    };
  }

  return parcel;
};

/**
 * Map SQL rows to LandUnits[]
 */
const mapToLandUnit = async (ids: number[], row: object): Promise<LandUnit> => {
  const p1 = findBuildingsByParcelIds(ids);
  const p2 = findUrbanZonesByParcelIds(ids);
  const p3 = findNearbyByParcelIds(ids);
  const p4 = findCoordinatesByParcelIds(ids);
  const p5 = findFootprintRulesByParcelIds(ids);
  const p6 = findHeightRulesByParcelIds(ids);
  let p7 = Promise.resolve(null);

  const parcels = await findParcelsByIds(ids);
  if (parcels.length > 0) {
    const insee = parcels[0].insee;
    p7 = findAdditionalInfosByInsee(insee);
  }

  return Promise.all([p1, p2, p3, p4, p5, p6, p7]).then(([buildings, urban_zones, nearby, coordinates, footprint_rules, height_rules, infos]) => {
    const building_extent = computeBuildingExtent(buildings);

    let prices = null;
    if (infos) {
      prices = infos.prices;
      urban_zones[0].link = infos.pluLink;
    }

    return {
      surface: +row['shape_area'],
      surface_fisc: +row['m2_dgfip'],
      geometry: JSON.parse(row['feature']),
      coordinates,
      parcels,
      building_extent,
      buildings,
      urban_zones,
      nearby,
      footprint_rules,
      height_rules,
      prices,
      risks: '/api/risks?lon=' + coordinates.longitude + '&lat=' + coordinates.latitude,
    };
  });
};
