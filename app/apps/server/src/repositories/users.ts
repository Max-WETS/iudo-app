import { User } from '@iudo/data';
import { apiUserAccounts } from '../services/airtable';

/**
 * Find user by email
 *
 * @param {string} email
 */
export const findUserByEmail = async (
  email: string
): Promise<User> => {
  return new Promise((resolve, reject) => {
    apiUserAccounts.select({
      maxRecords: 1,
      fields: ["Mail", "Nom", "Statut"],
      filterByFormula: `AND(Mail = '${email}', Statut = 'Actif')`
    }).eachPage(function page(records, fetchNextPage) {
      if (records.length > 0) {
        resolve({
          id: records[0].id,
          email: records[0].get("Mail"),
          name: records[0].get("Nom")
        } as User);
        return;
      }
      reject("User not found");
    }, function done(err) {
      if (err) { reject(err.message); return; }
      reject("User not found");
    });
  });
};
