import "./init";
import Vue from "vue";
import router from "./router";
import App from "./App.vue";

import Notifications from "vue-notification";
import vClickOutside from "v-click-outside";
import "sass-modern-mq/debug.js";

Vue.use(vClickOutside);

Vue.use(Notifications);

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
