import { action } from "@storybook/addon-actions";
import { withKnobs, select, boolean } from "@storybook/addon-knobs";

import Btn from "@front/components/Btn.vue";
// import Search from "@front/components/Search.vue";
import TheMap from "@front/components/TheMap.vue";
import TheHeader from "@front/components/TheHeader.vue";
import Block from "@front/components/Block.vue";

import "@front/scss/styles.scss";
import "@front/scss/_storybook.scss";

export default {
  title: "Components",
  decorators: [withKnobs],
};

const label = "Button colors";
const options = {
  Primary: { primary: true },
  Secondary: { secondary: true },
};
const defaultValue = options.Primary;

export const _Button = () => ({
  components: { Btn },
  template: `<btn v-bind="[bindColors, bindRounded, bindOutlined]">Hello</btn>`,
  props: {
    bindColors: {
      //@ts-ignore
      default: select(label, options, defaultValue),
    },
    bindRounded: {
      default: { rounded: boolean("Rounded", false) },
    },
    bindOutlined: {
      default: { outlined: boolean("Outlined", false) },
    },
  },
});

// export const _Search = () => ({
//   components: { Search },
//   template: `<search v-bind="[bindWithButton]" />`,
//   props: {
//     bindWithButton: {
//       default: { withButton: boolean("With button", false) },
//     },
//   },
// });

export const _TheMap = () => ({
  components: { TheMap },
  template: `<the-map />`,
});

export const _TheHeader = () => ({
  components: { TheHeader },
  template: `<the-header />`,
});

export const _block = () => ({
  components: { Block },
  template: `<block :title="title" :items="items" />`,
  props: {
    title: {
      default: "Caractéristiques",
    },
    items: {
      default: [
        {
          key: "Surface parcelle",
          value: "537m²",
        },
        {
          key: "Surface bâtie",
          value: "112m²",
        },
        {
          key: "Emprise au sol actuelle",
          value: "21%",
        },
        {
          key: "Hauteur médiane",
          value: "7,5m",
        },
      ],
    },
  },
});

// export const withText = () => ({
//   components: { MyButton },
//   template: '<my-button @click="action">Hello Button</my-button>',
//   methods: { action: action('clicked') },
// });

// export const withSomeEmoji = () => ({
//   components: { MyButton },
//   template: '<my-button>😀 😎 👍 💯</my-button>',
// });
