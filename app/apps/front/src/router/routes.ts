import { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import Embed from "../views/Embed.vue";
import Login from "../views/Login.vue";
// import { scrollToElement } from "@iudo/shared/utils";
// import { fetchUserAuth, setIsLoggedUser } from "@front/store/user/methods";
import { getRoutePath, isEmbed, isLoggedUser } from "@front/store/user/getters";
import router from ".";
import { nextTick } from "@vue/composition-api";

export enum ScrollIds {
  ADDRESS = "address",
  INFORMATIONS = "informations",
  POTENTIAL = "potential",
  FORM = "form",
}

export type MapRouteParams = {
  lat?: string;
  lng?: string;
  zoom?: string;
  step?:
    | ScrollIds.ADDRESS
    | ScrollIds.INFORMATIONS
    | ScrollIds.POTENTIAL
    | ScrollIds.FORM;
};

export type RouteQuery = {
  parcels?: string;
  search?: string;
};

export const routes: Array<RouteConfig> = [
  {
    path: "/@:lat?,:lng?,:zoom?z/:step?",
    alias: ["/", "/@"],
    name: "Home",
    // component: () => import(/* webpackChunkName: "Home" */ "../views/Home.vue"),
    component: Home,
    props(route) {
      //const parcels = route.params.parcels || ''
    },
    meta: {
      header: true,
    },
  },
  {
    path: "/embed/@:lat?,:lng?,:zoom?z/:step?",
    alias: ["/embed/", "/embed/@"],
    name: "Embed",
    // component: () => import(/* webpackChunkName: "Home" */ "../views/Home.vue"),
    component: Embed,
    props(route) {
      //const parcels = route.params.parcels || ''
    },
    meta: {
      header: false,
    },
  },
  {
    // path: "/focus",
    path: "/focus/@:lat?,:lng?,:zoom?z/:step?",
    name: "Focus",
    alias: ["/focus/", "/focus/@"],
    // component: () =>
    //   import(/* webpackChunkName: "Focus" */ "../views/Focus.vue"),
    component: Home,
    meta: {
      focus: true,
      requiresAuth: true,
      header: true,
    },
    props(route) {},
  },
  {
    path: "/embed/focus/@:lat?,:lng?,:zoom?z/:step?",
    name: "EmbedFocus",
    alias: ["/embed/focus/", "/embed/focus/@"],
    // component: () =>
    //   import(/* webpackChunkName: "Focus" */ "../views/Focus.vue"),
    component: Embed,
    meta: {
      focus: true,
      requiresAuth: false,
      header: false,
    },
    props(route) {},
  },
  {
    path: "/login/:email",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "Login" */ "../views/Login.vue"),
    props: true,
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: "*",
    name: "404",

    component: () => import(/* webpackChunkName: "404" */ "../views/404.vue"),
  },
  // {
  //   path: "/succeeded",
  //   name: "CheckoutSuccess",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(
  //       /* webpackChunkName: "CheckoutSuccess" */ "../views/CheckoutSuccess.vue"
  //     ),
  // },

  // {
  //   path: "/about",
  //   name: "About",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "About" */ "../views/About.vue"),
  // },
];
