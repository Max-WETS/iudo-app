import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import { routes, MapRouteParams, RouteQuery } from "./routes";
import { useUser } from "@front/store/user";

import {
  addToUserParcels,
  // checkUserParcelsNeighbors,
  checkUserLandUnit,
  fetchUserAuth,
  setRoute,
  setUserNeighbors,
} from "@front/store/user/methods";
import {
  hasUserParcels,
  getUserLandUnit,
  isLoggedUser,
} from "@front/store/user/getters";
import { getParcelsArrayFromQuery } from "@front/store/parcels";
import { fetchParcels } from "@iudo/shared/lib/api-parcels";
import {
  mapFitToGeometry,
  isMapReady,
  waitForMap,
  getMapCenter,
  getMapZoom,
  setBuildingsState,
  setPluState,
  setSatelliteState,
  setCadastreState,
} from "@front/store/map";
import { setSearchInput } from "@iudo/shared/components/search/methods";
import { watch, computed } from "@vue/composition-api";
import { scrollToElement } from "@iudo/shared/utils";
import { updateUserBuildingsSource } from "@front/store/map/layers/buildings";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export const fillStateFromRoute = async () => {
  const parcelsQuery = (router.currentRoute.query as RouteQuery).parcels;
  const searchQuery = (router.currentRoute.query as RouteQuery).search;
  if (searchQuery) {
    setSearchInput(searchQuery);
  }
  if (parcelsQuery && parcelsQuery.length) {
    const parcelsIds = getParcelsArrayFromQuery(parcelsQuery);

    const { parcels, neighbors } = await fetchParcels(parcelsIds);

    addToUserParcels(parcels);
    setUserNeighbors(neighbors);
    // await checkUserParcelsNeighbors(parcelsIds);
    await checkUserLandUnit(parcelsIds);
    await waitForMap();
    updateUserBuildingsSource();

    if (hasUserParcels.value) {
      // mapFitToGeometry(getUserLandUnit.value.geometry, 0);
    }
  }
  const isStringBoolean = (value: string | string[]) => {
    if (
      value &&
      (value.toString() === "true" || value.toString() === "false")
    ) {
      return true;
    } else {
      return false;
    }
  };
  const getStringAsBoolean = (value: string | string[]): boolean => {
    if (value && value.toString() === "true") {
      return true;
    } else if (value && value.toString() === "false") {
      return false;
    } else {
      return null;
    }
  };
  if (isStringBoolean(router.currentRoute.query.buildings)) {
    setBuildingsState(getStringAsBoolean(router.currentRoute.query.buildings));
  }
  if (isStringBoolean(router.currentRoute.query.plu)) {
    setPluState(getStringAsBoolean(router.currentRoute.query.plu));
  }
  if (isStringBoolean(router.currentRoute.query.satellite)) {
    setSatelliteState(getStringAsBoolean(router.currentRoute.query.satellite));
  }
  if (isStringBoolean(router.currentRoute.query.cadastre)) {
    setCadastreState(getStringAsBoolean(router.currentRoute.query.cadastre));
  }
};

router.onReady(async () => {
  // fillStateFromRoute();
});

router.afterEach(async (to, from) => {
  if (to.name !== from.name) {
    if (
      Object.keys(to.query).some((key) =>
        [
          "parcels",
          "search",
          "plu",
          "buildings",
          "satellite",
          "cadastre",
        ].includes(key)
      )
    ) {
      fillStateFromRoute();
    }
  }
});

router.beforeEach(async (to, from, next) => {
  const {
    params,
    query,
    name,
    path,
    meta,
    hash,
    matched,
    fullPath,
    redirectedFrom,
  } = to; // because circular reference in Route's matched property

  setRoute({
    params,
    query,
    name,
    path,
    meta,
    hash,
    fullPath,
    redirectedFrom,
  });

  console.log("route path:", path);
  console.log("route path type:", typeof path);

  if (to.path.includes("/login")) {
    console.log("you're on the login page");
    const regex = new RegExp(
      "[\\w! #$%&'*+-=? ^_`{|}~]+@[\\w! #$%&'*+-=? ^_`{|}~]+",
      "gm"
    );

    const isEmail = regex.test(params.email.toString());
    console.log("email params:", params.email);
    console.log("regex:", regex);
    console.log("regex test:", regex.test(params.email.toString()));

    if (isEmail) {
      console.log("login logic started");

      if (!isLoggedUser.value) {
        try {
          const { loginWithEmail } = useUser();

          await loginWithEmail(params.email.toString());

          if (isLoggedUser.value) {
            ("user logged in, push to focus");
            router.push("/focus");
          }
        } catch (err) {
          console.error(err);
          next({
            path: "/login",
            // query: { redirect: to.fullPath },
          });
        }
      }
    }
  }

  // if (to.matched.some(async (record) => record.meta.requiresAuth)) {
  if (to.meta.requiresAuth) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    console.log("protected path ok:", params);

    if (!isLoggedUser.value) {
      try {
        await fetchUserAuth();
        next();
      } catch (error) {
        next({
          path: "/login",
          query: { redirect: to.fullPath },
        });
      }
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }

  // next();
});

// router.beforeEach((to, from, next) => {
//   next(() => {});
// });

export const updateRouteLocation = ({
  params,
  query,
  replace = false,
  merge = false,
}: {
  params?: MapRouteParams;
  query?: RouteQuery;
  replace?: boolean;
  merge?: boolean;
}) => {
  const mergedParams = {
    ...router.currentRoute.params,
    ...params,
  };

  const mergedQuery = {
    ...router.currentRoute.query,
    ...query,
  };

  if (
    JSON.stringify(router.currentRoute.params) ===
      JSON.stringify(mergedParams) &&
    JSON.stringify(router.currentRoute.query) === JSON.stringify(mergedQuery)
  ) {
    if (params && params.step) {
      scrollToElement(`#${params.step}`);
    }
    return;
  }
  if (
    !mergedParams.lat &&
    !mergedParams.lng &&
    getMapCenter.value &&
    getMapCenter.value.lat &&
    getMapCenter.value.lng
  ) {
    mergedParams.lat = getMapCenter.value.lat.toString();
    mergedParams.lng = getMapCenter.value.lng.toString();
  }
  if (!mergedParams.zoom && getMapZoom.value) {
    mergedParams.zoom = getMapZoom.value.toString();
  }

  if (replace === true) {
    router.replace({
      params: merge ? mergedParams : params,
      query: merge ? mergedQuery : query,
    });
  } else {
    router.push({
      params: merge ? mergedParams : params,
      query: merge ? mergedQuery : query,
    });
  }
};

export default router;
