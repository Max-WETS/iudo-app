import { state } from "./state";
import { computed } from "@vue/composition-api";
import { formatAsPrice } from "@iudo/shared/utils";

export const getStripe = computed(() => state.stripe);
export const getElements = computed(() => state.elements);
export const getCard = computed(() => state.card);
export const getCardErrorMessage = computed(() => state.errorMessage);
export const isValidCardNumber = computed(() => state.isValidCardNumber);
export const getProduct = computed(() => state.product);
export const getProductPrice = computed(() =>
  getProduct.value
    ? isFreeProduct.value
      ? "Gratuit"
      : formatAsPrice(getProduct.value.price.unit_amount / 100)
    : null
);
export const isFreeProduct = computed(() =>
  getProduct.value !== null &&
  getProduct.value.price &&
  getProduct.value.price.unit_amount === 0
    ? true
    : false
);
