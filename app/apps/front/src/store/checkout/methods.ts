import { loadStripe, StripeElementStyle, StripeError } from "@stripe/stripe-js";
import { state, CheckoutState } from "./state";
import { getStripe, getElements, getCard, isFreeProduct } from "./getters";
import {
  getFormEmail,
  getFormName,
  FormState,
  getFormMessage,
  clearFormEmail,
  clearFormMessage,
  clearFormName,
  clearForm,
  getFormPhone,
} from "../form";
import router, { updateRouteLocation } from "@front/router";
import { fetchApi } from "@iudo/shared/utils";
import { CheckoutParams, Product } from "@iudo/data";
import { Stripe } from "stripe";
import {
  getUserParcelsIds,
  getUserAddressLabel,
  getUserName,
  getUserEmail,
  getUserAddressCity,
  getUserAddressPostCode,
} from "../user/getters";
import { clearUserParcels, setIsSaving } from "../user/methods";
import { resetState, showNotification } from "../global";
import { getParcelsIdsQueryFromArray } from "../parcels";
import { computed } from "@vue/composition-api";
import { update } from "lodash";
import { timeout } from "../map/utils";

export const initCheckout = async () => {
  state.stripe = await loadStripe(process.env.VUE_APP_STRIPE_PUBLISHABLE_KEY);
  state.elements = getStripe.value.elements(state.elementsOptions);

  state.card = getElements.value.create("card", {
    style: state.cardStyle,
    hidePostalCode: true,
  });

  getCard.value.mount("#card-element");

  bindCardEvents();
};

export const bindCardEvents = () => {
  getCard.value.on("change", function (event) {
    if (event.complete) {
      setIsValidCardNumber(true);
    } else {
      setIsValidCardNumber(false);
    }
    if (event.error) {
      setCheckoutErrorMessage(event.error.message);
    } else {
      setCheckoutErrorMessage(null);
    }
  });
};

export const setIsValidCardNumber = (value: boolean) => {
  state.isValidCardNumber = value;
};

export const setCheckoutErrorMessage = (errorMessage: string) => {
  state.errorMessage = errorMessage;
};

export const resetCheckout = () => {
  clearFormEmail();
  clearFormMessage();
  clearFormName();
  clearUserParcels();
};

const API_URL = process.env.VUE_APP_API_URL;
const APP_URL = process.env.VUE_APP_URL;

export namespace IudoApi {
  export interface ParcelParams {
    lon: string;
    lat: string;
  }
}

export const getCommonPostData = computed(() => {
  return {
    address: {
      line1: getUserAddressLabel.value,
      city: `${getUserAddressPostCode.value} ${getUserAddressCity.value}`.trim(),
    },
    parcels: getUserParcelsIds.value.map((id) => id.toString()),
    href: `${APP_URL}${
      router.resolve({
        params: { step: null },
        query: router.currentRoute.query,
      }).href
    }`,
  };
});

export const fetchCheckout = async (
  params: CheckoutParams
): Promise<Stripe.PaymentIntent> => {
  // const response = await fetch(`${API_URL}/stripe/checkout`, {
  //   method: "POST",
  // });

  return await fetchApi(
    `${API_URL}/stripe/checkout`,
    {},
    {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(params),
    }
  );
};

export const fetchProduct = async () => {
  return new Promise<void>(async (resolve, reject) => {
    const product: Product = await fetchApi(`${API_URL}/stripe/product`);

    setProduct(product);

    resolve();
  });
};

export const setProduct = (product: CheckoutState["product"]) => {
  state.product = product;
};

export const createPaymentIntent = async (
  params: Omit<CheckoutParams, "address" | "parcels">
) => {
  const paymentIntent = await fetchCheckout({
    ...params,
    ...getCommonPostData.value,
  });
  return paymentIntent;
};

export const createPaymentIntentFromForm = () => {
  return createPaymentIntent({
    email: getFormEmail.value,
    message: getFormMessage.value,
    name: getFormName.value,
    phone: getFormPhone.value,
  });
};

// export const createPaymentIntentFromSession = () => {
//   return createPaymentIntent({
//     email: getUserEmail.value,
//     name: getUserName.value,
//   });
// };

export const handleNoPayment = async () => {
  const paymentIntent = await createPaymentIntentFromForm();
  onCheckoutSuccess();
};

export const handleLoggedUserSave = async () => {
  setIsSaving(true);
  try {
    // await timeout(2000);
    const result = await postLoggedUserData();
    onLoggedUserSaveSuccess();
  } catch (e) {
    onLoggedUserSaveError();
  } finally {
    setIsSaving(false);
  }
};

export const onLoggedUserSaveError = () => {
  showNotification({
    text: "Il y'a un problème, l'adresse n'a pas pu être enregistrée.",
    type: "error",
  });
};

export const onLoggedUserSaveSuccess = () => {
  showNotification({
    text: "L'adresse a bien été enregistrée.",
    type: "success",
  });
};
export const postLoggedUserData = async () => {
  return await fetchApi(
    `${API_URL}/user/save`,
    {},
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        ...getCommonPostData.value,
      }),
    }
  );
};

export const onCheckoutSuccess = async () => {
  // router
  //   .push({
  //     path: "/succeeded",
  //   })
  //   .then(() => {
  //     resetState();
  //   });

  const parcelsQuery = getParcelsIdsQueryFromArray.value;
  const addressQuery = getUserAddressLabel.value;
  // const nameQuery = getFormName.value;
  // const emailQuery = getFormEmail.value;
  // const messageQuery = getFormMessage.value;

  const succesUrl = `https://www.iudo.co/map-contact?parcels=${parcelsQuery}&address=${addressQuery}`;
  resetState();
  window.location.href = succesUrl;
};

export const onCheckoutError = (error: StripeError) => {
  console.error(error);
};

export const handleCardPayment = async () => {
  const paymentIntent = await createPaymentIntentFromForm();

  await getStripe.value
    .confirmCardPayment(paymentIntent.client_secret, {
      payment_method: {
        card: getCard.value,
        billing_details: {
          email: getFormEmail.value,
          name: getFormName.value,
        },
      },
    })
    .then(function (result) {
      if (result.error) {
        // Show error to your customer (e.g., insufficient funds)

        onCheckoutError(result.error);
      } else {
        // The payment has been processed!
        if (result.paymentIntent.status === "succeeded") {
          onCheckoutSuccess();
          // Show a success message to your customer
          // There's a risk of the customer closing the window before callback
          // execution. Set up a webhook or plugin to listen for the
          // payment_intent.succeeded event that handles any business critical
          // post-payment actions.
        }
      }
    });
};
