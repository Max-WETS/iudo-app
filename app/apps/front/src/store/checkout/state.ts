import { reactive } from "@vue/composition-api";
import {
  Stripe,
  StripeElements,
  StripeCardElement,
  StripeElementStyle,
  StripeElementsOptions,
  PaymentIntent,
} from "@stripe/stripe-js";
import { Product } from "@iudo/data";

// import fontBold from "@front/assets/fonts/synthese/synthese-bold-webfont.woff2";
// import fontLight from "@front/assets/fonts/synthese/synthese-light-webfont.woff2";

export interface CheckoutState {
  stripe: Stripe;
  elements: StripeElements;
  card: StripeCardElement;
  cardStyle: StripeElementStyle;
  elementsOptions: StripeElementsOptions;
  product: Product;
  isFree: boolean;
  isValidCardNumber: boolean;
  errorMessage: string;
}

export const state = reactive<CheckoutState>({
  isValidCardNumber: false,
  errorMessage: null,
  stripe: null,
  product: null,
  elements: null,
  card: null,
  isFree: null,
  elementsOptions: {
    fonts: [
      {
        cssSrc:
          "https://fonts.googleapis.com/css2?family=Inter:wght@400;700&display=swap",
      },
      // {
      //   src: `url(/fonts/synthese/synthese-bold-webfont.woff2)`,
      //   // src: fontBold,
      //   family: "Synthese",
      //   weight: "bold",
      // },
      // {
      //   src: `url(/fonts/synthese/synthese-light-webfont.woff2)`,
      //   // src: fontLight,
      //   family: "Synthese",
      //   weight: "light",
      // },
    ],
  },

  cardStyle: {
    base: {
      color: "#1F297F",
      //fontFamily: "Synthese",
      fontFamily: "Inter",
      fontSmoothing: "antialiased",
      fontSize: "18px",
      fontWeight: "bold",
      "::placeholder": {
        color: "rgba(31, 41, 127, 0.6)",
        fontWeight: "normal",
      },
    },
    invalid: {
      color: "#fa755a",
      iconColor: "#fa755a",
    },
  },
});
