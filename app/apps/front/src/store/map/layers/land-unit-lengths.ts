// plu: {
//   fields: 'c_zonage c_zonage_b',

import { ui } from "@front/store/ui";
import { getUserLandUnit } from "@front/store/user/getters";
import mapboxgl from "mapbox-gl";
import { getMap } from "../getters";
import { defaultTransition, MAP_LAYER_NAMES } from "../layers";
import {
  IudoMapboxLayer,
  MAP_SOURCE_LAYERS,
  MAP_SOURCE_NAMES,
} from "../sources";
import { getOpacityExpression } from "../utils";
import {
  polygonToLine,
  midpoint,
  distance,
  Coord,
  Position,
  getCoord,
  nearestPointOnLine,
  dissolve,
  featureCollection,
  feature,
  convex,
  concave,
  combine,
  union,
  featureEach,
  Feature,
  MultiPolygon,
  getCoords,
  MultiLineString,
} from "@turf/turf";
import angle from "@turf/angle";
import { watch } from "@vue/composition-api";
import { getCadastreOpacityExpression } from "./cadastre";
import { Polygon } from "geojson";
import { flatten } from "lodash";
import { AreaGeometry } from "@iudo/data";

export const bindLandUnitLengthsWatcher = () => {
  watch(getUserLandUnit, (newValue, oldValue) => {
    // console.log(newValue, "newValue");

    (getMap.value.getSource(
      MAP_SOURCE_NAMES.LAND_UNIT_EDGES_LENGTH
    ) as mapboxgl.GeoJSONSource).setData(getLandUnitLengthsData());
  });
};

export const addLandUnitLenghtsSource = () => {
  getMap.value.addSource(MAP_SOURCE_NAMES.LAND_UNIT_EDGES_LENGTH, {
    type: "geojson",
    data: getLandUnitLengthsData(),
  });
};

export const addLandUnitLengthsLayer = () => {
  addLandUnitLenghtsSource();

  bindLandUnitLengthsWatcher();

  getMap.value.addLayer({
    source: MAP_SOURCE_NAMES.LAND_UNIT_EDGES_LENGTH,

    id: MAP_LAYER_NAMES.LAND_UNIT_EDGES_LENGTH,
    type: "symbol",
    paint: {
      // "text-color": ui.colors.hex.yellow,
      "text-halo-color": `rgba(255,255,255,1)`,
      "text-halo-width": 2,

      "text-color": [
        "interpolate",
        ["linear"],
        ["zoom"],
        14,
        `rgba(${ui.colors.rgb.blue}, 1)`,
        20,
        `rgba(${ui.colors.rgb.blue}, 1)`,
      ],
      "text-opacity": getOpacityExpression(
        { zoom: 16.5, opacity: 0 },
        { zoom: 17, opacity: 1 }
      ),
    },
    layout: {
      "text-size": 11,
      "text-field": ["get", "description"],
    },
  });
};

export const multiUnion = (
  thingsToUnion: Array<
    | GeoJSON.Feature<GeoJSON.Polygon | GeoJSON.MultiPolygon>
    | GeoJSON.Polygon
    | GeoJSON.MultiPolygon
  >
): GeoJSON.Feature<GeoJSON.Polygon | GeoJSON.MultiPolygon> => {
  const [firstThing, ...remainingThings] = thingsToUnion;
  if (!firstThing) {
    throw new Error("Expected at least one feature or geometry, got none");
  }

  let result = firstThing.type === "Feature" ? firstThing : feature(firstThing);

  for (const remainingThing of remainingThings) {
    try {
      result = union(result, remainingThing);
    } catch (e) {
      console.error(remainingThing, e);
    }
  }

  return result;
};

export const getLandUnitAsLines = (): GeoJSON.Feature<
  GeoJSON.LineString | GeoJSON.MultiLineString
> => {
  if (getUserLandUnit.value && getUserLandUnit.value.geometry) {
    const polygonGeometry = getUserLandUnit.value.geometry;

    //@ts-ignore
    const features = polygonGeometry.coordinates.map((coords, index) => {
      return feature<Polygon>({
        type: "Polygon",
        coordinates: [coords],
      });
    });

    const landUnitOuterRing = multiUnion(features);

    const multiLinesGeometry = polygonToLine(
      landUnitOuterRing
    ) as GeoJSON.Feature<GeoJSON.LineString | MultiLineString>;

    return multiLinesGeometry;
  } else {
    return null;
  }
};

export const getMetersAsKilometers = (meters: number) => meters / 1000;

export const getGroupedLinesByAngle = (
  linesCoordinates: Position[] | Position[][]
) => {
  let groupedLinesIndex = 0;
  let groupedLines: GeoJSON.LineString["coordinates"][] = [];

  linesCoordinates.forEach((coords, index) => {
    if (index === 0) {
      return;
    }
    const point1 = getCoords(linesCoordinates[index - 1]);
    const point2 = getCoords(linesCoordinates[index]);
    const point3 =
      index === linesCoordinates.length - 1
        ? getCoords(linesCoordinates[0])
        : getCoords(linesCoordinates[index + 1]);

    const pointsAngle = angle(point1, point2, point3);
    // console.log(pointsAngle, "ANGLE");

    // const angle1 = pointsAngle > 180 - 45 && pointsAngle < 180 + 45;
    const angle1 = pointsAngle < 180 - 45;
    const angle2 = pointsAngle > 180 + 45;
    const isCorner = angle1 || angle2;

    // const lineLength1 = distance(point1, point2, { units: "kilometers" });
    // const lineLength2 = distance(point2, point3, { units: "kilometers" });
    // const tolerance = getMetersAsKilometers(1);
    // const isIgnorableCorner =
    //   lineLength1 <= tolerance || lineLength2 <= tolerance;

    if (!groupedLines[groupedLinesIndex]) {
      groupedLines[groupedLinesIndex] = [];
    }
    groupedLines[groupedLinesIndex].push(point1, point2);

    if (isCorner) {
      // if (!isIgnorableCorner) {
      groupedLinesIndex++; // new line group!
      // }
    }
  });

  return groupedLines;
};

export const getLandUnitLengthsFeatures = () => {
  let landUnitLinesCoordinates = getLandUnitAsLines()
    ? getLandUnitAsLines().geometry.coordinates
    : [];

  const midpoints: { coordinates: Position; description: string }[] = [];

  const groupedLines = getGroupedLinesByAngle(landUnitLinesCoordinates);

  const minimumLength = getMetersAsKilometers(2);

  groupedLines.forEach((coordinates, index) => {
    const firstCoords = coordinates[0];
    const lastCoords = coordinates[coordinates.length - 1];
    // const lineMidPoint = midpoint(firstCoords, lastCoords);

    const pointsDistance = distance(firstCoords, lastCoords, {
      units: "kilometers",
    }); // in kilometers
    if (pointsDistance >= minimumLength) {
      const pointsMidpoint = getCoord(midpoint(firstCoords, lastCoords));

      const distanceInMeters = Math.round(pointsDistance * 1000);

      const nearestMidPoint = getCoord(
        nearestPointOnLine(
          {
            type: "LineString",
            coordinates,
          },
          pointsMidpoint
        )
      );
      midpoints.push({
        coordinates: nearestMidPoint,
        description: distanceInMeters + "m",
      });
    }
  });

  const landUnitLengthsFeatures = midpoints.map(
    ({ coordinates, description }) => {
      const feature = {
        type: "Feature",
        geometry: {
          type: "Point",
          coordinates,
        },
        properties: {
          description,
        },
      } as GeoJSON.Feature;
      return feature;
    }
  );

  return landUnitLengthsFeatures;
};

export const getLandUnitLengthsData = () => {
  const geojson: mapboxgl.GeoJSONSourceRaw["data"] = {
    type: "FeatureCollection",
    features: getLandUnitLengthsFeatures(),
  };

  return geojson;
};
