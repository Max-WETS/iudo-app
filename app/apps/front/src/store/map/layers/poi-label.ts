import { MAP_LAYER_NAMES } from "../layers";
import { getMap } from "..";

export const hidePoiLabelLayer = () => {
  getMap.value.setPaintProperty(MAP_LAYER_NAMES.POI_LABEL, "text-opacity", 0);
};

export const showPoiLabelLayer = () => {
  getMap.value.setPaintProperty(MAP_LAYER_NAMES.POI_LABEL, "text-opacity", 1);
};
