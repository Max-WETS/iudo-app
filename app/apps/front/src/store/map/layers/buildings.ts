import { ui } from "@front/store/ui";
import {
  getActiveBuildingFeatureId,
  getUserLandUnitBuildings,
} from "@front/store/user/getters";
import { Building } from "@iudo/data";
import { computed, nextTick } from "@vue/composition-api";
import { GeoJSONSourceRaw } from "mapbox-gl";
import { MapboxClickEvent } from "../events";
import { buildingsModel, getMap } from "../getters";
import { defaultTransition, MAP_LAYER_NAMES, layerStates } from "../layers";
import { IudoMapboxLayer, MAP_SOURCE_NAMES } from "../sources";
import { scrollToElement } from "@iudo/shared/utils";
import {
  setActiveBuildingFeatureId,
  setShowBuildingsDetails,
} from "@front/store/user/methods";
import { getOpacityExpression } from "../utils";

const getBuildingOpacityExpression = (opacity: number) => {
  return getOpacityExpression(
    {
      zoom: 14.5,
      opacity: 0,
    },
    {
      zoom: 15,
      opacity,
    }
  );
};

export const buildingDefaults = {
  "fill-color": [
    "interpolate",
    ["linear"],
    ["zoom"],
    0,
    "#e5e5e4",
    22,
    "#e5e5e4",
  ],
  "line-color": `hsl(60, 2%, 81%)`,
};

// export const getLayerStateNormalValue = (layerName: keyof typeof layerStates, prop) => {
//   return layerStates[layerName].
// }

export const getUserBuildingsAsFeatures = computed(() => {
  return getUserLandUnitBuildings.value
    ? getUserLandUnitBuildings.value.map((building, index) => {
        return {
          type: "Feature",
          geometry: building.geometry,
          properties: {
            index: building.index,
          },
          id: building.id,
        };
      })
    : [];
});

export const getUserBuildingsAsGeoJSON = computed(() => {
  return {
    type: "geojson",
    data: {
      type: "FeatureCollection",
      features: getUserBuildingsAsFeatures.value,
    },
  } as GeoJSONSourceRaw;
});

// export const moveBuildingLayers = (beforeLayerId: string) => {
//   getMap.value.moveLayer(MAP_LAYER_NAMES.BUILDING_FILLS, beforeLayerId);
//   getMap.value.moveLayer(MAP_LAYER_NAMES.BUILDING_LINES, beforeLayerId);
// };
// export const moveBuildingLayersToDefault = () => {
//   moveBuildingLayers("aeroway-line");
// };
// export const moveBuildingLayersToFront = () => {
//   moveBuildingLayers("admin-1-boundary-bg");
// };

export const addUserBuildingsSource = () => {
  getMap.value.addSource(
    MAP_SOURCE_NAMES.USER_BUILDINGS,
    getUserBuildingsAsGeoJSON.value
  );
};
export const updateUserBuildingsSource = () => {
  const data = getUserBuildingsAsGeoJSON.value.data;
  (getMap.value.getSource(
    MAP_SOURCE_NAMES.USER_BUILDINGS
  ) as mapboxgl.GeoJSONSource).setData(data);
};

export const addUserBuildingsLayers = () => {
  const userBuildingsLayerCommons: IudoMapboxLayer = {
    source: MAP_SOURCE_NAMES.USER_BUILDINGS,
    minzoom: 14,
  };

  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.USER_BUILDINGS_TEXT,
      type: "symbol",
      paint: {
        "text-color": ui.colors.hex.dark,
        "text-color-transition": defaultTransition,
        "text-opacity": getBuildingOpacityExpression(1),
      },
      layout: {
        "text-field": ["get", "index"],
        "text-size": 10,
        "text-allow-overlap": true,
      },
      ...userBuildingsLayerCommons,
    },
    "admin-1-boundary-bg"
  );

  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.USER_BUILDINGS_LINES,
      type: "line",
      //   layout: {
      //     'line-cap': 'round',
      //     'line-join': 'round'
      //   },
      paint: {
        ...layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_LINES].normal.paint,
        "line-opacity": getBuildingOpacityExpression(1),
      },
      ...userBuildingsLayerCommons,
    },
    MAP_LAYER_NAMES.USER_BUILDINGS_TEXT
  );

  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.USER_BUILDINGS_FILLS,
      type: "fill",
      //   layout: {
      //     'line-cap': 'round',
      //     'line-join': 'round'
      //   },
      paint: {
        ...layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_FILLS].normal.paint,
        "fill-opacity": getBuildingOpacityExpression(1),
      },
      ...userBuildingsLayerCommons,
    },
    MAP_LAYER_NAMES.USER_BUILDINGS_LINES
  );
};

export const showUserBuildings = () => {
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_FILLS,
    "visibility",
    "visible"
  );
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_LINES,
    "visibility",
    "visible"
  );
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_TEXT,
    "visibility",
    "visible"
  );
  // getMap.value.setPaintProperty(
  //   MAP_LAYER_NAMES.BUILDING_FILLS,
  //   "fill-color",
  //   `rgba(${ui.colors.rgb.blue},0.25)`
  // );
  // getMap.value.setPaintProperty(
  //   MAP_LAYER_NAMES.BUILDING_LINES,
  //   "line-color",
  //   `rgba(${ui.colors.rgb.blue},1)`
  // );
  // moveBuildingLayersToFront();
};

export const hideUserBuildings = () => {
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_FILLS,
    "visibility",
    "none"
  );
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_LINES,
    "visibility",
    "none"
  );
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_TEXT,
    "visibility",
    "none"
  );
  // getMap.value.setLayoutProperty("satellite-layer", "visibility", "none");
  // getMap.value.setPaintProperty(
  //   MAP_LAYER_NAMES.BUILDING_FILLS,
  //   "fill-color",
  //   buildingDefaults["fill-color"]
  // );
  // getMap.value.setPaintProperty(
  //   MAP_LAYER_NAMES.BUILDING_LINES,
  //   "line-color",
  //   buildingDefaults["line-color"]
  // );
  // moveBuildingLayersToDefault();
};

export const onUserBuildingsMouseMove = (feature: Building["geometry"]) => {
  // const parcelId = feature.properties.n_sq_pc;
  // onHoveredParcel(parcelId);

  hoverUserBuildingByFeatureId(feature.id);
};

export const onUserBuildingsMouseLeave = () => {
  // getMap.value.setFilter(MAP_LAYER_NAMES.CADASTRE_HIGHLIGHTED, false);
  // setMapLayerHoveredId("");

  applyUserBuildingState("normal");
  if (getActiveBuildingFeatureId.value === null) {
  }
};

export const applyUserBuildingState = (
  stateName: "normal" | "hover" | "active"
) => {
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_FILLS,
    "fill-color",
    layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_FILLS][stateName].paint[
      "fill-color"
    ]
  );
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_LINES,
    "line-color",
    layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_LINES][stateName].paint[
      "line-color"
    ]
  );
};

export const hoverUserBuildingByFeatureId = (id) => {
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_FILLS,
    "fill-color",
    [
      "match",
      ["id"],
      id,
      layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_FILLS].hover.paint[
        "fill-color"
      ],
      layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_FILLS].normal.paint[
        "fill-color"
      ],
    ]
  );
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_LINES,
    "line-color",
    [
      "match",
      ["id"],
      id,
      layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_LINES].hover.paint[
        "line-color"
      ],
      layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_LINES].normal.paint[
        "line-color"
      ],
    ]
  );
};

export const activeUserBuildingByFeatureId = (id) => {
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_FILLS,
    "fill-color",
    [
      "match",
      ["id"],
      id,
      layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_FILLS].active.paint[
        "fill-color"
      ],
      layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_FILLS].normal.paint[
        "fill-color"
      ],
    ]
  );
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.USER_BUILDINGS_LINES,
    "line-color",
    [
      "match",
      ["id"],
      id,
      layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_LINES].active.paint[
        "line-color"
      ],
      layerStates[MAP_LAYER_NAMES.USER_BUILDINGS_LINES].normal.paint[
        "line-color"
      ],
    ]
  );
};

export const onUserBuildingsClick = (e: MapboxClickEvent) => {
  const features = (getMap.value.queryRenderedFeatures(e.point, {
    layers: [MAP_LAYER_NAMES.USER_BUILDINGS_FILLS],
  }) as unknown) as GeoJSON.Feature<Building["geometry"]>[];
  const feature = features[0];

  setShowBuildingsDetails(true);

  nextTick(() => {
    temporaryHighlightUserBuilding(feature.id);
    scrollToElement(`#building-${feature.id}`, {
      block: "start",
    });
  });
};

let buidlingsHighlightTimeout;

export const temporaryHighlightUserBuilding = (id) => {
  setActiveBuildingFeatureId(id);
  clearTimeout(buidlingsHighlightTimeout);
  buidlingsHighlightTimeout = setTimeout(function () {
    setActiveBuildingFeatureId(null);
  }, 3000);
};
