import {
  getMap,
  getMapLayerHoveredId,
  getMapPopup,
  isMapPopup,
} from "../getters";
import { MAP_LAYER_NAMES, defaultTransition } from "../layers";
import { ui } from "@front/store/ui";
import {
  MAP_SOURCE_NAMES,
  MAP_SOURCE_LAYERS,
  IudoMapboxLayer,
} from "../sources";
import {
  onSelectedParcel,
  onUnselectedParcel,
  onHoveredParcel,
} from "@front/store/parcels";
import { searchParcel } from "@iudo/shared/lib/api-parcels";
import { MapPcaFeature, setMapLayerHoveredId, MapPcaProperties } from "..";
import { MapboxClickEvent } from "../events";
import { getUserParcelsIds, getUserParcels } from "@front/store/user/getters";
import { isUserParcel } from "@front/store/user/methods";
import { removeMapPopup } from "../methods";
import { showNotification } from "@front/store/global";
import { getOpacityExpression } from "../utils";

// const getOpacityWithExpression = (opacity: number): mapboxgl.Expression => [
//   "interpolate",
//   ["linear"],
//   ["zoom"],
//   14.5,
//   0,
//   15,
//   opacity,
// ];

export const getCadastreOpacityExpression = (opacity: number) => {
  return getOpacityExpression(
    {
      zoom: 14.5,
      opacity: 0,
    },
    {
      zoom: 15,
      opacity,
    }
  );
};

const cadastreOpacityInterpolation = getCadastreOpacityExpression(1);

export const addCadastreLayers = () => {
  const cadastreLayerCommons: IudoMapboxLayer = {
    source: MAP_SOURCE_NAMES.CADASTRE,
    "source-layer": MAP_SOURCE_LAYERS.CADASTRE,
  };

  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.CADASTRE_FILLS,
      type: "fill",
      paint: {
        "fill-color": `rgba(${ui.colors.rgb.yellow}, 0)`,
        "fill-opacity": cadastreOpacityInterpolation,
        "fill-opacity-transition": defaultTransition,
        // "fill-opacity": [
        //   "case",
        //   ["boolean", ["feature-state", "hover"], false],
        //   0.25,
        //   0.1,
        // ],
      },
      ...cadastreLayerCommons,
    },
    "admin-1-boundary-bg"
  );

  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.CADASTRE_LINES,
      type: "line",
      paint: {
        "line-color": `rgba(${ui.colors.rgb.dark}, 0.5)`,
        "line-opacity": cadastreOpacityInterpolation,
        "line-opacity-transition": defaultTransition,
        // "line-dasharray": [2, 1],
      },
      ...cadastreLayerCommons,
    },
    "admin-1-boundary-bg"
  );

  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.CADASTRE_HIGHLIGHTED,
      type: "line",
      //   layout: {
      //     'line-cap': 'round',
      //     'line-join': 'round'
      //   },
      paint: {
        "line-color": `rgba(${ui.colors.rgb.yellow}, 1)`,
        "line-opacity": cadastreOpacityInterpolation,
        "line-opacity-transition": defaultTransition,
        "line-width": 2,
        "line-dasharray": [1, 1],
      },
      filter: ["==", "n_sq_pc", ""],
      ...cadastreLayerCommons,
    },
    "admin-1-boundary-bg"
  );

  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.CADASTRE_SELECTED_FILLS,
      type: "fill",
      //   layout: {
      //     'line-cap': 'round',
      //     'line-join': 'round'
      //   },
      paint: {
        "fill-color": `rgba(${ui.colors.rgb.yellow}, 0.1)`,
        "fill-opacity": cadastreOpacityInterpolation,
        "fill-opacity-transition": defaultTransition,
      },
      filter: ["==", "n_sq_pc", ""],
      ...cadastreLayerCommons,
    },
    "admin-1-boundary-bg"
  );

  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.CADASTRE_SELECTED_LINES,
      type: "line",
      //   layout: {
      //     'line-cap': 'round',
      //     'line-join': 'round'
      //   },
      paint: {
        "line-color": `rgba(${ui.colors.rgb.yellow}, 1)`,
        "line-opacity": cadastreOpacityInterpolation,
        "line-width": 2,
        "line-opacity-transition": defaultTransition,
      },
      filter: ["==", "n_sq_pc", ""],
      ...cadastreLayerCommons,
    },
    "admin-1-boundary-bg"
  );
};

export const hideCadastreLayer = () => {
  // getMap.value.setLayoutProperty("satellite-layer", "visibility", "none");
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.CADASTRE_FILLS,
    "fill-opacity",
    0
  );
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.CADASTRE_LINES,
    "line-opacity",
    0
  );
};
export const showCadastreLayerWithSomeOpacity = () => {
  // getMap.value.setLayoutProperty("satellite-layer", "visibility", "none");
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.CADASTRE_FILLS,
    "fill-opacity",
    getCadastreOpacityExpression(0.25)
  );
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.CADASTRE_LINES,
    "line-opacity",
    getCadastreOpacityExpression(0.5)
  );
};
export const showCadastreLayer = () => {
  // getMap.value.setLayoutProperty("satellite-layer", "visibility", "none");
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.CADASTRE_FILLS,
    "fill-opacity",
    cadastreOpacityInterpolation
  );
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.CADASTRE_LINES,
    "line-opacity",
    cadastreOpacityInterpolation
  );
};

export const onCadastreClick = async (e: MapboxClickEvent) => {
  const feature = (e.features[0] as unknown) as MapPcaFeature;

  if (isMapPopup.value && isUserParcel(feature.properties.n_sq_pc)) {
    removeMapPopup();
  }

  var { lat, lng } = e.lngLat;

  const parcel = await searchParcel({
    lat: lat.toString(),
    lon: lng.toString(),
  });

  const isParcelAlreadySelected = getUserParcelsIds.value.includes(parcel.id);

  if (parcel && !isParcelAlreadySelected) {
    onSelectedParcel(parcel);
  } else {
    const isProtectedParcel = getUserParcels.value.find(
      (userParcel) => userParcel.id === parcel.id && userParcel.protected
    );

    if (isProtectedParcel) {
      showNotification({
        text: "Vous ne pouvez pas déselectionner cette parcelle",
        type: "error",
      });
    } else {
      onUnselectedParcel(parcel.id);
    }
  }
};

// export const onCadastrefeatureStateHover = (feature: MapPcaFeature) => {
//   if (getMapLayerHoveredId.value) {
//     getMap.value.setFeatureState(
//       {
//         source: MAP_SOURCES.CADASTRE,
//         sourceLayer: MAP_SOURCE_LAYERS.CADASTRE,
//         id: getMapLayerHoveredId.value,
//       },
//       { hover: false }
//     );
//   }
//   setMapLayerHoveredId(feature.id);
//   getMap.value.setFeatureState(
//     {
//       source: MAP_SOURCES.CADASTRE,
//       sourceLayer: MAP_SOURCE_LAYERS.CADASTRE,
//       id: getMapLayerHoveredId.value,
//     },
//     { hover: true }
//   );
// };

// export const onCadastreMouseEnter = (feature: MapPcaFeature) => {
//   const parcelId = feature.properties.n_sq_pc;
// };

export const onCadastreMouseMove = (feature: MapPcaFeature) => {
  const parcelId = feature.properties.n_sq_pc;
  onHoveredParcel(parcelId);
};

export const onCadastreMouseLeave = () => {
  // if (getMapLayerHoveredId.value) {
  //   getMap.value.setFeatureState(
  //     {
  //       source: MAP_SOURCES.CADASTRE,
  //       sourceLayer: MAP_SOURCE_LAYERS.CADASTRE,
  //       id: getMapLayerHoveredId.value,
  //     },
  //     { hover: false }
  //   );
  // }
  getMap.value.setFilter(MAP_LAYER_NAMES.CADASTRE_HIGHLIGHTED, false);
  setMapLayerHoveredId("");
};

export const filterCadastreHighlightedLayerByPropValue = <
  T extends MapPcaProperties,
  P extends keyof T,
  V extends T[P][]
>(
  prop: P,
  value: V
) => {
  let filter = ["match", ["get", prop], value, true, false];
  getMap.value.setFilter(MAP_LAYER_NAMES.CADASTRE_HIGHLIGHTED, filter);
};

export const filterCadastreSelectedLayersByPropValue = <
  T extends MapPcaProperties,
  P extends keyof T,
  V extends T[P][]
>(
  prop: P,
  value: V
) => {
  let filter = ["match", ["get", prop], value, true, false];
  getMap.value.setFilter(MAP_LAYER_NAMES.CADASTRE_SELECTED_LINES, filter);
  getMap.value.setFilter(MAP_LAYER_NAMES.CADASTRE_SELECTED_FILLS, filter);
};

export const resetCadastreSelectedLayers = () => {
  getMap.value.setFilter(MAP_LAYER_NAMES.CADASTRE_SELECTED_LINES, false);
  getMap.value.setFilter(MAP_LAYER_NAMES.CADASTRE_SELECTED_FILLS, false);
};
