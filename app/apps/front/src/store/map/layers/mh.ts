import { getMap, getMapPopup } from "../getters";
import { MAP_LAYER_NAMES, defaultTransition } from "../layers";
import { MAP_SOURCE_NAMES, MAP_SOURCE_LAYERS } from "../sources";
import { state } from "../state";
import { MapboxClickEvent } from "../events";
import {
  MapMhFeature,
  setLastClickedAreas,
  setMapPopup,
  addMapPopup,
} from "..";
import { LngLat, Popup } from "mapbox-gl";
import { toggleArea } from "./areas";
import { showMapDefaultCursor, showMapPointerCursor } from "../methods";

export const addMhLayer = () => {
  getMap.value.addLayer({
    id: MAP_LAYER_NAMES.MONUMENTS_HISTORIQUES,
    source: MAP_SOURCE_NAMES.MONUMENTS_HISTORIQUES,
    "source-layer": MAP_SOURCE_LAYERS.MONUMENTS_HISTORIQUES,
    type: "symbol",
    layout: {
      "icon-image": "monument",
      // "text-field": ["get", "l_station"],
      "icon-size": 0.5,
      "text-offset": [0, 2],
    },
  });
  // getMap.value.addLayer({
  //   source: MAP_SOURCE_NAMES.MONUMENTS_HISTORIQUES,
  //   "source-layer": MAP_SOURCE_LAYERS.MONUMENTS_HISTORIQUES,
  //   id: MAP_LAYER_NAMES.MONUMENTS_HISTORIQUES,
  //   type: "circle",
  //   paint: {
  //     "circle-radius": 5,
  //     "circle-color": "rgba(55,148,179,1)",
  //   },
  // });
};

export const toggleMhArea = (feature: MapMhFeature) => {
  toggleArea(feature);

  setLastClickedAreas(feature.properties.l_imp);
};

export const onMhClick = (e: MapboxClickEvent) => {
  const features = (getMap.value.queryRenderedFeatures(e.point, {
    layers: [MAP_LAYER_NAMES.MONUMENTS_HISTORIQUES],
  }) as unknown) as MapMhFeature[];
  const feature = features[0];

  const lngLat = new LngLat(
    feature.geometry.coordinates[0],
    feature.geometry.coordinates[1]
  );

  addMapPopup(
    new Popup({ closeButton: false, offset: [0, -10] })
      .setLngLat(lngLat)
      .setHTML(feature.properties.l_imp)
      .addTo(getMap.value)
  );

  toggleMhArea(feature);
};

export const onHistoricMonumentsMouseMove = (feature: MapMhFeature) => {
  showMapPointerCursor();
};
export const onHistoricMonumentsMouseLeave = () => {
  showMapDefaultCursor();
};
