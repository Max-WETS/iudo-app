import { AreaGeometry } from "@iudo/data";
import { LngLat } from "mapbox-gl";
import { getMap, getLastClickedAreas, getMapState } from "../getters";
import { ui } from "@front/store/ui";
import { removeMapPopup } from "../methods";

export enum Area {
  source = "area-source",
  fillLayer = "area-fill-layer",
  lineLayer = "area-line-layer",
}

export const toggleArea = <T extends GeoJSON.Feature<GeoJSON.Point>>(
  feature: T
) => {
  const lngLat = new LngLat(
    feature.geometry.coordinates[0],
    feature.geometry.coordinates[1]
  );
  const source = getMap.value.getSource(Area.source);

  const handle = createGeoJSONCircle(lngLat, 0.5);

  const stationName = feature.properties.l_station || feature.properties.l_imp;
  if (!source) {
    createAreaSource(Area.source, handle);
  } else {
    updateAreaSource(Area.source, handle);
    if (
      getLastClickedAreas.value === stationName &&
      getAreaLayerVisibility() === "visible"
    ) {
      hideArea();
      removeMapPopup();
    } else {
      showArea();
    }
  }
};

export const getAreaLayerVisibility = () => {
  return (
    getMap.value.getLayoutProperty(Area.fillLayer, "visibility") &&
    getMap.value.getLayoutProperty(Area.lineLayer, "visibility")
  );
};

export const isAreaLayerExist = () => {
  return (
    getMap.value.getLayer(Area.fillLayer) &&
    getMap.value.getLayer(Area.lineLayer)
  );
};

export const hideArea = () => {
  if (isAreaLayerExist() && getAreaLayerVisibility() === "visible") {
    getMap.value.setLayoutProperty(Area.fillLayer, "visibility", "none");
    getMap.value.setLayoutProperty(Area.lineLayer, "visibility", "none");
  }
};
export const showArea = () => {
  if (isAreaLayerExist() && getAreaLayerVisibility() === "none") {
    getMap.value.setLayoutProperty(Area.fillLayer, "visibility", "visible");
    getMap.value.setLayoutProperty(Area.lineLayer, "visibility", "visible");
  }
};

export const updateAreaSource = (sourceName, handle) => {
  let data;

  data = handle.data;
  (getMap.value.getSource(sourceName) as mapboxgl.GeoJSONSource).setData(data);
};

export const createAreaSource = (sourceName, handle) => {
  getMap.value.addSource(sourceName, handle);

  getMap.value.addLayer({
    id: Area.fillLayer,
    type: "fill",
    source: sourceName,
    minzoom: getMapState.value.layers.areas.minzoom,
    layout: {
      visibility: "visible",
    },
    paint: {
      "fill-color": `rgba(${ui.colors.rgb["area-fill"]}, 0.3)`,
      // "fill-outline-color": ui.colors.hex.blue,
    },
  });
  getMap.value.addLayer({
    id: Area.lineLayer,
    type: "line",
    source: sourceName,
    minzoom: getMapState.value.layers.areas.minzoom,
    layout: {
      visibility: "visible",
    },
    paint: {
      "line-width": 1,
      "line-color": ui.colors.hex.blue,
    },
  });
};

export const createGeoJSONCircle = function (
  lngLat,
  radiusInKm,
  points = 64
): mapboxgl.GeoJSONSourceRaw {
  var km = radiusInKm;

  var ret = [];
  var distanceX = km / (111.32 * Math.cos((lngLat.lat * Math.PI) / 180));
  var distanceY = km / 110.574;

  var theta, x, y;
  for (var i = 0; i < points; i++) {
    theta = (i / points) * (2 * Math.PI);
    x = distanceX * Math.cos(theta);
    y = distanceY * Math.sin(theta);

    ret.push([lngLat.lng + x, lngLat.lat + y]);
  }
  ret.push(ret[0]);
  return {
    type: "geojson",
    data: {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: {
            type: "Polygon",
            coordinates: [ret],
          },
          properties: {},
        },
      ],
    },
  };
};
