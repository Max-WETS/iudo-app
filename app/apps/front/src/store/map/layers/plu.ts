// plu: {
//   fields: 'c_zonage c_zonage_b',

import { ui } from "@front/store/ui";
import { getMap } from "../getters";
import { defaultTransition, MAP_LAYER_NAMES } from "../layers";
import {
  IudoMapboxLayer,
  MAP_SOURCE_LAYERS,
  MAP_SOURCE_NAMES,
} from "../sources";
import { getOpacityExpression } from "../utils";

const getPluOpacityExpression = (from: number, to: number) => {
  return getOpacityExpression(
    {
      zoom: 11.5,
      opacity: from,
    },
    {
      zoom: 12,
      opacity: to,
    }
  );
};

export const addPluLayers = () => {
  const pluLayerCommons: IudoMapboxLayer = {
    source: MAP_SOURCE_NAMES.PLU,
    "source-layer": MAP_SOURCE_LAYERS.PLU,
  };

  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.PLU_TEXT,
      type: "symbol",
      paint: {
        // "text-color": ui.colors.hex.yellow,
        "text-color": [
          "interpolate",
          ["linear"],
          ["zoom"],
          14,
          `rgba(${ui.colors.rgb.yellow}, 1)`,
          20,
          `rgba(${ui.colors.rgb.yellow}, 1)`,
        ],
        "text-opacity": getPluOpacityExpression(0, 1),
      },
      layout: {
        "text-field": ["get", "c_zonage_b"],
      },
      ...pluLayerCommons,
    },
    MAP_LAYER_NAMES.CADASTRE_HIGHLIGHTED
  );

  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.PLU_LINES,
      type: "line",
      paint: {
        // "line-color": `rgba(${ui.colors.rgb.blue}, 1)`,
        "line-color": [
          "interpolate",
          ["linear"],
          ["zoom"],
          14,
          `rgba(${ui.colors.rgb.blue}, 1)`,
          20,
          `rgba(${ui.colors.rgb.blue}, .5)`,
        ],
        "line-opacity": getPluOpacityExpression(0, 0.5),
        "line-opacity-transition": defaultTransition,
        "line-width": ["interpolate", ["linear"], ["zoom"], 11.5, 1, 20, 3],
      },

      ...pluLayerCommons,
    },
    MAP_LAYER_NAMES.PLU_TEXT
  );

  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.PLU_FILLS,
      type: "fill",
      paint: {
        // "fill-color": `rgba(${ui.colors.rgb.blue}, 1)`,
        "fill-color": [
          "interpolate",
          ["linear"],
          ["zoom"],
          14,
          `rgba(${ui.colors.rgb.blue}, 1)`,
          20,
          `rgba(${ui.colors.rgb.blue}, .1)`,
        ],
        "fill-opacity": getPluOpacityExpression(0, 0.5),
        "fill-opacity-transition": defaultTransition,
      },

      ...pluLayerCommons,
    },
    MAP_LAYER_NAMES.PLU_LINES
  );
};

export const showPluLayers = () => {
  // getMap.value.setLayoutProperty("satellite-layer", "visibility", "none");
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.PLU_FILLS,
    "visibility",
    "visible"
  );
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.PLU_LINES,
    "visibility",
    "visible"
  );
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.PLU_TEXT,
    "visibility",
    "visible"
  );
};

export const hidePluLayers = () => {
  // getMap.value.setLayoutProperty("satellite-layer", "visibility", "none");
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.PLU_FILLS,
    "visibility",
    "none"
  );
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.PLU_LINES,
    "visibility",
    "none"
  );
  getMap.value.setLayoutProperty(
    MAP_LAYER_NAMES.PLU_TEXT,
    "visibility",
    "none"
  );
};
