import { getMap, getSatelliteState } from "../getters";
import { MAP_LAYER_NAMES, defaultTransition } from "../layers";
import { setSatelliteState } from "../methods";
import { MAP_SOURCE_NAMES } from "../sources";
import { state } from "../state";

export const addSatelliteLayer = () => {
  getMap.value.addLayer(
    {
      id: MAP_LAYER_NAMES.SATELLITE,
      type: "raster",
      source: MAP_SOURCE_NAMES.SATELLITE,
      // layout: {
      //   visibility: "none",
      // },
      paint: {
        "raster-opacity": 0,
        "raster-opacity-transition": defaultTransition,
      },
    },
    MAP_LAYER_NAMES.CADASTRE_HIGHLIGHTED
  );
};

export const hideSatelliteLayer = () => {
  // getMap.value.setLayoutProperty("satellite-layer", "visibility", "none");
  getMap.value.setPaintProperty(MAP_LAYER_NAMES.SATELLITE, "raster-opacity", 0);
};

export const showSatelliteLayerWithSomeOpacity = () => {
  getMap.value.setPaintProperty(
    MAP_LAYER_NAMES.SATELLITE,
    "raster-opacity",
    0.1
  );
};

export const showSatelliteLayer = () => {
  getMap.value.setPaintProperty(MAP_LAYER_NAMES.SATELLITE, "raster-opacity", 1);
  // getMap.value.setLayoutProperty("satellite-layer", "visibility", "visible");
  // let rasterOpacity;
  // if (getMapState.value.cadastre === true) {
  //   rasterOpacity = 0.05;
  // } else {
  //   rasterOpacity = 1;
  // }
  // getMap.value.setPaintProperty(
  //   MAP_LAYERS.SATELLITE,
  //   "raster-opacity",
  //   rasterOpacity
  // );
};

export const toggleSatelliteLayer = () => {
  setSatelliteState(!getSatelliteState());
};
