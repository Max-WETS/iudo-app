import {
  setLastClickedAreas,
  setMapPopup,
  removeMapPopup,
  addMapPopup,
  showMapPointerCursor,
  showMapDefaultCursor,
} from "../methods";
import { getLastClickedAreas, getMap, getMapState } from "../getters";
import { MapStcFeature } from "..";
import { LngLat, Popup } from "mapbox-gl";
import { ui } from "@front/store/ui";
import { MAP_LAYER_NAMES } from "../layers";
import {
  IudoMapboxLayer,
  MAP_SOURCE_LAYERS,
  MAP_SOURCE_NAMES,
} from "../sources";
import { MapboxClickEvent } from "../events";
import { toggleArea } from "./areas";

export const addStcLayers = () => {
  const stcLayerCommons: IudoMapboxLayer = {
    source: MAP_SOURCE_NAMES.STC,
    "source-layer": MAP_SOURCE_LAYERS.STC,
  };

  // const radiusInMeters = 800;

  // getMap.value.addLayer({
  //   id: MAP_LAYERS.STC_SYMBOLS,
  //   type: "circle",
  //   paint: {
  //     "circle-radius": 5,
  //     "circle-color": "rgba(55,148,179,1)",
  //   },
  //   ...stcLayerCommons,
  // });

  // let image = new Image(64, 64);
  // image.src = TramwaySvg;

  // getMap.value.loadImage(Tramway, function (error, image) {
  // getMap.value.addImage("TRM", image);
  // });

  getMap.value.addLayer({
    id: MAP_LAYER_NAMES.STC_SYMBOLS,
    type: "symbol",
    layout: {
      "icon-image": ["get", "c_nature"],
      // "text-field": ["get", "l_station"],
      "icon-size": 0.5,
      "text-offset": [0, 2],
    },
    ...stcLayerCommons,
  });
};

export const toggleStcArea = (feature: MapStcFeature) => {
  toggleArea(feature);

  setLastClickedAreas(feature.properties.l_station);
};

export const onStcClick = (e: MapboxClickEvent) => {
  const features = (getMap.value.queryRenderedFeatures(e.point, {
    layers: [MAP_LAYER_NAMES.STC_SYMBOLS],
  }) as unknown) as MapStcFeature[];
  const feature = features[0];

  const lngLat = new LngLat(
    feature.geometry.coordinates[0],
    feature.geometry.coordinates[1]
  );

  addMapPopup(
    new Popup({ closeButton: false, offset: [0, -10] })
      .setLngLat(lngLat)
      .setHTML(feature.properties.l_station)
      .addTo(getMap.value)
  );

  toggleStcArea(feature);
};

export const onStcMouseEnter = (feature: MapStcFeature) => {
  showMapPointerCursor();
};
export const onStcMouseLeave = () => {
  showMapDefaultCursor();
};
