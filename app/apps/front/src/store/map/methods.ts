import { state, MapState } from "./state";
import { getMap, getMapPopup, isMapReady } from "./getters";
import { Map, LngLatLike, LngLatBoundsLike } from "mapbox-gl";
import router from "@front/router";
import { initMapWatchers } from "./watchers";
import { initMapLayers } from "./layers";
import { initMapSources } from "./sources";
import { initMapEvents } from "./events";
import { Parcel, AreaGeometry } from "@iudo/data";
import { bbox } from "@turf/turf";
import { bindSearchEvents, updateCadastreSelectedLayers } from "../parcels";
import {
  getUserAddressLabel,
  getUserLandUnit,
  hasUserParcels,
} from "../user/getters";
import { watch } from "@vue/composition-api";
import { updateUserBuildingsSource } from "./layers/buildings";

export const initMap = (event) => {
  setMapInstance(event.map);
  handleMapResize();
  initMapSources();
  initMapLayers();
  initMapEvents();
  initMapDefaults();
  initMapWatchers();
  bindSearchEvents();
  setMapReady(true);

  updateCadastreSelectedLayers();
  updateUserBuildingsSource();
};

export const handleMapResize = () => {
  const element = document.getElementById("the-map");
  element.classList.add("is-resized");
  // const elementRect = element.getBoundingClientRect();

  const windowHeight = window.innerHeight;

  const elementStyle = getComputedStyle(element);
  const elementPositionTop =
    element.offsetTop || element.getBoundingClientRect().top;
  const elementMarginBottom = parseInt(elementStyle.marginBottom);

  const mapComputedHeight =
    windowHeight - elementPositionTop - elementMarginBottom;

  element.style.height = mapComputedHeight + "px";
  getMap.value.resize();
};

export const roundZoom = (zoom) => {
  return Math.round(zoom * 2) / 2;
};

export const initMapPosition = () => {};

export const initMapDefaults = () => {
  const { lat, lng, zoom } = router.currentRoute.params;

  if (!lat && !lng) {
    getMap.value.fitBounds(state.maxBounds, { duration: 0 });
  }

  if (lat && lng) {
    setMapCenter({
      lat: Number(lat),
      lng: Number(lng),
    });
    getMap.value.setCenter(state.center);
  }
  if (zoom) {
    setMapZoom(Number(zoom));
    getMap.value.setZoom(state.zoom);
  }
};

export const setMapInstance = (mapInstance: null | Map) => {
  state.instance = Object.freeze({ wrapper: mapInstance });
};

export const onMapUnmounted = () => {
  // for future...
  setMapReady(false);
};

export const zoomIn = () => {
  getMap.value.zoomIn();
};
export const zoomOut = () => {
  getMap.value.zoomOut();
};

export const setCadastreState = (value: boolean) => {
  state.layers.cadastre = value;
};

export const setSatelliteState = (value: boolean) => {
  state.layers.satellite = value;
};

export const setPluState = (value: boolean) => {
  state.layers.plu = value;
};

export const setBuildingsState = (value: boolean) => {
  state.layers.buildings = value;
};

export const setMapCenter = (center: MapState["center"]) => {
  state.center = center;
};
export const resetMapState = () => {
  setMapCenter(null);
  setMapZoom(null);
};

export const setMapZoom = (zoom: number) => {
  state.zoom = roundZoom(zoom);
};

export const setMapReady = (isReady: boolean) => {
  state.isReady = isReady;
};

export const mapFlyToPoint = (center: LngLatLike) => {
  getMap.value.flyTo({
    center,
    zoom: 16,
  });
};

export const mapFitToGeometry = (geometry: AreaGeometry, duration?) => {
  const boundingBox = bbox(geometry);
  const padding = 60;

  const fitBoundsOptions: mapboxgl.FitBoundsOptions = {
    padding: { top: padding, bottom: padding, left: padding, right: padding },
  };

  if (duration !== null && duration !== undefined) {
    fitBoundsOptions.duration = duration;
  }

  getMap.value.fitBounds(boundingBox as LngLatBoundsLike, {
    ...fitBoundsOptions,
    maxZoom: 18.5,
  });
};

export const mapFitToParcel = (parcel: Parcel) => {
  mapFitToGeometry(parcel.geometry);
};

export const mapFitToUserLandUnit = () => {
  if (getUserLandUnit.value) {
    mapFitToGeometry(getUserLandUnit.value.geometry);
  }
};

export const setMapLayerHoveredId = (id) => {
  state.layerHoveredId = id;
};

export const setLastClickedAreas = (id: string) => {
  state.layers.areas.lastClicked = id;
};

export const addMapPopup = (popup: mapboxgl.Popup) => {
  removeMapPopup();
  setMapPopup(popup);
};
export const setMapPopup = (popup: mapboxgl.Popup) => {
  state.popup = Object.freeze({ wrapper: popup });
};
export const removeMapPopup = () => {
  if (getMapPopup.value) {
    getMapPopup.value.remove();
    state.popup = null;
  }
};

export const showMapPointerCursor = () => {
  getMap.value.getCanvas().style.cursor = "pointer";
};
export const showMapDefaultCursor = () => {
  getMap.value.getCanvas().style.cursor = "";
};

export const waitForMap = async () => {
  return new Promise<void>((resolve, reject) => {
    if (isMapReady.value === true) {
      resolve();
    } else {
      const stopMapReadyWatcher = watch(isMapReady, (value) => {
        if (value === true) {
          resolve();
          stopMapReadyWatcher();
        }
      });
    }
  });
};

export const getMapAsDataUrl = function () {
  // var img = new Image();
  // var mapCanvas = getMap.value.getCanvas();
  // img.src = mapCanvas.toDataURL("image/png");
  // // .replace("image/png", "image/octet-stream");
  // return img;

  return new Promise(function (resolve, reject) {
    getMap.value.once("render", function () {
      resolve(getMap.value.getCanvas().toDataURL("image/png"));
    });
    /* trigger render */
    getMap.value.setBearing(getMap.value.getBearing());
  });
};

export const saveMapAsImage = async () => {
  // INFO: you need to set mapbox preserveDrawingBuffer variable as true before!
  // const img = getMapAsImage();
  const dataUrl = (await getMapAsDataUrl()) as string;
  const anchor = document.createElement("a");
  const imageName = hasUserParcels.value ? getUserAddressLabel.value : "iudo";
  anchor.setAttribute("download", imageName + ".png");
  anchor.setAttribute("href", dataUrl);
  anchor.click();
};
