import { getMap, getMapState } from "./getters";
import { addUserBuildingsSource } from "./layers/buildings";

export enum MAP_SOURCE_NAMES {
  SATELLITE = "satellite-source",
  CADASTRE = "cadastre-source",
  STC = "stc-source",
  MONUMENTS_HISTORIQUES = "mh-source",
  PLU = "plu-source",
  USER_BUILDINGS = "user-buildings-source",
  LAND_UNIT_EDGES_LENGTH = "land-unit-edges-length-source",
}

export enum MAP_SOURCE_LAYERS {
  CADASTRE = "pca",
  EMPRISE_BATIE_METROPOLE_GRAND_PARIS = "ebm",
  PATRIMOINE_IMMEUBLE_PROTEGE = "pip",
  PATRIMOINE_PERIMETRE_PROTECTION = "ppp",
  STC = "stc",
  PERIMETRE_GARES_MGP = "p8g",
  PLU = "plu",
  MONUMENTS_HISTORIQUES = "mh",
  LAND_UNIT_EDGES_LENGTH = "land-unit-edges-length",
}

export interface IudoMapboxLayer extends Partial<mapboxgl.Layer> {
  source: typeof MAP_SOURCE_NAMES[keyof typeof MAP_SOURCE_NAMES];
  "source-layer"?: typeof MAP_SOURCE_LAYERS[keyof typeof MAP_SOURCE_LAYERS];
}

export const initMapSources = () => {
  addIudoSource(MAP_SOURCE_NAMES.PLU, MAP_SOURCE_LAYERS.PLU, {
    minzoom: 11,
    maxzoom: 20,
  });
  addIudoSource(MAP_SOURCE_NAMES.CADASTRE, MAP_SOURCE_LAYERS.CADASTRE);
  addIudoSource(MAP_SOURCE_NAMES.STC, MAP_SOURCE_LAYERS.STC, {
    minzoom: getMapState.value.layers.areas.minzoom,
  });
  addIudoSource(
    MAP_SOURCE_NAMES.MONUMENTS_HISTORIQUES,
    MAP_SOURCE_LAYERS.MONUMENTS_HISTORIQUES
  );
  addSatelliteSource();

  addUserBuildingsSource();
};

let API_URL = process.env.VUE_APP_API_URL;
if (API_URL.startsWith("/")) {
  // mapbox source does not support relative url
  API_URL = window.location.origin + API_URL;
}

export const addIudoSource = (sourceName, sourceLayerName, params = {}) => {
  getMap.value.addSource(sourceName, {
    type: "vector",
    tiles: [API_URL + `/tiles/${sourceLayerName}/{z}/{x}/{y}.pbf`],
    minzoom: 14,
    maxzoom: 20,
    ...params,
  });
};

export const addSatelliteSource = () => {
  getMap.value.addSource(MAP_SOURCE_NAMES.SATELLITE, {
    type: "raster",
    url: "mapbox://mapbox.satellite",
  });
};
