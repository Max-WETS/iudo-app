import { watchEffect, watch } from "@vue/composition-api";
import {
  hideSatelliteLayer,
  showSatelliteLayer,
  showSatelliteLayerWithSomeOpacity,
} from "./layers/satellite";
import {
  showCadastreLayer,
  hideCadastreLayer,
  showCadastreLayerWithSomeOpacity,
} from "./layers/cadastre";
import router, { updateRouteLocation } from "@front/router";
import {
  getMapState,
  getMap,
  getSatelliteState,
  getCadastreState,
  getPluState,
  getBuildingsState,
} from "./getters";
import { MAP_LAYER_NAMES } from "./layers";
import { hidePoiLabelLayer, showPoiLabelLayer } from "./layers/poi-label";
import { bindUserParcelsWatcher } from "../user/watchers";
import { hidePluLayers, showPluLayers } from "./layers/plu";
import {
  activeUserBuildingByFeatureId,
  applyUserBuildingState,
  hideUserBuildings,
  showUserBuildings,
} from "./layers/buildings";
import { getActiveBuildingFeatureId } from "../user/getters";

export const initMapWatchers = () => {
  bindSatelliteLayerWatcher();
  bindMapRouteWatcher();
  bindUserParcelsWatcher();
  bindMapClickWatcher();
};

export const bindMapClickWatcher = () => {
  watch(
    () => getActiveBuildingFeatureId.value,
    (value, oldValue) => {
      if (value === null) {
        applyUserBuildingState("normal");
      } else {
        activeUserBuildingByFeatureId(value);
      }
    }
  );
};

export const bindSatelliteLayerWatcher = () => {
  watchEffect(() => {
    if (getSatelliteState() === true && getCadastreState() === false) {
      showPoiLabelLayer();
    } else {
      hidePoiLabelLayer();
    }

    if (getSatelliteState() === true) {
      if (getCadastreState() === true) {
        showSatelliteLayerWithSomeOpacity();
      } else {
        showSatelliteLayer();
      }
    } else {
      hideSatelliteLayer();
    }
    if (getCadastreState() === true) {
      showCadastreLayer();
    } else {
      if (getSatelliteState() === true) {
        showCadastreLayerWithSomeOpacity();
      } else {
        hideCadastreLayer();
      }
    }

    if (getPluState() === true) {
      showPluLayers();
    } else {
      hidePluLayers();
    }

    if (getBuildingsState() === true) {
      showUserBuildings();
    } else {
      hideUserBuildings();
    }
  });
};

export const bindMapRouteWatcher = () => {
  watch(
    () => [getMapState.value.center, getMapState.value.zoom],
    ([center, zoom], [prevCenter, prevZoom]) => {
      const isNewCenter = JSON.stringify(center) !== JSON.stringify(prevCenter);
      const isNewZoom = zoom !== prevZoom;

      if ((isNewCenter || isNewZoom) && typeof center !== "number") {
        updateRouteLocation({
          params: {
            lat: center && center.lat ? center.lat.toString() : null,
            lng: center && center.lng ? center.lng.toString() : null,
            zoom: zoom ? zoom.toString() : null,
          },
          query: router.currentRoute.query,
          replace: true,
        });
      }
    }
  );
};
