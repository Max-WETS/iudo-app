import { reactive } from "@vue/composition-api";
import { Map, LngLatBoundsLike } from "mapbox-gl";

export interface MapState {
  accessToken: string;
  style: string;
  draft: boolean;

  optimize: boolean;
  instance: null | Readonly<{
    wrapper: Map;
  }>;
  center: { lng: number; lat: number };
  maxBounds: LngLatBoundsLike;
  zoom: number;
  layerHoveredId: null | string | number;
  layers: {
    areas: {
      lastClicked: string;
      minzoom: number;
    };
    plu: boolean;
    buildings: boolean;
    satellite: boolean;
    cadastre: boolean;
  };
  popup: null | Readonly<{
    wrapper: mapboxgl.Popup;
  }>;
  isReady: boolean;
}

export const state = reactive<MapState>({
  isReady: false,
  accessToken:
    "pk.eyJ1IjoibWF4aW1lbGVicmV0b24iLCJhIjoiY2tjdnA2dTE1MDYxZjJybWpqdHRvZzl5ZyJ9.GZ6fbPGk7Jd9BDXFG8W8ig",
  style: "mapbox://styles/maximelebreton/ckcvp8duw0o3c1imuubit675o",
  optimize: true,
  draft: true,

  instance: null,
  center: null,
  maxBounds: [1.252441, 48.403908, 3.606262, 49.367784],
  zoom: null,
  layerHoveredId: null,
  layers: {
    areas: {
      lastClicked: null,
      minzoom: 10,
    },
    plu: false,
    buildings: false,
    satellite: true,
    cadastre: true,
  },
  popup: null,
});
