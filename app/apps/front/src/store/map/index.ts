export * from "./state";
export * from "./getters";
export * from "./methods";

export type MapStcProperties = {
  l_station: string;
  c_nature: string;
};

export type MapPcaProperties = {
  n_sq_pc: number;
};

export type MapMhProperties = {
  l_imp: string;
};

export type MapPcaFeature = GeoJSON.Feature<GeoJSON.Point, MapPcaProperties>;

export type MapStcFeature = GeoJSON.Feature<GeoJSON.Point, MapStcProperties>;

export type MapMhFeature = GeoJSON.Feature<GeoJSON.Point, MapMhProperties>;
