import { getMap, getMapLayerHoveredId } from "./getters";
import { MAP_LAYER_NAMES } from "./layers";
import {
  setMapCenter,
  setMapZoom,
  setMapLayerHoveredId,
  handleMapResize,
} from "./methods";

import { MapStcFeature, MapPcaFeature, MapMhFeature } from ".";
import {
  toggleStcArea,
  onStcClick,
  onStcMouseEnter as onStcMouseMove,
  onStcMouseLeave,
} from "./layers/stc";
import {
  onCadastreClick,
  onCadastreMouseMove,
  onCadastreMouseLeave,
} from "./layers/cadastre";
import { state } from "./state";
import {
  onMhClick,
  onHistoricMonumentsMouseMove,
  onHistoricMonumentsMouseLeave,
} from "./layers/mh";
import { hideArea } from "./layers/areas";
import { debounce } from "lodash";
import {
  onUserBuildingsClick,
  onUserBuildingsMouseLeave,
  onUserBuildingsMouseMove,
} from "./layers/buildings";
import { Building } from "@iudo/data";

export const initMapEvents = () => {
  bindMapClickEvents();
  bindMapMoveEndEvent();
  bindMapZoomEndEvent();
  bindMapMouseEnter();
  bindMapMouseMove();
  bindMapMouseLeave();
  bindMapWindowResize();
};

export const bindMapWindowResize = () => {
  var debounced = debounce(handleMapResize, 250);

  const onWindowResize = window.addEventListener("resize", debounced);
};

export const bindMapMouseEnter = () => {
  // When the mouse leaves the state-fill layer, update the feature state of the
  // previously hovered feature.
  // getMap.value.on("mouseenter", MAP_LAYER_NAMES.CADASTRE_FILLS, function () {});
};

export const bindMapMouseMove = () => {
  getMap.value.on("mousemove", MAP_LAYER_NAMES.CADASTRE_FILLS, function (e) {
    if (e.features.length > 0) {
      const feature = (e.features[0] as unknown) as MapPcaFeature;
      //onCadastrefeatureStateHover(e);
      if (!e.isPointHovered) {
        onCadastreMouseMove(feature);
      }
    }
  });
  getMap.value.on("mousemove", MAP_LAYER_NAMES.MONUMENTS_HISTORIQUES, function (
    e
  ) {
    e.isPointHovered = true;
    if (e.features.length > 0) {
      const feature = (e.features[0] as unknown) as MapMhFeature;
      onHistoricMonumentsMouseMove(feature);
    }
  });
  getMap.value.on("mousemove", MAP_LAYER_NAMES.STC_SYMBOLS, function (e) {
    e.isPointHovered = true;
    if (e.features.length > 0) {
      const feature = (e.features[0] as unknown) as MapStcFeature;
      onStcMouseMove(feature);
    }
  });
  getMap.value.on("mousemove", MAP_LAYER_NAMES.USER_BUILDINGS_FILLS, function (
    e
  ) {
    if (e.features.length > 0) {
      const feature = (e.features[0] as unknown) as Building["geometry"];

      if (!e.isPointHovered) {
        onUserBuildingsMouseMove(feature);
      }
    }
  });
};

export const bindMapMouseLeave = () => {
  // When the mouse leaves the state-fill layer, update the feature state of the
  // previously hovered feature.
  getMap.value.on("mouseleave", MAP_LAYER_NAMES.CADASTRE_FILLS, function () {
    onCadastreMouseLeave();
  });
  getMap.value.on(
    "mouseleave",
    MAP_LAYER_NAMES.MONUMENTS_HISTORIQUES,
    function (e) {
      e.isPointHovered = false;
      onHistoricMonumentsMouseLeave();
    }
  );
  getMap.value.on("mouseleave", MAP_LAYER_NAMES.STC_SYMBOLS, function (e) {
    e.isPointHovered = false;
    onStcMouseLeave();
  });
  getMap.value.on("mouseleave", MAP_LAYER_NAMES.USER_BUILDINGS_FILLS, function (
    e
  ) {
    e.isPointHovered = false;
    onUserBuildingsMouseLeave();
  });
};

const bindMapMoveEndEvent = () => {
  getMap.value.on("moveend", ({ originalEvent }) => {
    setMapCenter(getMap.value.getCenter());

    // if (originalEvent) {
    //   getMap.value.fire("usermoveend");
    // } else {
    //   getMap.value.fire("flyend");
    // }
  });
};

const bindMapZoomEndEvent = () => {
  getMap.value.on("zoomend", ({ originalEvent }) => {
    setMapZoom(getMap.value.getZoom());
  });
};

const bindMapClickEvents = () => {
  bindUserBuildingsClick();
  bindStcLayerClick();
  bindMhLayerClick();
  bindCadastreLayerClick();
  bindMapClick();
};

export type MapboxClickEvent = mapboxgl.MapMouseEvent & {
  features?: mapboxgl.MapboxGeoJSONFeature[];
} & mapboxgl.EventData;

export const bindMapClick = () => {
  getMap.value.on("click", async (e) => {
    if (
      !e.isStcLayerClicked &&
      !e.isMhLayerClicked &&
      !e.isUserBuildingsClicked
    ) {
      hideArea();
    }
  });
};

const bindCadastreLayerClick = () => {
  getMap.value.on("click", MAP_LAYER_NAMES.CADASTRE_FILLS, async (e) => {
    if (
      !e.isStcLayerClicked &&
      !e.isMhLayerClicked &&
      !e.isUserBuildingsClicked
    ) {
      onCadastreClick(e);
    }
  });
};

const bindStcLayerClick = () => {
  getMap.value.on("click", MAP_LAYER_NAMES.STC_SYMBOLS, async (e) => {
    e.isStcLayerClicked = true;
    onStcClick(e);
  });
};

const bindMhLayerClick = () => {
  getMap.value.on("click", MAP_LAYER_NAMES.MONUMENTS_HISTORIQUES, async (e) => {
    e.isMhLayerClicked = true;
    onMhClick(e);
  });
};

const bindUserBuildingsClick = () => {
  getMap.value.on("click", MAP_LAYER_NAMES.USER_BUILDINGS_FILLS, async (e) => {
    e.isUserBuildingsClicked = true;
    onUserBuildingsClick(e);
  });
};
