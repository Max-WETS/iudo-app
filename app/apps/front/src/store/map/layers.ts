import { addCadastreLayers } from "./layers/cadastre";
import { addStcLayers } from "./layers/stc";
import { addSatelliteLayer } from "./layers/satellite";
import { addMhLayer } from "./layers/mh";
import { addPluLayers } from "./layers/plu";
import { addUserBuildingsLayers } from "./layers/buildings";
import { ui } from "../ui";
import { addLandUnitLengthsLayer } from "./layers/land-unit-lengths";

export enum MAP_LAYER_NAMES {
  SATELLITE = "satellite-layer",
  LAND_UNIT_EDGES_LENGTH = "land-unit-edges-length",
  CADASTRE_FILLS = "cadastre-fills-layer",
  CADASTRE_HIGHLIGHTED = "cadastre-highlighted-layer",
  CADASTRE_SELECTED_LINES = "cadastre-selected-lines-layer",
  CADASTRE_SELECTED_FILLS = "cadastre-selected-fills-layer",
  CADASTRE_LINES = "cadastre-lines-layer",
  STC_SYMBOLS = "stc-symbol-layer",
  STC_AREAS = "stc-areas-layer",
  POI_LABEL = "poi-label",
  MONUMENTS_HISTORIQUES = "mh-layer",
  PLU_FILLS = "plu-fills-layer",
  PLU_LINES = "plu-lines-layer",
  PLU_TEXT = "plu-text-layer",
  // BUILDING_FILLS = "building",
  // BUILDING_LINES = "building-outline",
  USER_BUILDINGS_FILLS = "user-buildings-fills",
  USER_BUILDINGS_LINES = "user-buildings-lines",
  USER_BUILDINGS_TEXT = "user-buildings-text",
}

export const initMapLayers = () => {
  addCadastreLayers();
  addStcLayers();
  addMhLayer();
  addUserBuildingsLayers();
  addSatelliteLayer();
  addPluLayers();
  addLandUnitLengthsLayer();
};

export const defaultTransition: mapboxgl.Transition = {
  duration: 300,
  delay: 0,
};

export const layerStates = {
  "user-buildings-fills": {
    normal: {
      paint: {
        "fill-color": `rgba(${ui.colors.rgb.blue}, 0)`,
        // "fill-opacity": cadastreOpacityInterpolation,
        "fill-color-transition": defaultTransition,
      },
    },
    hover: {
      paint: {
        "fill-color": `rgba(${ui.colors.rgb.blue}, .1)`,
        "fill-color-transition": defaultTransition,
      },
    },
    active: {
      paint: {
        "fill-color": `rgba(${ui.colors.rgb.blue}, .25)`,
        "fill-color-transition": defaultTransition,
      },
    },
  },
  "user-buildings-lines": {
    normal: {
      paint: {
        "line-color": `rgba(${ui.colors.rgb.blue}, 0)`,
        // "fill-opacity": cadastreOpacityInterpolation,
        "line-color-transition": defaultTransition,
      },
    },
    hover: {
      paint: {
        "line-color": `rgba(${ui.colors.rgb.blue}, .5)`,
        "line-color-transition": defaultTransition,
      },
    },
    active: {
      paint: {
        "line-color": `rgba(${ui.colors.rgb.blue}, 1)`,
        "line-color-transition": defaultTransition,
      },
    },
  },
} as {
  [key in MAP_LAYER_NAMES]: {
    [key in "normal" | "hover" | "active"]: Partial<mapboxgl.Layer>;
  };
};
