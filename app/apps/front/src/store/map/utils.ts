export const getOpacityExpression = (
  from: {
    zoom: number;
    opacity: number;
  },
  to: { zoom: number; opacity: number }
): mapboxgl.Expression => {
  return [
    "interpolate",
    ["linear"],
    ["zoom"],
    from.zoom,
    from.opacity,
    to.zoom,
    to.opacity,
  ];
};

export function timeout(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
