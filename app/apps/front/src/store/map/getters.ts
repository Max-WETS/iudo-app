import { computed } from "@vue/composition-api";
import { state } from "./state";
import {
  setBuildingsState,
  setCadastreState,
  setPluState,
  setSatelliteState,
} from "./methods";

export const getMapState = computed(() => {
  return state;
});

export const getMap = computed(() => {
  //return MAP_INSTANCE.instance;
  //return MAP.instance;
  return getMapState.value.instance.wrapper;
});

export const isMapReady = computed(() => {
  return getMapState.value.isReady;
});

export const getMapLayerHoveredId = computed(() => {
  return getMapState.value.layerHoveredId;
});

export const getMapStyle = computed(() => {
  const draftSuffix = getMapState.value.draft ? "/draft" : "";
  const optimizeSuffix = getMapState.value.optimize ? "?optimize=true" : "";
  return getMapState.value.style + draftSuffix + optimizeSuffix;
});

export const getMapAccessToken = computed(() => {
  return getMapState.value.accessToken;
});

export const getMapCenter = computed(() => {
  return getMapState.value.center;
});

export const getMapZoom = computed(() => {
  return getMapState.value.zoom;
});

export const getMapMaxBounds = computed(() => {
  return getMapState.value.maxBounds;
});

export const getSatelliteState = () => {
  return getMapState.value.layers.satellite;
};

export const getCadastreState = () => {
  return getMapState.value.layers.cadastre;
};

export const getPluState = () => {
  return getMapState.value.layers.plu;
};

export const getBuildingsState = () => {
  return getMapState.value.layers.buildings;
};

export const cadastreModel = computed({
  get: getCadastreState,
  set: (value) => {
    setCadastreState(value);
  },
});

export const satelliteModel = computed({
  get: getSatelliteState,
  set: (value) => {
    setSatelliteState(value);
  },
});

export const pluModel = computed({
  get: getPluState,
  set: (value) => {
    setPluState(value);
  },
});

export const buildingsModel = computed({
  get: getBuildingsState,
  set: (value) => {
    setBuildingsState(value);
  },
});

export const getLastClickedAreas = computed(
  () => state.layers.areas.lastClicked
);

export const getMapPopup = computed(() =>
  state.popup ? state.popup.wrapper : null
);

export const isMapPopup = computed(() => (state.popup ? true : false));
