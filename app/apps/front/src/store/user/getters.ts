import state from "./state";
import { computed } from "@vue/composition-api";
import {
  formatAsSquareMeters,
  formatAsYesNo,
  formatAsPercent,
  formatAsMeters,
  formatAsPricePerSquareMeter,
} from "@iudo/shared/utils";
import { Risk, Nearby } from "@iudo/data";

import { ScrollIds } from "@front/router/routes";
import { CharacteristicBlock, LocalisationBlock, RisksBlock } from "../types";
import { setShowBuildingsDetails } from "./methods";
import router from "@front/router";

export const getUserState = computed(() => state);

export const isLoggedUser = computed(() => state.isLoggedUser);

export const getUserParcels = computed(() => state.parcels);

export const getUserNeighborsIds = computed(() => state.neighbors);

export const getUserParcelsIds = computed(() =>
  getUserParcels.value.map((parcel) => parcel.id)
);

export const getUserParcelsCsp = computed(() =>
  getUserParcels.value.map((parcel) => {
    return `${parcel.csp}`;
  })
);

export const getUserParcelsCspAsString = computed(() =>
  getUserParcelsCsp.value.join(", ")
);

export const hasUserParcels = computed(() =>
  getUserParcelsIds.value.length ? true : false
);

export const hasUserNeighbors = computed(() =>
  getUserNeighborsIds.value.length ? true : false
);

export const hasUserLandUnit = computed(() =>
  Object.entries(getUserLandUnit.value).length === 0 ? false : true
);

export const getUserSubmitedAddress = computed(() => {
  return state.submitedAddress;
});

export const getUserLandUnit = computed(() => state.landUnit);

// export const getUserLandUnitHouseNumber = computed(
//   () => getUserLandUnit.value.housenumber
// );

export const getUserLandUnitAddress = computed(() => state.landUnitAddress);

export const getUserAddressStreet = computed(() =>
  // getUserSubmitedAddress.value && getUserSubmitedAddress.value.street
  //   ? getUserSubmitedAddress.value.street
  //   :
  getUserLandUnitAddress.value && getUserLandUnitAddress.value.street
    ? getUserLandUnitAddress.value.street
    : getUserLandUnitAddress.value.name
    ? getUserLandUnitAddress.value.name
    : ""
);

export const getUserAddressHouseNumber = computed(() =>
  // getUserSubmitedAddress.value &&
  // getUserSubmitedAddress.value.type === "housenumber"
  //   ? getUserSubmitedAddress.value.housenumber
  //   :
  getUserLandUnitAddress.value &&
  getUserLandUnitAddress.value.type === "housenumber"
    ? getUserLandUnitAddress.value.housenumber
    : ""
);

export const getUserAddressPostCode = computed(() =>
  // getUserSubmitedAddress.value
  //   ? getUserSubmitedAddress.value.postcode
  //   :
  getUserLandUnitAddress.value ? getUserLandUnitAddress.value.postcode : ""
);

export const getUserAddressCity = computed(() =>
  // getUserSubmitedAddress.value
  //   ? getUserSubmitedAddress.value.city
  //   :
  getUserLandUnitAddress.value ? getUserLandUnitAddress.value.city : ""
);

export const getUserAddressLabel = computed(() =>
  // getUserSubmitedAddress.value && getUserSubmitedAddress.value.label
  //   ? getUserSubmitedAddress.value.label
  //   :
  state.landUnitAddress && state.landUnitAddress.label
    ? state.landUnitAddress.label
    : ""
);

export const getUserLandUnitSurface = computed(() => {
  return getUserLandUnit.value.surface_fisc
    ? getUserLandUnit.value.surface_fisc
    : null;
});

export const getUserLandUnitBuildingExtent = computed(() =>
  getUserLandUnit.value.building_extent
    ? getUserLandUnit.value.building_extent
    : null
);

export const getUserLandUnitBuildingSurface = computed(() => {
  return getUserLandUnit.value.building_extent
    ? getUserLandUnit.value.building_extent.surface
    : null;
});

export const getUserLandUnitBuildingFootprint = computed(() => {
  if (
    getUserLandUnitSurface.value &&
    getUserLandUnitBuildingSurface.value !== null
  ) {
    return (
      (getUserLandUnitBuildingSurface.value * 100) /
      getUserLandUnitSurface.value
    );
  } else {
    return null;
  }
});

export const getUserLandUnitBuildingMedianHeight = computed(() => {
  return getUserLandUnit.value.building_extent
    ? getUserLandUnit.value.building_extent.height_med
    : null;
});

export const getUserLandUnitBuildingMinHeight = computed(() => {
  return getUserLandUnit.value.building_extent
    ? getUserLandUnit.value.building_extent.height_min
    : null;
});

export const getUserLandUnitBuildingMaxHeight = computed(() => {
  return getUserLandUnit.value.building_extent
    ? getUserLandUnit.value.building_extent.height_max
    : null;
});

export const getUserLandUnitBuildings = computed(() =>
  getUserLandUnit.value.buildings ? getUserLandUnit.value.buildings : null
);

export const getUserLandUnitUrbanZones = computed(() => {
  return getUserLandUnit.value.urban_zones
    ? getUserLandUnit.value.urban_zones
    : null;
});

export const getUserLandUnitUrbanZonesAsString = computed(() => {
  return getUserLandUnitUrbanZones.value
    ? getUserLandUnitUrbanZones.value.map((zone) => zone.label).join(", ")
    : "";
});

export const getUserLandUnitRisks = computed(() => state.landUnitRisks);

export const getUserLandUnitNearBy = computed(
  () => getUserLandUnit.value.nearby
);

export const getUserLandUnitPrices = computed(
  () => getUserLandUnit.value.prices
);

export const getUserLandUnitRentalPrice = computed(() =>
  getUserLandUnit.value.prices ? getUserLandUnit.value.prices.rental : null
);
export const getUserLandUnitSellingPrice = computed(() =>
  getUserLandUnit.value.prices ? getUserLandUnit.value.prices.selling : null
);

export const isUserLandUnitTremorRisk = computed(() => {
  return getUserLandUnitRisks.value
    ? getUserLandUnitRisks.value.includes(Risk.tremor)
    : null;
});

export const isUserLandUnitClayRisk = computed(() => {
  return getUserLandUnitRisks.value
    ? getUserLandUnitRisks.value.includes(Risk.clay)
    : null;
});

export const isUserLandUnitFloodRisk = computed(() => {
  return getUserLandUnitRisks.value
    ? getUserLandUnitRisks.value.includes(Risk.flood)
    : null;
});

export const isUserLandUnitCavityRisk = computed(() => {
  return getUserLandUnitRisks.value
    ? getUserLandUnitRisks.value.includes(Risk.cavity)
    : null;
});

export const isUserLandUnitNearByTransport = computed(() => {
  return getUserLandUnitNearBy.value
    ? getUserLandUnitNearBy.value.includes(Nearby.transport)
    : null;
});

export const isUserLandUnitNearByHistoricMonument = computed(() => {
  return getUserLandUnitNearBy.value
    ? getUserLandUnitNearBy.value.includes(Nearby.historic_monument)
    : null;
});

export const isUserLandUnitNearByGpeStation = computed(() => {
  return getUserLandUnitNearBy.value
    ? getUserLandUnitNearBy.value.includes(Nearby.gpe_station)
    : null;
});

export const getCharacteristicsBlock = computed(() => {
  return [
    {
      key: CharacteristicBlock.SURFACE_UNITE_FONCIERE,
      value: formatAsSquareMeters(getUserLandUnitSurface.value),
      rawValue: formatAsSquareMeters(getUserLandUnitSurface.value, false),
    },
    {
      key: CharacteristicBlock.SURFACE_BATIE,
      value: formatAsSquareMeters(getUserLandUnitBuildingSurface.value),
      rawValue: formatAsSquareMeters(
        getUserLandUnitBuildingSurface.value,
        false
      ),
    },
    {
      key: CharacteristicBlock.EMPRISE_AU_SOL,
      value: formatAsPercent(getUserLandUnitBuildingFootprint.value),
      rawValue: formatAsPercent(getUserLandUnitBuildingFootprint.value, false),
    },
    {
      key: CharacteristicBlock.HAUTEUR_MEDIANE,
      value: formatAsMeters(getUserLandUnitBuildingMedianHeight.value),
      rawValue: formatAsMeters(
        getUserLandUnitBuildingMedianHeight.value,
        false
      ),
    },
  ];
});

export const LocalisationItems = computed(() => ({
  ZONE_PLU: {
    key: LocalisationBlock.ZONE_PLU,
    description:
      isLoggedUser.value &&
      getUserLandUnitUrbanZones.value[0] &&
      getUserLandUnitUrbanZones.value[0].link
        ? `
    <a href="${getUserLandUnitUrbanZones.value[0].link}" target="_blank">liens -></a>
    `
        : "",
    value: getUserLandUnitUrbanZonesAsString.value,
    rawValue: getUserLandUnitUrbanZonesAsString.value,
  },
  STATION_DE_TRANSPORT: {
    key: LocalisationBlock.STATION_DE_TRANSPORT,
    value: formatAsYesNo(isUserLandUnitNearByTransport.value),
    rawValue: formatAsYesNo(isUserLandUnitNearByTransport.value),
    // tooltip: LocalisationBlock.TCSP,
  },
  MONUMENT_HISTORIQUE: {
    key: LocalisationBlock.MONUMENT_HISTORIQUE,
    value: formatAsYesNo(isUserLandUnitNearByHistoricMonument.value),
    rawValue: formatAsYesNo(isUserLandUnitNearByHistoricMonument.value),
  },
  FUTURE_GARE: {
    key: LocalisationBlock.FUTURE_GARE,
    value: formatAsYesNo(isUserLandUnitNearByGpeStation.value),
    rawValue: formatAsYesNo(isUserLandUnitNearByGpeStation.value),
    // tooltip: LocalisationBlock.TOOLTIP_FUTURE_GARE,
  },
  LOCATION: {
    key: LocalisationBlock.LOCATION,
    value: formatAsPricePerSquareMeter(getUserLandUnitRentalPrice.value),
    rawValue: getUserLandUnitRentalPrice.value,
  },
  VENTE: {
    key: LocalisationBlock.VENTE,
    value: formatAsPricePerSquareMeter(getUserLandUnitSellingPrice.value),
    rawValue: getUserLandUnitSellingPrice.value,
  },
}));

export const RisksItems = computed(() => ({
  INONDATIONS: {
    key: RisksBlock.INONDATIONS,
    value: formatAsYesNo(isUserLandUnitFloodRisk.value),
    rawValue: formatAsYesNo(isUserLandUnitFloodRisk.value),
  },
  CAVITES_SOUTERRAINES: {
    key: RisksBlock.CAVITES_SOUTERRAINES,
    value: formatAsYesNo(isUserLandUnitCavityRisk.value),
    rawValue: formatAsYesNo(isUserLandUnitCavityRisk.value),
  },
  MOUVEMENT_DE_TERRAIN: {
    key: RisksBlock.MOUVEMENT_DE_TERRAIN,
    value: formatAsYesNo(isUserLandUnitTremorRisk.value),
    rawValue: formatAsYesNo(isUserLandUnitTremorRisk.value),
  },
  ARGILES: {
    key: RisksBlock.ARGILES,
    value: formatAsYesNo(isUserLandUnitClayRisk.value),
    rawValue: formatAsYesNo(isUserLandUnitClayRisk.value),
  },
}));

export const getLocalisationBlock = computed(() => {
  return [
    LocalisationItems.value.ZONE_PLU,
    LocalisationItems.value.STATION_DE_TRANSPORT,
    LocalisationItems.value.MONUMENT_HISTORIQUE,
    LocalisationItems.value.FUTURE_GARE,
  ];
});
export const getSpecialUserLocalisationBlock = computed(() => {
  return [
    LocalisationItems.value.ZONE_PLU,
    LocalisationItems.value.STATION_DE_TRANSPORT,
    LocalisationItems.value.MONUMENT_HISTORIQUE,
    LocalisationItems.value.FUTURE_GARE,
    LocalisationItems.value.LOCATION,
    LocalisationItems.value.VENTE,
  ];
});

export const getRisksBlock = computed(() => {
  return [
    RisksItems.value.INONDATIONS,
    RisksItems.value.CAVITES_SOUTERRAINES,
    RisksItems.value.MOUVEMENT_DE_TERRAIN,
    RisksItems.value.ARGILES,
  ];
});

// export const isFormUserStep = computed(() =>
//   getRoute.value &&
//   getRoute.value.wrapper &&
//   getRoute.value.wrapper.params &&
//   getRoute.value.wrapper.params.route === "form"
//     ? true
//     : false
// );

export const isFormUserStep = computed(
  () => getRoute.value && getRoute.value.params.step === "form"
);

export const getRouteStep = computed(() =>
  getRoute.value && getRoute.value.params && getRoute.value.params.step
    ? getRoute.value.params.step
    : null
);

export const getRoute = computed(() => getUserState.value.route);
export const getRoutePath = computed(() => getUserState.value.route.path);

export const isEmbed = computed(() =>
  window.location.pathname.includes("/embed")
);

export const isResolvedRoute = computed(() => {
  return getRoute.value.name !== "404";
});

export const isFocusRoute = computed(
  // () => getRoute.value && getRoute.value.name === "Focus"
  () => getRoute.value && getRoute.value.path.includes("/focus")
);

export const getStepNumber = computed(() => {
  if (getRouteStep.value === ScrollIds.POTENTIAL) {
    return 1;
  } else if (getRouteStep.value === ScrollIds.FORM) {
    return 2;
  } else {
    return 0;
  }
});

// export const showUserBuildingsModel = computed({
//   get() {
//     return state.showBuildingDetails;
//   },
//   set(value: boolean) {
//     setShowBuildingsDetails(value);
//   },
// });

export const getShowBuildingsDetails = computed(
  () => state.showBuildingDetails
);
export const getActiveBuildingFeatureId = computed(
  () => state.activeBuildingFeatureId
);

export const getUserName = computed(() => state.profile.name);
export const getUserEmail = computed(() => state.profile.email);
export const getIsSaving = computed(() => state.isSaving);
