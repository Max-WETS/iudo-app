import { reactive, watch } from "@vue/composition-api";
import { Parcel, LandUnit, Risk, AddressApi } from "@iudo/data";
import { Route } from "vue-router";
import { SearchFeatureProperties } from "libs/data/src/lib/api-adresse";

export type E = { [key in Risk]? };
export type Z = keyof E;

export interface UserState {
  parcels: Parcel[];
  neighbors: number[];
  submitedAddress: SearchFeatureProperties;
  landUnit: Partial<LandUnit>;
  landUnitAddress: Partial<AddressApi.SearchFeatureProperties>;
  landUnitRisks: Risk[];
  isLoggedUser: boolean;
  isSaving: boolean;
  /**
   * Route without "matched" property because of circular reference problem
   */
  route: Omit<Route, "matched">;
  showBuildingDetails: boolean;
  activeBuildingFeatureId: number | string;
  profile: {
    name: string;
    email: string;
  };
}

export default reactive<UserState>({
  parcels: [],
  neighbors: [],
  submitedAddress: null,
  landUnit: {},
  landUnitAddress: {},
  landUnitRisks: null,
  route: null,
  isLoggedUser: false,
  showBuildingDetails: false,
  activeBuildingFeatureId: null,
  profile: null,
  isSaving: false,
});
