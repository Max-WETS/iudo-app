import { watch } from "@vue/composition-api";
import { getUserParcelsIds, getRouteStep, getRoute } from "./getters";
import {
  updateCadastreSelectedLayers,
  getParcelsIdsQueryFromArray,
} from "../parcels";
import {
  checkUserLandUnit,
  resetStep,
  updateParcelsAndNeighbors,
} from "./methods";
import { updateRouteLocation } from "@front/router";

import { ScrollIds } from "@front/router/routes";
import { updateUserBuildingsSource } from "../map/layers/buildings";
import { hideForm, showForm } from "../form";

export const bindFormWatcher = () => {
  watch(
    () => getRouteStep.value,
    (step, OldStep) => {
      if (step && step === ScrollIds.FORM) {
        showForm();
      } else {
        hideForm();
      }
    }
  );
};

export const bindUserParcelsWatcher = () => {
  watch(
    () => getUserParcelsIds.value,
    async (parcelsIds, OldParcelsIds) => {
      const isNewParcels =
        JSON.stringify(parcelsIds) !== JSON.stringify(OldParcelsIds);

      if (isNewParcels) {
        updateCadastreSelectedLayers();

        await Promise.all([
          await updateParcelsAndNeighbors(parcelsIds),
          await checkUserLandUnit(parcelsIds),
        ]);
        updateUserBuildingsSource();

        // if (!getMapCenter.value.lat && !getMapCenter.value.lng) {

        // }

        const options = {
          query: {
            parcels: getParcelsIdsQueryFromArray.value,
          },
        };

        if (getUserParcelsIds.value.length === 1 && !getRouteStep.value) {
          // scrollToElement(`#${ScrollIds.ADDRESS}`);
          updateRouteLocation({
            ...options,
            merge: true,
            replace: false,
            params: { step: ScrollIds.ADDRESS },
          });
        } else if (getUserParcelsIds.value.length === 0) {
          // clearUserLandUnitInfos();
          updateRouteLocation({
            ...options,
            merge: true,
            replace: true,
            params: { step: null },
          });
        } else {
          updateRouteLocation({
            ...options,
            replace: true,
          });
        }
      }
    }
  );
};
