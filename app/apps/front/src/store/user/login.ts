import router from "@front/router";
import HTTPError, { fetchApi } from "@iudo/shared/utils";
import { computed } from "@vue/composition-api";
import { resolve } from "url";
import { showNotification } from "../global";
import { getRoute, isLoggedUser } from "./getters";
import { setIsLoggedUser } from "./methods";
import state from "./state";

const API_URL = process.env.VUE_APP_API_URL;

export const fetchUserAuth = async () => {
  return new Promise<void>(async (resolve, reject) => {
    try {
      const result = await fetchApi(`${API_URL}/user`);
      console.log(result)
      if (!isLoggedUser.value) {
        handleUserLoginSuccess(result);
      }
      resolve();
    } catch (error) {
      handleUserLoginError(error);
      reject(error);
    }
  });
};

export const handleUserLoginSuccess = ({
  name,
  email,
}: Partial<typeof state.profile>) => {
  setUserProfile({
    email,
    name,
  });
  setIsLoggedUser(true);
  showNotification({
    text: "Vous êtes connecté avec l'adresse " + email,
    type: "success",
  });
};
export const handleUserLoginError = (error: HTTPError) => {
  setIsLoggedUser(false);

  if (error.message === "401: User not found") {
    showNotification({
      text: "Désolé, ce compte utilisateur n'existe pas",
      type: "error",
    });
  } else if (error.message === "401: Unauthenticated") {
    showNotification({
      text: "Vous devez être connecté pour accéder à cette page",
      type: "error",
    });
  } else {
    showNotification({
      text: error.message,
      type: "error",
    });
  }
};

// export const handeUserLogin = (result: { error?: string }) => {
//   const { error, name, email } = result;

//   if (name && email) {
//   } else {
//   }
// };

export const loginWithEmail = async (email: string) => {
  try {
    const result = await fetchApi(
      `${API_URL}/user`,
      { email },
      {
        method: "POST",
      }
    );
    handleUserLoginSuccess(result);
  } catch (error) {
    handleUserLoginError(error);
  }
};

export const logout = async () => {
  const result = await fetchApi(`${API_URL}/user/logout`);
  if (result.success === true) {
    setUserProfile(null);
    setIsLoggedUser(false);
    router.go(0);
  }
};

export const goToApp = () => {
  window.location.assign("https://app.iudo.co");
};

export const setUserProfile = (profile: typeof state.profile) => {
  state.profile = profile;
};
export const getUserProfile = computed(() => state.profile);
