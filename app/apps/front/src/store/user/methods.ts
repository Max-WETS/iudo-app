import Vue from "vue";
import state, { UserState } from "./state";
import { Parcel, LandUnit, Risk } from "@iudo/data";
import { AddressApi } from "@iudo/data";
import {
  getUserLandUnitAddress,
  getUserNeighborsIds,
  getUserParcels,
  getUserParcelsIds,
} from "./getters";
import {
  // fetchParcelsNeighbors,
  fetchLandUnits,
  fetchParcels,
} from "@iudo/shared/lib/api-parcels";
import {
  // fetchParcelsNeighbors,
  fetchAddressFromCoordinates,
} from "../parcels";
import { Route } from "vue-router";
import { updateRouteLocation } from "@front/router";
import { ScrollIds } from "@front/router/routes";
import { clearForm } from "../form";
import { buildingsModel } from "../map";

export * from "./login";

export const setIsLoggedUser = (value: boolean) => {
  state.isLoggedUser = value;
};

export const addToUserParcels = (parcels: Parcel[]) => {
  //state.parcels.push(parcel);
  state.parcels = [...state.parcels, ...parcels];
};
export const setUserParcels = (parcels: Parcel[]) => {
  state.parcels = parcels;
};
export const clearUserParcels = () => {
  state.parcels = [];
};

export const setUserLandUnitAddress = (
  address: AddressApi.SearchFeatureProperties
) => {
  state.landUnitAddress = address;
};
export const clearUserLandUnitAddress = () => setUserLandUnitAddress(null);

export const clearUserLandUnitInfos = () => {
  clearUserLandUnitAddress();
  clearUserLandUnit();
  clearUserSubmitedAddress();
};

export const setUserSubmitedAddress = (
  address: AddressApi.SearchFeatureProperties
) => {
  state.submitedAddress = address;
};
export const clearUserSubmitedAddress = () => setUserSubmitedAddress(null);

export const setUserLandUnit = (landUnit: LandUnit | {}) => {
  Vue.set(state, "landUnit", landUnit);
};
export const clearUserLandUnit = () => setUserLandUnit({});

export const removeFromUserParcels = (parcelId: Parcel["id"]) => {
  state.parcels.splice(
    state.parcels.findIndex(function (parcel) {
      return parcel.id.toString() === parcelId.toString();
    }),
    1
  );
};

export const setUserNeighbors = (neighbors: number[]) => {
  Vue.set(state, "neighbors", neighbors);
};
export const removeUserNeighbors = () => {
  Vue.set(state, "neighbors", []);
};

export const isNeighbor = (parcelId: number) => {
  return getUserNeighborsIds.value.includes(parcelId);
};

export const isUserParcel = (parcelId: number) => {
  return getUserParcelsIds.value.includes(parcelId);
};

export const updateParcelsAndNeighbors = async (
  parcelsIds: Array<Parcel["id"]>
) => {
  if (parcelsIds.length) {
    const { neighbors, parcels } = await fetchParcels(parcelsIds);
    setUserParcels(parcels);
    setUserNeighbors(neighbors);
  }
};

export const fetchLandUnitRisks = async (url: string): Promise<Risk[]> => {
  const response = await fetch(url);
  const result = response.json();
  return result;
};

export const setUserLandUnitRisks = (value: Risk[]) => {
  Vue.set(state, "landUnitRisks", value);
};

export const checkUserLandUnit = async (parcelsIds: Array<Parcel["id"]>) => {
  if (parcelsIds.length) {
    const landUnits = await fetchLandUnits(parcelsIds);
    const landUnit = landUnits[0];
    const buildingsWithIndexes = landUnit.buildings.map((building, index) => ({
      ...building,
      index: index + 1,
    }));
    landUnit.buildings = buildingsWithIndexes;
    setUserLandUnit(landUnit);

    const risksUrl = landUnit.risks;

    const [landUnitRisks, landUnitAddress] = await Promise.all([
      await fetchLandUnitRisks(risksUrl),
      await fetchAddressFromCoordinates(landUnit.coordinates),
    ]);

    setUserLandUnitRisks(landUnitRisks);

    setUserLandUnitAddress(landUnitAddress);
  } else {
    setUserLandUnit({});
    setUserLandUnitRisks(null);
    setUserLandUnitAddress(null);
  }
};

export const setRoute = (route: UserState["route"]) => {
  state.route = route;
};

export const goToStep = (step: ScrollIds, replace = false) =>
  updateRouteLocation({
    merge: true,
    replace: replace,
    params: { step: step },
  });
export const resetStep = () => {
  updateRouteLocation({
    merge: true,
    replace: true,
    params: { step: null },
  });
};
export const setShowBuildingsDetails = (value: boolean) => {
  state.showBuildingDetails = value;
};
export const setActiveBuildingFeatureId = (
  value: typeof state.activeBuildingFeatureId
) => {
  state.activeBuildingFeatureId = value;
};

export const setIsSaving = (value: boolean) => {
  state.isSaving = value;
};
