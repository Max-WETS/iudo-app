import state from "./state";
import * as getters from "./getters";
import * as methods from "./methods";
import * as watchers from "./watchers";
import { toRefs } from "@vue/composition-api";

export const useUser = () => ({
  ...toRefs(state),
  ...getters,
  ...methods,
  ...watchers,
});
