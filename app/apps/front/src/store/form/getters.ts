import { state } from "./state";
import { computed } from "@vue/composition-api";
import {
  setFormName,
  setFormEmail,
  setFormMessage,
  setFormPhone,
  setFormCgu,
} from "./methods";

export const getFormName = computed(() => state.fields.name);
export const getFormEmail = computed(() => state.fields.email);
export const getFormMessage = computed(() => state.fields.message);
export const getFormPhone = computed(() => state.fields.phone);

export const getFormErrors = computed(() => state.errors);

export const formNameModel = computed({
  get() {
    return state.fields.name;
  },
  set(value: string) {
    setFormName(value);
  },
});

export const formEmailModel = computed({
  get() {
    return state.fields.email;
  },
  set(value: string) {
    setFormEmail(value);
  },
});

export const formPhoneModel = computed({
  get() {
    return state.fields.phone;
  },
  set(value: string) {
    setFormPhone(value);
  },
});

export const formMessageModel = computed({
  get() {
    return state.fields.message;
  },
  set(value: string) {
    setFormMessage(value);
  },
});

export const formCguModel = computed({
  get() {
    return state.fields.cgu;
  },
  set(value: boolean) {
    setFormCgu(value);
  },
});

export const isFormVisible = computed(() => state.isVisible);
export const isFormLoading = computed(() => state.isLoading);
