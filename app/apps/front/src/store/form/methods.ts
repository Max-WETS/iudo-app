import { state } from "./state";
import { handleCardPayment, isFreeProduct, handleNoPayment } from "../checkout";
import { Analytics } from "@iudo/data";
import { getFormEmail, getFormName } from "./getters";
import { bindFormWatcher } from "../user/watchers";

export const onFormSubmit = async (e) => {
  setFormIsLoading(true);
  if (isFreeProduct.value) {
    await handleNoPayment();
  } else {
    await handleCardPayment();
  }
  setFormIsLoading(false);

  // // Segments
  // if (window.analytics) {
  //   window.analytics.identify(null, {
  //     email: getFormEmail.value,
  //     name: getFormName.value,
  //   });
  // }
};

export const setFormName = (name: string) => {
  state.fields.name = name;
  if (name) {
    localStorage.formName = name;
  }
};
export const clearFormName = () => {
  state.fields.name = null;
  localStorage.removeItem("formName");
};

export const setFormPhone = (phone: string) => {
  state.fields.phone = phone;
  if (phone) {
    localStorage.formPhone = phone;
  }
};

export const clearFormPhone = () => {
  state.fields.phone = null;
  localStorage.removeItem("formPhone");
};

export const setFormEmail = (email: string) => {
  state.fields.email = email;
  if (email) {
    localStorage.formEmail = email;
  }
};
export const clearFormEmail = () => {
  state.fields.email = null;
  localStorage.removeItem("formEmail");
};

export const setFormMessage = (message: string) => {
  state.fields.message = message;
  if (message) {
    localStorage.formMessage = message;
  }
};

export const clearFormMessage = () => {
  state.fields.message = null;
  localStorage.removeItem("formMessage");
};

export const setFormCgu = (cgu: boolean) => {
  state.fields.cgu = cgu;
  if (cgu) {
    localStorage.formCgu = cgu;
  }
};
export const clearFormCgu = () => {
  state.fields.cgu = false;
  localStorage.removeItem("formCgu");
};

export const initForm = () => {
  if (localStorage.formEmail) {
    setFormEmail(localStorage.formEmail);
  }
  if (localStorage.formName) {
    setFormName(localStorage.formName);
  }
  if (localStorage.formMessage) {
    setFormMessage(localStorage.formMessage);
  }
  if (localStorage.formPhone) {
    setFormPhone(localStorage.formPhone);
  }
  if (localStorage.formCgu) {
    setFormCgu(localStorage.formCgu);
  }
  bindFormWatcher();
};

export const clearForm = () => {
  clearFormName();
  clearFormPhone();
  clearFormEmail();
  clearFormMessage();
  clearFormCgu();
};
export const toggleForm = () => (state.isVisible = !state.isVisible);
export const hideForm = () => (state.isVisible = false);
export const showForm = () => (state.isVisible = true);

export const setFormIsLoading = (value) => (state.isLoading = value);
