import { reactive } from "@vue/composition-api";
import { initForm } from ".";

export interface FormState {
  fields: {
    name: string;
    email: string;
    message: string;
    phone: string;
    cgu: boolean;
  };
  errors: [];
  isVisible: boolean;
  isLoading: boolean;
}

export const state = reactive<FormState>({
  fields: {
    name: "",
    email: "",
    message: "",
    phone: "",
    cgu: false,
  },
  errors: [],
  isVisible: false,
  isLoading: false,
});
