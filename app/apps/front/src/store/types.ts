export enum CharacteristicBlock {
  SURFACE_UNITE_FONCIERE = "Surface unité foncière",
  SURFACE_BATIE = "Surface bâtie",
  EMPRISE_AU_SOL = "Emprise au sol actuelle",
  HAUTEUR_MEDIANE = "Hauteur médiane",
}
export enum LocalisationBlock {
  ZONE_PLU = "Zone PLU",
  STATION_DE_TRANSPORT = "Station de transport (-500m)",
  MONUMENT_HISTORIQUE = "Monument historique (-500m)",
  FUTURE_GARE = "Future gare (-800m)",
  TCSP = "Station Transport en Commun en Site Propre (TCSP)",
  TOOLTIP_FUTURE_GARE = "Future gare du Grand Paris Express",
  LOCATION = "Location (prix moyen)",
  VENTE = "Vente (prix moyen)",
}
export enum RisksBlock {
  CAVITES_SOUTERRAINES = "Cavités souterraines",
  INONDATIONS = "Inondations",
  MOUVEMENT_DE_TERRAIN = "Mouvement de terrain",
  ARGILES = "Retrait-gonflement des argiles",
}
