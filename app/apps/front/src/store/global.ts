import { clearForm } from "./form";
import { clearUserParcels, clearUserLandUnitInfos } from "./user/methods";
import { resetMapState } from "./map";
import { computed, reactive, watch } from "@vue/composition-api";
import { NotificationOptions } from "vue-notification";
import { clearSearchInput } from "@iudo/shared/components/search/methods";

import links from "./links";

export const resetState = () => {
  resetMapState();
  clearForm();
  clearUserParcels();
  clearUserLandUnitInfos();
  clearSearchInput();
};

export const state = reactive({
  notificationInstance: null,
  showLexicon: false,
});

export const isLexiconVisible = computed(() => state.showLexicon);

export const showLexicon = () => {
  state.showLexicon = true;
};
export const hideLexicon = () => {
  state.showLexicon = false;
};

export const setNotificationInstance = (value) => {
  state.notificationInstance = value;
  state.notificationInstance({
    group: "foo",
    title: "Important message",
    text: "Hello user! This is a notification!",
  });
};
export const getNotificationInstance = computed(
  () => state.notificationInstance
);

export const showNotification = (options: NotificationOptions | string) =>
  getNotificationInstance.value(options);

export const getLinks = computed(() => links);
