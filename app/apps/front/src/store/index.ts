import * as map from "./map";

import * as form from "./form";
import * as checkout from "./checkout";
import * as utils from "@iudo/shared/utils";
import * as global from "./global";

export const useMap = () => map;

export const useForm = () => form;
export const useCheckout = () => checkout;
export const useUtils = () => utils;
export const useGlobal = () => global;
