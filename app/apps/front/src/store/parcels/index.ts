import { Parcel, LandUnit, ParcelsApiResponse, AddressApi } from "@iudo/data";
import { fetchApi } from "@iudo/shared/utils";
import {
  addToUserParcels,
  removeFromUserParcels,
  isUserParcel,
  isNeighbor,
  setUserSubmitedAddress,
  clearUserParcels,
} from "../user/methods";
import { reverseAddress } from "@iudo/shared/components/address";
import { Popup } from "mapbox-gl";
import {
  getMap,
  setMapLayerHoveredId,
  addMapPopup,
  showMapPointerCursor,
  showMapDefaultCursor,
  mapFitToParcel,
  mapFlyToPoint,
} from "../map";
import { getUserParcelsIds, hasUserParcels } from "../user/getters";
import {
  filterCadastreSelectedLayersByPropValue,
  resetCadastreSelectedLayers,
  filterCadastreHighlightedLayerByPropValue,
} from "../map/layers/cadastre";
import { computed } from "@vue/composition-api";
import { showNotification } from "../global";
import EventBus from "@iudo/shared/components/search/eventBus";

const API_URL = process.env.VUE_APP_API_URL;

export const bindSearchEvents = () => {
  EventBus.$on("search-submit-event", async (event, isStandalone) => {});

  EventBus.$on(
    "on-selected-result",
    async ({
      address,
      parcel,
      coordinates,
    }: {
      address?: AddressApi.SearchFeatureProperties;
      parcel?: Parcel;
      coordinates?: [number, number];
    }) => {
      clearUserParcels();

      setUserSubmitedAddress(address);

      if (parcel && address.type === "housenumber") {
        // showParcelPopup({
        //   address,
        //   parcel,
        // });

        mapFitToParcel(parcel);
        onSelectedParcel(parcel);
      } else {
        const [lon, lat] = coordinates;
        mapFlyToPoint(coordinates);
      }
    }
  );
};

export const updateCadastreSelectedLayers = () => {
  if (getUserParcelsIds.value && getUserParcelsIds.value.length) {
    filterCadastreSelectedLayersByPropValue("n_sq_pc", getUserParcelsIds.value);
  } else {
    resetCadastreSelectedLayers();
  }
};

export const onHoveredParcel = (parcelId: Parcel["id"]) => {
  setMapLayerHoveredId(parcelId);

  if (!hasUserParcels.value || isUserParcel(parcelId) || isNeighbor(parcelId)) {
    showMapPointerCursor();
  } else {
    showMapDefaultCursor();
  }

  if (!hasUserParcels.value || (hasUserParcels.value && isNeighbor(parcelId))) {
    filterCadastreHighlightedLayerByPropValue("n_sq_pc", [parcelId]);
  }
};

export const onUnselectedParcel = (parcelId: Parcel["id"]) => {
  removeFromUserParcels(parcelId);
};

export const onSelectedParcel = async (parcel: Parcel) => {
  if (
    !hasUserParcels.value ||
    (hasUserParcels.value && isNeighbor(parcel.id))
  ) {
    addToUserParcels([parcel]);

    if (!hasUserParcels.value) {
      const parcelAddress = await fetchAddressFromCoordinates(
        parcel.coordinates
      );
      showParcelPopup({
        address: parcelAddress,
        parcel,
      });
    }
  } else {
    showNotification({
      text: "Seules les parcelles contigües peuvent être sélectionnées  !",
      type: "error",
    });
  }
};

export const fetchAddressFromCoordinates = async (
  coordinates: Parcel["coordinates"]
) => {
  const { latitude: lat, longitude: long } = coordinates;
  const addressResult = await reverseAddress({
    lat,
    long,
    type: hasUserParcels.value ? "housenumber" : undefined,
  });

  const addressFeature = addressResult.features[0];
  const address = addressFeature.properties;

  return address;
};

export const showParcelPopup = ({ address, parcel }) => {
  const { latitude: lat, longitude: lng } = parcel.coordinates;

  addMapPopup(
    new Popup({ closeButton: false, offset: [0, 0] })
      .setLngLat({ lat, lng })
      .setHTML(
        `
  <div><b>${address.label}</b></div>
  <div>${parcel.csp}</div>
  `
      )
      .addTo(getMap.value)
  );
};

export const getParcelsIdsQueryFromArray = computed(() => {
  return getUserParcelsIds.value.join(",");
});

export const getParcelsArrayFromQuery = (parcels) => {
  return parcels.split(",").map((parcel) => Number(parcel));
};
