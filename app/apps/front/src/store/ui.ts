export const ui = {
  colors: {
    rgb: {
      yellow: "255, 241, 47",
      blue: "31, 41, 127",
      "area-fill": "241, 247, 239",
      dark: "56, 53, 47",
    },
    hex: {
      yellow: "#FFF12F",
      blue: "#1F297F",
      dark: "#38352F",
    },
  },
};
