export default {
  create_account: "https://airtable.com/shr5rVvW7oh1RiVx9",
  potential_example:
    "https://uploads-ssl.webflow.com/5dbbfd68c0a7286c3cd6bf0f/601d131673e085c08bb406a8_IUDO_Exemple-Fiche-potentiel.pdf",
  cgu_cgv: "https://www.iudo.co/legal#cgu-cgv",
};
