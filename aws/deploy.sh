#!/bin/sh

ecs-cli compose --project-name iudo-app service up --cluster-config iudo-app --force-deployment --ecs-profile iudo-app-profile