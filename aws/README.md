# Amazon AWS

## Mise en place

### Détail de la configuration nécessaire

* Instance : 1 x t3a.xlarge (4vcpu, 16Go RAM) ou 1 x t3a.large (2vcpu, 8Go RAM)
* EBS : 30Go en gp2

Dans le cas d'une utilisation d'une instance t3a.xlarge, ne pas oublier de mettre à jour le fichier `ecs-params.yml` :

```yaml
task_definition:
  task_size:
    mem_limit: 16GB
    cpu_limit: 4vcpu
```

voir https://github.com/aws/amazon-ecs-cli/issues/606

### Create a cluster configuration:

```
ecs-cli configure --cluster iudo-app --default-launch-type EC2 --config-name iudo-app --region eu-west-3
```

### Create a profile using your access key and secret key:

```
ecs-cli configure profile --access-key AWS_ACCESS_KEY_ID --secret-key AWS_SECRET_ACCESS_KEY --profile-name iudo-app-profile
```

### Generates the secret & IAM task execution role for use in an Amazon ECS task definition. 

Replace the "username" & "password" env variables by the right values.

```
USERNAME=username PASSWORD=password ecs-cli registry-creds --region eu-west-3 --ecs-profile iudo-app-profile --cluster-config iudo-app up ./credentials.yml --role-name iudo-app
```

A ce stade, quelques manipulations manuelles :

* Ajouter la stratégie "traefik" (cf traefik.strategy.json) au rôle `iudo-app`


### Create a RSA key pair

https://eu-west-3.console.aws.amazon.com/ec2/v2/home?region=eu-west-3#KeyPairs:

### Create the cluster

```
ecs-cli up --keypair id_rsa --capability-iam --instance-type t3a.large --port 443 --cluster-config iudo-app --ecs-profile iudo-app-profile
```

A ce stade, quelques manipulations manuelles :

* Associer l'IP Elastic à l'instance EC2
* Ajouter le port 80 dans le groupe de sécurité

### Deploy the Compose File

```
ecs-cli compose --project-name iudo-app up --create-log-groups --cluster-config iudo-app --ecs-profile iudo-app-profile
```

### View the Running Containers on a Cluster 

```
ecs-cli ps --cluster-config iudo-app --ecs-profile iudo-app-profile
```

### Create an ECS Service from a Compose File

Now that you know that your containers work properly, you can make sure that they are replaced if they fail or stop. You can do this by creating a service from your compose file with the ecs-cli compose service up command. This command creates a task definition from the latest compose file (if it does not already exist) and creates an ECS service with it, with a desired count of 1. 

```
ecs-cli compose --project-name iudo-app down --cluster-config iudo-app --ecs-profile iudo-app-profile
```

Now you can create your service.

```
ecs-cli compose --project-name iudo-app service up --cluster-config iudo-app --force-deployment --ecs-profile iudo-app-profile --deployment-min-healthy-percent=0 --deployment-max-percent=100
```

# Clean Up

When you are done, you should clean up your resources so they do not incur any more charges. First, delete the service so that it stops the existing containers and does not try to run any more tasks.

```
ecs-cli compose --project-name iudo-app service rm --cluster-config iudo-app --ecs-profile iudo-app-profile
```

Now, take down your cluster, which cleans up the resources that you created earlier with ecs-cli up.

```
ecs-cli down --force --cluster-config iudo-app --ecs-profile iudo-app-profile
```