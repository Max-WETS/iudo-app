IUDo
===

Ce monorepo contient l'ensemble des éléments constitutifs de l'application IUDo.

* `app` : Application Vue.JS + API Node Express
* `geoserver` : Instance pré-configurée de [GeoServer](http://geoserver.org/) qui fournit les flux WFS
* `postgis` : Instance pré-configurée de PostgresQL + PostGIS intégrant l'ensemble des données utiles

## Environnement de développement

### Installation

1. Construire et démarrer le projet :
    ```
    docker-compose build
    docker-compose up -d
    ```

2. Vérifier le bon fonctionnement :

    - http://admin.iudo.localhost:8081 :
      vérifier le bon chargement des données `admin>gis>Databases>gis>Schemas>public>Tables(9)`
    - http://gis.iudo.localhost:8081/geoserver :
      vérifier la présence des couches correspondantes en choisissant `Prévisualisation de la couche` dans le menu
  
> IMPORTANT : Les services postgis et geoserver peuvent être un peu longs à démarrer,
notamment dans le cadre d'un premier lancement, avec le chargement des données dans la base.
Il faut donc attendre un peu avant que tout soit opérationnel.

### App

Pour démarrer l'application en mode dévéloppement, se rendre dans le dossier `app` :

1. Copier le fichier `.env` en `.local.env` et renseigner les variables manquantes

2. Lancer  : 

   ```
   npm install
   npm run serve
   ```

L'application est alors disponible sur http://localhost:8080

Une documentation plus détailée de l'application est accessible dans le dossier app : [app/README.md](app/README.md)

### GeoServer

#### Accès en environnement de développement

- Accès à l'administration du GeoServer : `http://gis.iudo.localhost:8081/geoserver`
- Compte administrateur : `admin/poc-iudo`

#### Publication des couches pour Mapbox

S'il est nécessaire d'ajouter des couches supplémentaires.
Depuis l'interface d'administration de GeoServer :

1. Clic sur `Couches` pour sélectionner une couche vecteur à publier 
2. Sur l'onglet `Données`
   - définir les emprises natives (basées sur les données par exemple)
   - définir l'emprise géographique (calculées sur les emprises natives par exemple)
3. Sur l'onglet `Cache de tuiles`, sur la section `Formats image de tuiles`,
ajouter aux formats standards GIF/PNG/JPEG :
   - application/json;type=geojson
   - application/json;type=topojson
   - application/vnd.mapbox-vector-tile

#### Paramétrage de GeoServer

Le dossier de paramétrage de GeoServer est le dossier `geoserver/data`.

### PostGis

La description du contenu des données de la base PostGIS est disponible dans le dossier postgis : [postgis/README.md](postgis/README.md)

#### Rechargement des données dans la base `postgis`

Pour réinitialiser le chargement des données dans la base postgis,
il faut redémarrer les conteneurs en supprimant les volumes permanents.

```
docker-compose down -v
docker-compose up -d
```

#### PG Admin

Accessible en environnement de développement uniquement.

- Accès à PGAdmin : `http://admin.iudo.localhost:8081`
- Compte : `contact@conjecto.com/iudo-sig`

## Déploiement

Le déploiement de l'application IUDo est entièrement gérer par Gitlab CI. Le contenu de `gitlab-ci.yml` détaille les étapes de construction et de déploiement des différents containeurs 
de l'application.
