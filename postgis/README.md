IUDo : PostGIS
===

## Liste des sources dans la base `postgis`

| Nom | Section | Couche - Table SIG | Source |
| --- | ------- | ------------------ | ------ |
| Parcelle Cadastrale | Carte, Informations Unité Foncière | pca | [Lien Opendata APUR](https://opendata.apur.org/datasets/parcelle-cadastrale?geometry=2.294%2C48.816%2C2.296%2C48.816)
| Emprise bâtie Métropole Grand Paris | Carte, Informations Unité Foncière | ebm | [Lien Opendata APUR](https://opendata.apur.org/datasets/emprise-batie-metropole-du-grand-paris-12/data?geometry=2.297%2C48.821%2C2.316%2C48.823)
| Patrimoine Immeuble Protégé | Carte | pip | [Lien Opendata APUR](https://opendata.apur.org/datasets/patrimoine-immeuble-protege)
| Patrimoine Périmètre Protection | Carte | ppp | [Lien Opendata APUR](https://opendata.apur.org/datasets/patrimoine-perimetre-protection/geoservice?geometry=2.239%2C48.808%2C2.362%2C48.828)
| Station Transport en Commun | Carte | stc | [Lien Opendata APUR](https://opendata.apur.org/datasets/station-de-transport-en-commun?geometry=2.313%2C48.707%2C2.467%2C48.727)
| Périmètre 800 m Gares MGP Stat Associées | Carte | p8g | [Lien Opendata APUR](https://opendata.apur.org/datasets/perimetre-800m-gares-mgp-stat-associees-1)
| PLU Zonage | Informations Unité Foncière | plu | [Lien Opendata APUR](https://opendata.apur.org/datasets/plu-zonage?geometry=2.292%2C48.817%2C2.322%2C48.822)
| PCI : numvoie | Informations Unité Foncière | nvo | [ Lien data.gouv.fr ](https://cadastre.data.gouv.fr/datasets/plan-cadastral-informatise)

NB : les données relatives aux risques naturels ne sont pas gérées dans le SIG mais collectées en asynchrone 
sur l'[API Géorisques](http://www.georisques.gouv.fr/doc-api#/Risques).

## Correspondance des données utiles

| Information | Type/Unité | Section | Sous-section | Couche - Table SIG | Champ | Exemple
| ----------- | ---------- | ------- | ------------ | ------------------ | ----- | -------
| N° |  | Adresse | | nvo | numero | 39
| rue |  | Adresse | | API BAN | nomvoie | Rue Chauvelot 
| Code postal | | Adresse | | API BAN | codepostal | 92240
| Commune |  | Adresse | | API BAN | nomcommune | Malakoff
| Identifiant de la parcelle | Code | Parcelle |  | pca | c_csp (c_cainsee+c_sec+c_np)| 92046-C-0252
| Surface de la parcelle | m<sup>2</sup>  | Informations générales | Caractéristiques | pca | m2_dgfip / shape_area | 537 / 531.21 m<sup>2</sup>
| Surface bâtie | m<sup>2</sup> | Informations générales | Caractéristiques | ebm | surface (surface du bâti) | 202.57 m<sup>2</sup>
| Emprise au sol | % | Informations générales | Caractéristiques | calcul | Surface bâtie/Surface parcelle | 37.72% 
| Hauteur médiane | m | Informations générales | Caractéristiques | ebm | h_med | 7,74 m
| Zone PLU | Code | Informations générales | Localisation | plu | c_zonage_b | UBa
| Gare (-500m) | Oui/Non | Informations générales | Localisation | stc | présence d'au moins un enregistrement | Non
| Monument historique (-500m) | Oui/Non | Informations générales | Localisation | pip | présence d'au moins un enregistrement avec c_imp_niv2='MH' | Oui
| Future gare (-800m) | Oui/Non | Informations générales | Localisation | p8g | présence d'au moins un enregistrement | Non
| Cavités souterraines | Oui/Non | Informations générales | Risques | API géorisques | num_risque=135 | Oui
| Inondations | Oui/Non | Informations générales | Risques | API géorisques | num_risque=140 | Non
| Mouvement de terrain | Oui/Non | Informations générales | Risques | API géorisques | num_risque=134 | Oui
| Retrait-gonflement des argiles | Oui/Non | Informations générales | Risques | API géorisques | num_risque=157 (Mouvement de terrain - Tassements différentiels) | Non

## Listes de valeurs

### PLU ZONAGE (PLU)

#### c_zonage

|Code|Nom|Définition|
|--- |--- |--- |
|1|Zone naturelle|Zone classée en zone N (selon les articles L151-11 à L151-13 du CU) dans les PLU des communes. Secteurs équipés ou non, à protéger en raison soit de la qualité des sites, des milieux naturels, des paysages et de leur intérêt, soit de l’existence d’une exploitation forestière, soit de leur caractère d’espaces naturels, selon le code de l’urbanisme.|
|2|Zone urbaine verte|Zone urbaine utilisée dans certains PLU pour protéger les espaces verts et de loisirs urbains (squares, équipements sportifs, cimetière…). Cette zone couvre généralement des espaces faiblement bâtis dont la fonction sportive ou culturelle, de loisirs ou paysagère doit être préservée et mise en valeur afin d’assurer aux habitants un cadre de vie de qualité. La constructibilité est généralement limitée.|
|3|Zone pavillonnaire|Zone urbaine d’habitat à dominante pavillonnaire : zone à vocation résidentielle, composée d’habitat individuel. C’est généralement une zone monofonctionnelle où l’activité n’est pas autorisée ou est peu développée.|
|4|Grand Ensemble|Zone urbaine d’habitat collectif discontinu qui désigne les zones dans les PLU dont la vocation est principalement résidentielle et qui correspondent aux immeubles de logements collectifs construits en ordre discontinu et aux grands ensembles datant des années 1960-1970.|
|5|Zone mixte dense|Zone urbaine destinées à la fois à l’habitat, aux services et aux activités. Elle correspond en général à un tissu dense. Elle se développe le plus souvent le long des axes et au niveau des centres-villes. Elle se caractérise par des règles de hauteur et d’emprise au sol plus élevées. Et les constructions doivent en principe s’implanter en ordre continu, à l’alignement des voies.|
|6|Zone mixte semi-dense|Zone urbaine correspondant à une zone intermédiaire composée à la fois de petits collectifs et d’habitat pavillonnaire. On y trouve également quelques commerces et des équipements. C’est un tissu généralement semi-dense. Les règles relatives aux hauteurs et à l’emprise au sol des constructions sont moins élevées qu’en zone mixte dense. Et les bâtiments peuvent être implantés en retrait ou à l’alignement par rapport aux voies.|
|7|Equipement|Zone urbaine d’équipement public ou collectif d’intérêt général. Tous les PLU ne disposent pas d’une telle zone. Elle correspond aux différents équipements publics ou collectifs d’intérêt général que les communes souhaitent préserver. Ces zones à vocation unique n’autorisent généralement pas les autres destinations (habitat, commerces, bureaux…).|
|8|Zone d'activité|Zone urbaine dans les PLU qui correspond aux zones destinées aux activités à vocation économique, industrielle, commerciale ou dédiées aux infrastructures ferroviaires, portuaires et aéroportuaires. Elle se développe généralement le long des infrastructures routières et ferroviaires, mais également le long des canaux. L’habitat n’y est généralement pas autorisé sauf en cas de nécessité de fonctionnement ou de gardiennage.|
|9|Zone à urbaniser|Identifié comme « zone AU » dans les PLU communaux, elle correspond aux secteurs destinés à être ouverts à l’urbanisation selon le code de l’urbanisme (article R151-20)|
|10|Zone Agricole|Identifié comme « zone A » dans les PLU communaux, elle correspond aux secteurs de la commune, équipés ou non, à protéger en raison du potentiel agronomique, biologique ou économique des terres agricoles selon le code de l’urbanisme (articles L151-11 à L151-13).|
|11|Secteur hors PLU|Correspond aux secteurs non règlementés par les PLU, comme les secteurs du Marais et du 7ème arrondissement à Paris qui sont régis par des plans de sauvegarde et de mise en valeur (PSMV) et le jardin du Luxembourg que la loi « urbanisme et habitat » du 2 juillet 2003 a placé en dehors du champ d’application du PLU parisien.|

### PLU EMPRISE (EMP)

#### c_emprise

|Code|Nom|Définition|
|--- |--- |--- |
|1|inférieur ou égal à 40%|Le CES maximal défini dans le règlement se situe dans une fourchette de 0 à 40%|
|2|50%|Le CES maximal défini dans le règlement se situe dans une fourchette de 41 à 55%|
|3|60%|Le CES maximal défini dans le règlement se situe dans une fourchette de 56 à 65%|
|4|entre 70% et 80%|Le CES maximal défini dans le règlement se situe dans une fourchette de 66 à 85%|
|5|entre 90% et 100%|Le CES maximal défini dans le règlement se situe dans une fourchette de 86 à 100%|
|6|Zone naturelle|Zones classées en zone N dans les PLU des communes. Ces zones n’ayant pas vocation à être bâties, elles n’ont pas fait l’objet d’un récolement de la règle d’emprise au sol des constructions|
|7|Emprise non règlementée|Zones ou secteurs pour lesquels l’emprise au sol des constructions n’est pas règlementée, la règle relative à l’emprise au sol des constructions étant facultative dans les règlements des PLU.|
|8|Emprise définie selon plan masse|Secteurs pour lesquels l’emprise au sol règlementaire est reportée sur un plan de masse qui comporte généralement des prescriptions architecturales (hauteur, emprise, implantation…) particulières et plus précises que la règle générale.|
|9|Secteur hors PLU|Correspond aux secteurs non règlementés par les PLU, comme les secteurs du Marais et du 7ème arrondissement à Paris qui sont régis par des plans de sauvegarde et de mise en valeur (PSMV) et le jardin du Luxembourg que la loi « urbanisme et habitat » du 2 juillet 2003 a placé en dehors du champ d’application du PLU parisien.|

### PLU HAUTEUR (HAU)

#### c_hauteur

|Code|Nom|Définition|
|--- |--- |--- |
|1|Inférieur ou égal à 9m|La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette jusqu’à 9 mètres|
|2|de 10 à 15m|La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette entre 10 et 15 mètres|
|3|de 16 à 24m|La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette entre 16 et 24 mètres|
|4|de 25 à 30 m|La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette entre 25 et 30 mètres|
|5|de 31 à 36m|La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette entre 31 et 36 mètres|
|6|37 m et plus|La hauteur maximale des constructions définie dans le règlement se situe dans une fourchette de 37 mètres et plus|
|7|Zone naturelle|Zones classées en zone N dans les PLU des communes. Ces zones n’ayant pas vocation à être bâties, elles n’ont pas fait l’objet d’un récolement de la règle de la hauteur des constructions|
|8|Zone sans limitation de hauteur|Zones ou secteurs pour lesquels la hauteur des constructions n’est pas règlementée, la règle relative à la hauteur des constructions étant facultative dans les règlements des PLU.|
|9|Hauteur définie selon le plan masse|Secteurs pour lesquels la hauteur règlementaire est reportée sur un plan de masse qui comporte généralement des prescriptions architecturales (hauteur, emprise, implantation…) particulières et plus précises que la règle générale.|
|10|Hauteur relative aux façades voisines|La hauteur est définie par référence à celle des bâtiments existants voisins. Secteurs généralement constitués de bâtis anciens dans lesquels les hauteurs existantes sont variables ce qui constitue une caractéristique architecturale et urbaine que les communes souhaitent préserver.|
|11|Hauteur relative à largeur de voie|La hauteur est définie par référence à la largeur de la voie, c’est-à-dire en fonction de la distance entre la construction et l’alignement opposé.|
|12|La Défense|Secteur de la Défense, pour lequel les hauteurs règlementaires sont spécifiques et correspondent généralement à des immeubles de grande hauteur (IGH)|
|13|Secteur hors PLU|Correspond aux secteurs non règlementés par les PLU, comme les secteurs du Marais et du 7ème arrondissement à Paris qui sont régis par des plans de sauvegarde et de mise en valeur (PSMV) et le jardin du Luxembourg que la loi « urbanisme et habitat » du 2 juillet 2003 a placé en dehors du champ d’application du PLU parisien.|

### EMPRISE BATI PARIS (EBP)

#### c_perconst

|Code|Nom|Définition|
|--- |--- |--- |
|1|Avant 1800||
|2|1801-1850||
|3|1851-1914||
|5|1915-1939||
|6|1940-1967||
|7|1968-1975||
|8|1976-1981||
|9|1982-1989||
|10|1990-1999||
|11|2000-2007||
|12|2008 et plus||

#### c_toitdom

|Code|Nom|Définition|
|--- |--- |--- |
|1|Tuile||
|2|Zinc||
|3|Ardoise||
|4|Terrasse béton||
|5|Terrasse végétale||

#### c_morpho

|Code|Nom|Définition|
|--- |--- |--- |
|1|Logement individuel|Hauteur inférieure à 10 mètres et surface au sol comprise entre 3 et 190m|
|2|Immeuble collectif de moins de 3 étages|Hauteur inférieure à 10 mètres et surface au sol  comprise entre 190 et 500m²|
|3|Immeuble collectif de moins de 3 étages|Hauteur inférieure à 10 mètres et surface au sol  comprise entre 500 et 1000m²|
|4|Immeuble collectif de plus de 3 étages|Hauteur comprise entre 10 et 37 mètres et surface inférieure 1000 m²|
|5|Ensemble d’habitation, de bureaux et d’activités de plus de 6 étages|Hauteur comprise entre 20 et 37 mètres et surface supérieure à 1000m²|
|6|Ensemble d’habitation, de bureaux et d’activités de moins de 6 étages|Hauteur inférieure à 20 mètres et surface au sol supérieure à 1000 m²|
|7|Tour et IGH|Hauteur supérieure ou égale à 37 mètres quelle que soit la surface au sol|

## Questions en suspens

- Valider le mode de calcul de la hauteur médiane d'une UF (valider la pondération par les surfaces bâties)
- valider que le "Retrait-gonflement des argiles" correspond bien au "Mouvement de terrain - Tassements différentiels" (num_risque=157) 

## Requêtes

#### Emprise bâtie
```postgresql
SELECT
    pca.c_csp AS parcelle,
    pca.m2_dgfip AS surface_parcelle,
    pca.wkb_geometry AS geo_parcelle,
    ebm.*
FROM public.ebm AS ebm
         JOIN public.pca AS pca
              ON ST_Contains(ST_Buffer(pca.wkb_geometry,2), ebm.wkb_geometry)
WHERE pca.c_csp = '92046-C-0252';
```

#### PLU
```postgresql
SELECT plu.*
FROM public.plu AS plu
JOIN public.pca AS pca
ON ST_Contains(plu.wkb_geometry, ST_Buffer(pca.wkb_geometry,-2))
WHERE pca.c_csp = '92046-C-0252';
```

#### Gare (-500m)
```postgresql
SELECT ST_Distance(pca.wkb_geometry, stc.wkb_geometry) AS distance, stc.*
FROM public.stc AS stc
JOIN public.pca AS pca
ON ST_DWithin(pca.wkb_geometry, stc.wkb_geometry, 500)
WHERE pca.c_csp = '92046-C-0252';
```

#### Monument historique (-500m)
```postgresql
SELECT ST_Distance(pca.wkb_geometry, pip.wkb_geometry) AS distance, pip.*
FROM public.pip AS pip
JOIN public.pca AS pca
ON ST_DWithin(pca.wkb_geometry, pip.wkb_geometry, 500)
WHERE pca.c_csp = '92046-C-0252'
AND pip.c_imp_niv2='MH' ;
```

#### Future gare (-800m)
```postgresql
SELECT p8g.*
FROM public.p8g AS p8g
JOIN public.pca AS pca
ON ST_Intersects(pca.wkb_geometry, p8g.wkb_geometry)
WHERE pca.c_csp = '92046-C-0252';
```

#### API Géorisques

Récupération des coordonnées lat,lon depuis la géométrie
```postgresql
SELECT 
    ST_X(ST_TRANSFORM(ST_CENTROID(pca.wkb_geometry),4674)) AS LONG,
    ST_Y(ST_TRANSFORM(ST_CENTROID(pca.wkb_geometry),4674)) AS LAT
FROM public.pca AS pca
WHERE pca.c_csp = '92046-C-0252';
```
https://www.georisques.gouv.fr/api/v1/gaspar/risques?rayon=150&latlon=2.30694186877035%2C48.8219642636189&page=1&page_size=10
