IUDo - Commues couvertes

## CSV

Colonnes :

* CODGEO : Code géographique
* LIBGEO : Libellé géographique
* DEP : Département
* REG : Région
* EPCI : Intercommunalité/Métropole
* NATURE_EPCI : Nature d'EPCI
* ARR : Arrondissement
* CV : Canton ville
* ZE2010 : Zone d'emploi 2010
* UU2010 : Unité urbaine 2010
* TUU2017 : Tranche d'unité urbaine 2017
* TDUU2017 : Tranche détaillée d'unité urbaine 2017
* AU2010 : Aire urbaine 2010
* TAU2017 : Tranche d'aire urbaine 2017
* CATAEU2010 : Catégorie commune dans aires urbaines
* BV2012 : Bassin de vie 2012
