#!/usr/bin/env bash

HOST=localhost
PORT=5432
SCHEMAS=public
DBNAME=${POSTGRES_DBNAME:-gis}
USER=${POSTGRES_USER:-iudo}
PASS=${POSTGRES_PASS:-iudo}
SRID=2154
TABLE=nvo

TMPDIR=/tmp/others/nvo

if [ "$#" -eq 0 ]; then
  # Départements limités à Paris, Petite Couronne et Grand Couronne
  DPTS=(
    '75'
    '92' '93' '94'
    '77' '78' '91' '95'
    )
else
  DPTS=( "$@" )
fi

function sql {
    PGPASSWORD=$PASS psql -d $DBNAME -h $HOST -U $USER  -c "$1"
}  

echo "-- préparation du répertoire temporaire"
mkdir -p $TMPDIR
cd $TMPDIR
rm -fR *

for key in "${!DPTS[@]}"
do
  dpt=${DPTS[$key]}
  echo "-- Import du département ${dpt} dans la table ${TABLE}"

  echo "-- ${dpt} : téléchargement..."
  wget https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/geojson/departements/${dpt}/raw/pci-${dpt}-numvoie.json.gz -q -O ${dpt}.numvoie.json.gz
  if [ $? -ne 0 ]; then
    continue
  fi

  echo "-- ${dpt} : décompression..."
  gunzip -q ${dpt}.numvoie.json.gz

  echo "-- ${dpt} : chargement en bdd..."
  [[ $key = 0 ]] && opts=( -overwrite ) || opts=( -append )
  ogr2ogr -f "PostgreSQL" "PG:host=${HOST} port=${PORT} schemas=${SCHEMAS} dbname=${DBNAME} user=${USER} password=${PASS}" ${dpt}.numvoie.json -nln ${TABLE} -s_srs EPSG:4326 -t_srs EPSG:${SRID} ${opts[@]}

  echo "-- ${dpt} : terminé."
done

echo "-- création des index..."
# sql "CREATE UNIQUE INDEX IF NOT EXISTS ${TABLE}_idx ON ${TABLE} (id)"
sql "CREATE INDEX IF NOT EXISTS ${TABLE}_geom_idx ON ${TABLE} USING GIST (wkb_geometry);"
sql "VACUUM ANALYZE ${TABLE};"
sql "CLUSTER ${TABLE} USING ${TABLE}_geom_idx;"

echo "-- Fin des traitements"