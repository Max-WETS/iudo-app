#!/usr/bin/env bash

HOST=localhost
PORT=5432
SCHEMAS=public
DBNAME=${POSTGRES_DBNAME:-gis}
USER=${POSTGRES_USER:-iudo}
PASS=${POSTGRES_PASS:-iudo}
SRID=2154
TABLE=(
  'pca'     # PARCELLE CADASTRALE
  'ebm'     # EMPRISE BATIE METROPOLE DU GRAND PARIS
  'pip'     # PATRIMOINE IMMEUBLE PROTEGE
  'ppp'     # PATRIMOINE PERIMETRE PROTECTION
  'stc'     # STATION DE TRANSPORT EN COMMUN
  'p8g'     # PERIMETRE 800M GARES MGP STAT ASSOCIEES 
  'plu'     # PLU ZONAGE
  'ebp'     # EMPRISE BATIE PARIS
  'emp'     # PLU EMPRISE
  'hau'     # PLU HAUTEUR
  )
URL=(
  'https://opendata.arcgis.com/datasets/c38cf1e31205484ca9014e35132789cc_0.zip?outSR=%7B%22latestWkid%22%3A2154%2C%22wkid%22%3A102110%7D'
  'https://opendata.arcgis.com/datasets/6113a2ad151f4418af92212f19879d66_0.zip?outSR=%7B%22latestWkid%22%3A2154%2C%22wkid%22%3A102110%7D'
  'https://opendata.arcgis.com/datasets/5edbb2bccca444b5bf9a6cf8862693f3_0.zip?outSR=%7B%22latestWkid%22%3A2154%2C%22wkid%22%3A102110%7D'
  'https://opendata.arcgis.com/datasets/dbb68831dfe141348118da4961885b45_0.zip?outSR=%7B%22latestWkid%22%3A2154%2C%22wkid%22%3A102110%7D'
  'https://opendata.arcgis.com/datasets/0c3b913f450c4246816fd7a3f68b456a_0.zip?outSR=%7B%22latestWkid%22%3A2154%2C%22wkid%22%3A102110%7D'
  'https://opendata.arcgis.com/datasets/c909db90d1b944628afe60ddef11c1a0_0.zip?outSR=%7B%22latestWkid%22%3A2154%2C%22wkid%22%3A102110%7D'
  'https://opendata.arcgis.com/datasets/a62283a69981422e91ede3f9cbce9c9e_0.zip?outSR=%7B%22latestWkid%22%3A2154%2C%22wkid%22%3A102110%7D'
  'https://opendata.arcgis.com/datasets/002f14c0cf28435296a341d9921adf99_0.zip?outSR=%7B%22latestWkid%22%3A2154%2C%22wkid%22%3A102110%7D'
  'https://opendata.arcgis.com/datasets/0a0506ba1bae49bda16f3a3ae70c70ce_2.zip?outSR=%7B%22latestWkid%22%3A2154%2C%22wkid%22%3A102110%7D'
  'https://opendata.arcgis.com/datasets/3fd2cdfb69be41dd899c19b3b304c13b_1.zip?outSR=%7B%22latestWkid%22%3A2154%2C%22wkid%22%3A102110%7D'
  )
DIR=(
  'PCA'
  'EBM'
  'PIP'
  'PPP'
  'STC'
  'P8G'
  'PLU'
  'EBP'
  'EMP'
  'HAU'
  )
FILENAME=(
  'PARCELLE_CADASTRALE'
  'EMPRISE_BATIE_METROPOLE_DU_GRAND_PARIS'
  'PATRIMOINE_IMMEUBLE_PROTEGE'
  'PATRIMOINE_PERIMETRE_PROTECTION'
  'STATION_DE_TRANSPORT_EN_COMMUN'
  'PERIMETRE_800M_GARES_MGP_STAT_ASSOCIEES'
  'PLU_ZONAGE'
  'EMPRISE_BATIE_PARIS'
  'PLU_EMPRISE'
  'PLU_HAUTEUR'
  )
GEOMETRY=(
  'MULTIPOLYGON'
  'MULTIPOLYGON'
  'MULTIPOLYGON'
  'MULTIPOLYGON'
  'POINT'
  'MULTIPOLYGON'
  'MULTIPOLYGON'
  'MULTIPOLYGON'
  'MULTIPOLYGON'
  'MULTIPOLYGON'
  )

FID=(
  'n_sq_pc'
  'n_sq_eb'
  'n_sq_imp'
  'n_sq_pp'
  'n_sq_stc'
  'n_sq_st'
  'objectid'
  'n_sq_eb'
  'objectid'
  'objectid'
)
  
OPTS=(
  '-lco COLUMN_TYPES=n_sq_pc=integer'
  ''
  ''
  ''
  ''
  ''
  ''
  ''
  ''
  ''
)
  
# set and clean temporary directory
TMPDIR=/tmp/apur
mkdir -p $TMPDIR
cd $TMPDIR
rm -fR *

function sql {
    PGPASSWORD=$PASS psql -d $DBNAME -h $HOST -U $USER  -c "$1"
}  

# get number of tables to process
count=${#TABLE[@]}
echo "-- ${count} table(s) à traiter..."

for (( i=1; i<${count}+1; i++ ));
do
  echo "-- Import des données de la table ${TABLE[$i-1]} "

  echo "-- ${DIR[$i-1]} : téléchargement..."
  wget ${URL[$i-1]} -q -O ${DIR[$i-1]}.zip
  if [ $? -ne 0 ]; then
      continue
  fi

  echo "-- ${DIR[$i-1]} : décompression..."
  mkdir -p ${DIR[$i-1]}
  unzip -q ${DIR[$i-1]}.zip -d ${DIR[$i-1]}
  rm ${DIR[$i-1]}.zip

  echo "-- ${TABLE[$i-1]} : suppression de la table existante..."
  sql "DROP TABLE IF EXISTS ${TABLE[$i-1]}"

  echo "-- ${TABLE[$i-1]} : chargement en bdd..."
  ogr2ogr -f "PostgreSQL" "PG:host=${HOST} port=${PORT} schemas=${SCHEMAS} dbname=${DBNAME} user=${USER} password=${PASS}" ${DIR[$i-1]}/${FILENAME[$i-1]}.shp -nln ${TABLE[$i-1]} -a_srs EPSG:${SRID} -nlt ${GEOMETRY[$i-1]} ${OPTS[$i-1]}

  echo "-- ${TABLE[$i-1]} : création des index..."
  # sql "CREATE UNIQUE INDEX IF NOT EXISTS ${TABLE[$i-1]}_key ON ${TABLE[$i-1]} (ogc_fid)"
  sql "CREATE INDEX IF NOT EXISTS ${TABLE[$i-1]}_fid_idx ON ${TABLE[$i-1]} (${FID[$i-1]})"
  sql "CREATE INDEX IF NOT EXISTS ${TABLE[$i-1]}_wkb_geometry_geom_idx ON ${TABLE[$i-1]} USING GIST (wkb_geometry);"
  sql "VACUUM ANALYZE ${TABLE[$i-1]};"
  # sql "CLUSTER ${TABLE[$i-1]} USING ${TABLE[$i-1]}_geom_idx;"

  echo "-- ${TABLE[$i-1]} : terminé."

done
echo "-- Fin des traitements"


# remove temp dir
rm -fR $TMPDIR