SELECT plu.*
FROM public.plu AS plu
JOIN public.pca AS pca
ON ST_Contains(plu.wkb_geometry, ST_Buffer(pca.wkb_geometry,-2))
WHERE pca.c_csp = '92046-C-0252';