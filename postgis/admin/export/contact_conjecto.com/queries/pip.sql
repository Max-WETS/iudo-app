SELECT ST_Distance(pca.wkb_geometry, pip.wkb_geometry) AS distance, pip.*
FROM public.pip AS pip
JOIN public.pca AS pca
ON ST_DWithin(pca.wkb_geometry, pip.wkb_geometry, 500)
WHERE pca.c_csp = '92046-C-0252'
AND pip.c_imp_niv2='MH' ;