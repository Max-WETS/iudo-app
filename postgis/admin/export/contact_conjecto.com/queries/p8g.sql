SELECT ST_Distance(pca.wkb_geometry, p8g.wkb_geometry) AS distance, p8g.*
FROM public.p8g AS p8g
JOIN public.pca AS pca
ON ST_Intersects(ST_BUFFER(pca.wkb_geometry,500), p8g.wkb_geometry)
WHERE pca.c_csp = '92046-C-0252';