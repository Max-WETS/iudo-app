SELECT
  pca.c_csp AS parcelle,
  pca.m2_dgfip AS surface_parcelle,
  pca.wkb_geometry AS geo_parcelle,
  ebm.*
FROM public.ebm AS ebm
JOIN public.pca AS pca
ON ST_Contains(ST_Buffer(pca.wkb_geometry,2), ebm.wkb_geometry)
WHERE pca.c_csp = '92046-C-0252';