SELECT 
ST_X(ST_TRANSFORM(ST_CENTROID(pca.wkb_geometry),4674)) AS LONG,
ST_Y(ST_TRANSFORM(ST_CENTROID(pca.wkb_geometry),4674)) AS LAT
FROM public.pca AS pca
WHERE pca.c_csp = '92046-C-0252';