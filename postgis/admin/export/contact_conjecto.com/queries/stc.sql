SELECT ST_Distance(pca.wkb_geometry, stc.wkb_geometry) AS distance, stc.*
FROM public.stc AS stc
JOIN public.pca AS pca
ON ST_DWithin(pca.wkb_geometry, stc.wkb_geometry, 600)
WHERE pca.c_csp = '92046-C-0252';